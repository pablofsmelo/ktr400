/*=========================================================================================================*/
/*                              Biblioteca de comandos do LCD                                              */
/*=========================================================================================================*/

#ifndef LCD4_H_
#define LCD4_H_

//Defini�oes de hardware
#define RS			PIN_C6
#define EN			PIN_C7
#define RS_ON 		output_high(RS)
#define RS_OFF 		output_low(RS)
#define EN_ON 		output_high(EN)
#define EN_OFF 		output_low(EN)
#define D4			PIN_B3
#define D5			PIN_B4
#define D6			PIN_B5
#define D7			PIN_B6

//Fun�oes
void limpa_display(void);
void posicao(char z,char y);
void escreve_string(unsigned char *dado);
void envia_dado(char dado);
void escreve_lcd(char dado);
void comando_lcd(char dado);
void inicializa_lcd(void);
void escreve_numero_sinal(unsigned char *string,long int valor, unsigned char tamanho, char casas);
void escreve_numero(unsigned char *string,long int valor, unsigned char tamanho, char casas);
void caracteres_especiais_lcd(void);
void caracter_seta_direita(void);
void caracter_seta_esquerda(void);


void limpa_display(void)
{
    comando_lcd(0x01);
    delay_ms(2);
}

void posicao(char z,char y)//z=linha,y=coluna
{
    if(z==1)//linha 1
    {
         comando_lcd(0x7f+y);//Escolhe qual coluna somando 0X7f com o valor de y.Ex: 0x7f + 1= 0x80 que � a primeira coluna do display
    }
    if(z==2)//linha 2
    {
        comando_lcd(0xbf+y);
    }
    delay_us(5);
}
void escreve_string(unsigned char *dado)
{
    int comp,a;
    for(a=0,comp=1;comp!=0x00;a++)
    {
        if (dado[a]!=0x00)
        {
			escreve_lcd(dado[a]);
        }

        comp = dado [a];
    }
}


void envia_dado(char dado)
{
	output_bit(D4,dado&0x01);
	output_bit(D5,dado&0x02);
	output_bit(D6,dado&0x04);
	output_bit(D7,dado&0x08);
	delay_us(200);
	EN_ON;
	delay_us(200);
	EN_OFF;
}


void escreve_lcd (char dado)
{
	RS_ON;
    envia_dado(dado>>4);
    envia_dado(dado);
}

void comando_lcd (char dado)
{
	RS_OFF;
    envia_dado(dado>>4);
    envia_dado(dado);
}

void inicializa_lcd(void)
{
	int cont;
	char Inic[6];
	Inic[0] = 0x33; // inicializa o display
	Inic[1] = 0x32; // inicializa o display
	Inic[2] = 0x28; // modo 4 bits
	Inic[3] = 0x06; // auto-incremento
	Inic[4] = 0x0C; // display ligado, cursor apagado
	Inic[5] = 0x01; // limpa o display
	EN_OFF;
	for(cont=0;cont<6;cont++)
    {
		comando_lcd(Inic[cont]);
	}
    delay_ms(2);
}

void escreve_numero_sinal(unsigned char* string, signed long int valor, unsigned char tamanho, char casas)
{
  char menor=0;

  if(valor < 0)
  {
    menor = 1;
    valor = -valor;
  }
  escreve_numero(string, valor, tamanho, casas);
  string[0] = '+';
  if(menor)
    string[0] = '-';
}

void escreve_numero(unsigned char *string,long int valor, unsigned char tamanho, char casas)
{
  unsigned char i, x;

  for(i=1; i<=tamanho; i++)
  {
    if(casas && i==casas + 1)
      x = ',';
    else
    {
      x = (valor % 10)+ 0x30;
      valor /= 10;
    }
    string[tamanho-i] = (char)x;
	string[tamanho] = 0;
  }
}
//============================================================================================================
void caracteres_especiais_lcd(void)
{
	unsigned char caracteres[]={
	//002 - SETA <-
	0,4,12,31,31,12,4,0,
	//003 - SETA ->
	0,4,6,31,31,6,4,0};
	unsigned int cont_caracter;
	
	comando_lcd(0x40);
	for(cont_caracter=0;cont_caracter<16;cont_caracter++)
	{
		escreve_lcd(caracteres[cont_caracter]);
	}
	
}
//========================================================================================
void caracter_seta_direita(void)
{
	escreve_lcd(0x01);
}
//===================================================================================
void caracter_seta_esquerda(void)
{
	escreve_lcd(0x0);
}
#endif
/*=========================================================================================================================*/
/*		                                        Fim do C�digo                                                              */
/*=========================================================================================================================*/

