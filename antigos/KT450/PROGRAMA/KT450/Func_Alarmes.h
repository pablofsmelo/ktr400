#ifndef _FUNC_ALARMES_H
#define _FUNC_ALARMES_H

#include <string.h>

#define MELODIA_0	0
#define MELODIA_1	1
#define MELODIA_2	2
#define MELODIA_3	3
#define MELODIA_4	4
#define MELODIA_5	5
#define MELODIA_6	6
#define MELODIA_7	7
#define MELODIA_8	8
#define MELODIA_9	9
#define MELODIA_10	10
#define MELODIA_11	11
#define MELODIA_12	12
#define MELODIA_13	13
#define MELODIA_14	14
#define MELODIA_15	15
#define MELODIA_16	16
#define MELODIA_17	17
#define MELODIA_18	18
#define MELODIA_19	19
#define RL_1		20
#define RL_2		21

#define POS_SENHA			220
#define POS_ALARME_EVENTO	222
#define POS_CALIB_SALVA		223


typedef struct
{
	unsigned at_in:1;
	unsigned seg_min:1;
	unsigned minuto:6;
	unsigned hora:6;
	unsigned melodia_rl_low:2;
	unsigned duracao:6;
	unsigned melodia_rl_high:2;
	unsigned dias:7;
	unsigned melodia_rl_b_high:1;
}ALARME_S;

ALARME_S	ALARMES_KT[55];

void read_alarmes(void);
void write_alarme(ALARME_S al, unsigned int num);
unsigned long int read_senha(void);
void write_senha(unsigned long int senha);
unsigned int read_alarme_evento(void);
void write_alarme_evento(unsigned int tipo);
void zerar_alarmes(void);
unsigned int read_calib_relogio(void);
void write_calib_relogio(unsigned int valor_ajustado);


void read_alarmes(void)
{
	unsigned int cont_read=0,aux=0;
	
	for(cont_read=0;cont_read<55;cont_read++)
	{
		aux=read_eeprom(4*cont_read);
		if(aux==0xFF) 
		{
			ALARMES_KT[cont_read].at_in=0;
			ALARMES_KT[cont_read].seg_min=0;
			ALARMES_KT[cont_read].minuto=0;
		}
		else
		{
			ALARMES_KT[cont_read].at_in=aux&0x01;
			ALARMES_KT[cont_read].seg_min=(aux>>1)&0x01;
			ALARMES_KT[cont_read].minuto=(aux>>2)&0x3F;
		}
		aux=read_eeprom((4*cont_read)+1);
		if(aux==0xFF) 
		{
			ALARMES_KT[cont_read].hora=0;
			ALARMES_KT[cont_read].melodia_rl_low=0;
		}
		else
		{
			ALARMES_KT[cont_read].hora=aux&0x3F;
			ALARMES_KT[cont_read].melodia_rl_low=(aux>>6&0x03);
		}
		aux=read_eeprom((4*cont_read)+2);
		if(aux==0xFF) 
		{
			ALARMES_KT[cont_read].duracao=0;
			ALARMES_KT[cont_read].melodia_rl_high=0;
		}
		else
		{
			ALARMES_KT[cont_read].duracao=aux&0x3F;
			ALARMES_KT[cont_read].melodia_rl_high=(aux>>6&0x03);
		}
		aux=read_eeprom((4*cont_read)+3);
		ALARMES_KT[cont_read].dias=aux&0x7F;
		if(aux&0x80) ALARMES_KT[cont_read].melodia_rl_b_high=1;
		else ALARMES_KT[cont_read].melodia_rl_b_high=0;
		/*if(aux==0xFF) 
		{
			ALARMES_KT[cont_read].dias=0x7F;
			ALARMES_KT[cont_read].melodia_rl_b_high=0;
		}
		else
		{
			ALARMES_KT[cont_read].dias=aux&0x7F;
			ALARMES_KT[cont_read].melodia_rl_b_high=(aux>>7&0x01);
		}*/
		
	}
}
//==========================================================================================
void write_alarme(ALARME_S al, unsigned int num)
{
	unsigned int aux=0;
	
	aux|=(al.minuto<<2);
	//aux<<=7;
	aux|=(al.seg_min<<1);
	aux|=al.at_in;
	write_eeprom(num*4,aux);
	aux=0;
	aux|=al.hora;
	aux|=(al.melodia_rl_low<<6);
	write_eeprom((num*4)+1,aux);
	aux=0;
	aux|=al.duracao;
	aux|=(al.melodia_rl_high<<6);
	write_eeprom((num*4)+2,aux);
	aux=0;
	aux|=al.dias;
	//aux|=(al.melodia_rl_b_high<<7);
	if(al.melodia_rl_b_high)aux|=0x80;
	write_eeprom((num*4)+3,aux);
}
//=========================================================================================
unsigned long int read_senha(void)
{
	unsigned long int senha_read=0;
	
	senha_read=read_eeprom(POS_SENHA);
	senha_read<<=8;
	senha_read |= read_eeprom(POS_SENHA+1);
	if(senha_read==0xffff) senha_read=0000;
	return senha_read;
}
//====================================================================
void write_senha(unsigned long int senha)
{
	write_eeprom(POS_SENHA,((senha>>8)&0x00ff));
	write_eeprom(POS_SENHA+1,(senha&0x00ff));
}
//====================================================================
unsigned int read_alarme_evento(void)
{
	unsigned long int tipo=0;
	
	tipo = read_eeprom(POS_ALARME_EVENTO);
	if(tipo==0xff) tipo=MELODIA_0;
	return tipo;
}
//===================================================================
void write_alarme_evento(unsigned int tipo)
{
	write_eeprom(POS_ALARME_EVENTO, tipo);
}
//======================================================================
void zerar_alarmes(void)
{
	unsigned int aux=0;
	
	for(aux=0;aux<220;aux++)
	{
		write_eeprom(aux, 0xff);
	}
}
//===========================================================
unsigned int read_calib_relogio(void)
{
	unsigned long int valor=0;
	
	valor = read_eeprom(POS_CALIB_SALVA);
	if(valor==0xff) valor=0;
	return valor;
}
//============================================================
void write_calib_relogio(unsigned int valor_ajustado)
{
	write_eeprom(POS_CALIB_SALVA, valor_ajustado);
}
//==============================================================
#endif