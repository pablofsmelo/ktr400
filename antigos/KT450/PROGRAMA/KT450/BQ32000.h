#ifndef _BQ32000_H
#define _BQ32000_H

#define RTC_SDA  PIN_B0
#define RTC_SCL  PIN_B1

#define CAL_CFG1	0x07
#define SF_KEY1		0x20
#define SF_KEY2		0x21
#define SFR			0x22

#use i2c(master, sda=RTC_SDA, scl=RTC_SCL)

typedef struct
{
	unsigned int hora;
	unsigned int minuto;
	unsigned int segundo;
	unsigned int dia;
	unsigned int mes;
	unsigned int ano;
	unsigned int dia_semana;
	
}TIME_DATE;

TIME_DATE	GET_TIME_DATE, SET_TIME_DATE;

signed char valor_calib_reg_bq=0;
signed char inc_dec_calib=0;

unsigned int bin2bcd(unsigned int binary_value);
unsigned int bcd2bin(unsigned int bcd_value);

void bq32000_init(void)
{
   unsigned int seconds = 0;
   i2c_start();
   i2c_write(0xD0);      // WR to RTC
   i2c_write(0x00);      // REG 0
   i2c_start();
   i2c_write(0xD1);      // RD from RTC
   seconds = bcd2bin(i2c_read(0)); // Read current "seconds" in DS1307
   i2c_stop();
   i2c_start();
   i2c_write(0xD0);      // WR to RTC
   i2c_write(0x07);      // Control Register
   i2c_write(0b10010000);     // Disable squarewave output pin
   i2c_stop();
}

void bq32000_set_date_time(void)
{
  SET_TIME_DATE.segundo &= 0x7F;
  SET_TIME_DATE.hora &= 0x3F;

  i2c_start();
  i2c_write(0xD0);            // I2C write address
  i2c_write(0x00);            // Start at REG 0 - Seconds
  i2c_write(bin2bcd(SET_TIME_DATE.segundo));      // REG 0
  i2c_write(bin2bcd(SET_TIME_DATE.minuto));      // REG 1
  i2c_write(bin2bcd(SET_TIME_DATE.hora));      // REG 2
  i2c_write(bin2bcd(SET_TIME_DATE.dia_semana));      // REG 3
  i2c_write(bin2bcd(SET_TIME_DATE.dia));      // REG 4
  i2c_write(bin2bcd(SET_TIME_DATE.mes));      // REG 5
  i2c_write(bin2bcd(SET_TIME_DATE.ano));      // REG 6
  i2c_write(0x80);            // REG 7 - Disable squarewave output pin
  i2c_stop();
}
//add
void bq32000_set_time(void)
{
  SET_TIME_DATE.segundo &= 0x7F;
  SET_TIME_DATE.hora &= 0x3F;

  i2c_start();
  i2c_write(0xD0);            // I2C write address
  i2c_write(0x00);            // Start at REG 0 - Seconds
  i2c_write(bin2bcd(SET_TIME_DATE.segundo));      // REG 0
  i2c_write(bin2bcd(SET_TIME_DATE.minuto));      // REG 1
  i2c_write(bin2bcd(SET_TIME_DATE.hora));      // REG 2
  //i2c_write(0x80);            // REG 7 - Disable squarewave output pin
  i2c_stop();
}

//add
void bq32000_set_date(void)
{
  //SET_TIME_DATE.segundo &= 0x7F;
  //SET_TIME_DATE.hora &= 0x3F;

  i2c_start();
  i2c_write(0xD0);            // I2C write address
  i2c_write(0x03);            // Start at REG 0 - Seconds
  i2c_write(bin2bcd(SET_TIME_DATE.dia_semana));      // REG 3
  i2c_write(bin2bcd(SET_TIME_DATE.dia));      // REG 4
  i2c_write(bin2bcd(SET_TIME_DATE.mes));      // REG 5
  i2c_write(bin2bcd(SET_TIME_DATE.ano));      // REG 6
 // i2c_write(0x80);            // REG 7 - Disable squarewave output pin
  i2c_stop();
}

void bq32000_get_date(void)
{
  i2c_start();
  i2c_write(0xD0);
  i2c_write(0x03);            // Start at REG 3 - Day of week
  i2c_start();
  i2c_write(0xD1);
  GET_TIME_DATE.dia_semana  = bcd2bin(i2c_read() & 0x7f);   // REG 3
  GET_TIME_DATE.dia = bcd2bin(i2c_read() & 0x3f);   // REG 4
  GET_TIME_DATE.mes = bcd2bin(i2c_read() & 0x1f);   // REG 5
  GET_TIME_DATE.ano = bcd2bin(i2c_read(0));            // REG 6
  i2c_stop();
}

void bq32000_get_time(void)
{
  i2c_start();
  i2c_write(0xD0);
  i2c_write(0x00);            // Start at REG 0 - Seconds
  i2c_start();
  i2c_write(0xD1);
  GET_TIME_DATE.segundo = bcd2bin(i2c_read() & 0x7f);
  GET_TIME_DATE.minuto = bcd2bin(i2c_read() & 0x7f);
  GET_TIME_DATE.hora = bcd2bin(i2c_read(0) & 0x3f);
  i2c_stop();

}

unsigned int bin2bcd(unsigned int binary_value)
{
  unsigned int temp;
  unsigned int retval;

  temp = binary_value;
  retval = 0;

  while(1)
  {
    // Get the tens digit by doing multiple subtraction
    // of 10 from the binary value.
    if(temp >= 10)
    {
      temp -= 10;
      retval += 0x10;
    }
    else // Get the ones digit by adding the remainder.
    {
      retval += temp;
      break;
    }
  }

  return(retval);
}


// Input range - 00 to 99.
unsigned int bcd2bin(unsigned int bcd_value)
{
  unsigned int temp;

  temp = bcd_value;
  // Shifting upper digit right by 1 is same as multiplying by 8.
  temp >>= 1;
  // Isolate the bits for the upper digit.
  temp &= 0x78;

  // Now return: (Tens * 8) + (Tens * 2) + Ones

  return(temp + (temp >> 2) + (bcd_value & 0x0f));
}

void bq32000_Enable_Out( void )
{
	i2c_start();
	i2c_write(0xD0);            // I2C write address
	i2c_write(SF_KEY1);
	i2c_write(0x5E);
	i2c_stop();
	
	i2c_start();
	i2c_write(0xD0);            // I2C write address
	i2c_write(SF_KEY2);
	i2c_write(0xC7);
	i2c_stop();
	
	i2c_start();
	i2c_write(0xD0);            // I2C write address
	i2c_write(SFR);
	i2c_write(0x01);
	i2c_stop();
	
	i2c_start();
	i2c_write(0xD0);            // I2C write address
	i2c_write(CAL_CFG1);
	i2c_write(0x40);
	i2c_stop();
	/*Escreve_Registrador(SF_KEY1,0x5E);
	Escreve_Registrador(SF_KEY2,0xC7);
	Escreve_Registrador(SFR,0x00);//1Hz
	Escreve_Registrador(CAL_CFG1,0xEA);//0b11101010 +20ppm*/
}		

void bq32000_set_calib(signed char reg_val)
{
	unsigned char aux=0;
	
	
	aux|=0x40;
	//aux2=reg_val;
	if(reg_val<0)
	{
		aux|=0x20;
		reg_val*=-1;
	}
	aux|=(reg_val&0x1F);
	
	i2c_start();
	i2c_write(0xD0);            // I2C write address
	i2c_write(CAL_CFG1);
	i2c_write(aux);
	i2c_stop();
}

signed char bq32000_read_calib(void)
{
	//char val=0;
	unsigned char aux=0,aux2=0;
	//long long aux2=0,aux3=0,aux4=-245760,aux5=491520;
	float aux3=0;
	
	i2c_start();
	i2c_write(0xD0);            // I2C write address
	i2c_write(CAL_CFG1);
	i2c_start();
	i2c_write(0xD1);
	aux=i2c_read(0);
	i2c_stop();
	
	//aux=0x25;
	aux2=aux;
	aux2&=0x1F;
	aux3=aux2;
	//aux2*=1000000;
	//aux3=aux2/1000;
	if(bit_test(aux,5)) 
	{
		aux3*=-4.069;
	}
	else 
	{
		aux3*=2.0345;
	}
	
	//aux4=(unsigned char)aux3;
	
	return(signed char)aux3;
}
#endif