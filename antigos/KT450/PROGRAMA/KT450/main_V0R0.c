#include <18f2520.h>
#fuses HS,PROTECT,NOWDT,NOPUT,CPD,CPB,NODEBUG,NOBROWNOUT,NOLVP,NOMCLR
#use delay (clock=4000000)
#include "lcd4.h"
#include "BQ32000.h"
#include "Func_Alarmes.h"
#include "WTV020SD_LIB.h"


#define	TELA_INICIAL				0
#define TELA_PRINCIPAL				1
#define TELA_ALARMES_CONFIG			2
#define TELA_ALARME_EVENTO			3
#define TELA_MOSTRA_ALARMES			4
#define TELA_SENHA					5
#define TELA_CONFIG					6
#define TELA_CONFIG_ALARME_EVENTO	7
#define TELA_CONFIG_ALARMES			8
#define TELA_AJUSTE_RELOGIO			9
#define TELA_AJUSTE_DATA			10
#define TELA_SET_SENHA				11
#define TELA_RESET					12
#define TELA_SALVAR_SENHA			13
#define TELA_ALARME_ATIVO			14
#define TELA_CONFIG_REG_BQ			15
#define TELA_FREQ					16
#define TELA_TESTE					99

/*#define TECLA_C 	0x0F
#define TECLA_UP    0x1D
#define TECLA_DOWN  0x1B
#define TECLA_E     0x1E
#define TECLA_P		0x17
#define TECLA_SOLTA	0x1F*/

#define TECLA_UP 	0x0F
#define TECLA_C     0x1D
#define TECLA_DOWN  0x1B
#define TECLA_P     0x1E
#define TECLA_E		0x17
#define TECLA_SOLTA	0x1F

#BYTE PORTA  = 0XF80
#define TECLA_P_PIN		PIN_C4

#define LED_MELODIA		PIN_C2
#define RELE_1			PIN_A3
#define RELE_2			PIN_A5


struct
{
	unsigned int esc_tela;
	unsigned atualizar_tela:1;
	unsigned int tempo_tela_inicial;
	unsigned int tempo_atualiza_horario;
	unsigned int posicao_seta;
	unsigned int tempo_retorna_tela_anterior;
	unsigned int tela_anterior;
	unsigned pisca_pontos:1;
	unsigned escolhe_alarme_num:6;
	unsigned esc_indice_mostra_alarme:1;
	unsigned long int senha;
	unsigned long int senha_nova;
	unsigned int posicao_cursor_senha;
	unsigned int indice_tela_config;
	unsigned int tipo_alarme_evento;
	unsigned int pisca_config;
	unsigned int ultimo_alarme_ativo;
	unsigned int alarme_ativo_atual;
	unsigned long int duracao_alarme_ativo;
	unsigned char conta_tela_freq;
	unsigned contar_tempo_ativo:1;
	unsigned seg_min_alarme_ativo:1;
	unsigned estado_pisca:1;
	unsigned esc_at_in:1;
	unsigned tecla_press_tela:1;
	unsigned at_tela_freq:1;
}TELAS_DISPLAY;

struct 
{
	unsigned int tecla_pressionada;
	unsigned tratar_tecla:1;
	unsigned tempo_teclado:1;
	unsigned tecla_p_press:1;
	unsigned int tempo_tecla_p_press;
}TECLADO;

struct
{
	unsigned long int pulso_1;
	unsigned long int pulso_2;
	unsigned long int diferenca;
	unsigned troca_valor:1;
}CONT_FREQ;

void trata_timer1(void);
void t_inicial(void);
void t_alarmes_config(void);
void t_teste(void);
void t_principal(void);
void t_alarme_evento(void);
void t_mostra_alarmes(void);
void estado_teclado(void);
void t_senha(void);
void t_config(void);
void t_config_alarme_evento(void);
void t_config_alarmes(void);
void t_ajuste_relogio(void);
void t_ajuste_data(void);
void t_set_senha(void);		
void t_reset(void);		
void t_salvar_senha(void);
void t_alarme_ativo(void);
void t_config_reg_bq(void);
void t_freq(void);

void mostra_dias(unsigned int rot);
void mostra_estado_alarmes(unsigned int indice,unsigned int num_alarme);
unsigned long int inc_senha(unsigned int indice_s,unsigned long int valor_atual);
unsigned long int dec_senha(unsigned int indice_s,unsigned long int valor_atual);
void verifica_alarmes(void);

//=======================================================================
#INT_EXT2
void ext_int_2(void)
{
	if(!CONT_FREQ.troca_valor) 
	{
		CONT_FREQ.troca_valor^=1;
		CONT_FREQ.pulso_1=get_timer0();
	}
	else
	{
		CONT_FREQ.troca_valor^=1;
		CONT_FREQ.pulso_2=get_timer0();
		CONT_FREQ.diferenca=CONT_FREQ.pulso_2-CONT_FREQ.pulso_1;
		TELAS_DISPLAY.at_tela_freq=1;
	}
	clear_interrupt(INT_EXT2);	
}
//==================================================
#INT_TIMER1//Interrupção a cada 100ms
void trata_timer1(void)
{
	static unsigned int cont_segundo=0,cont_minuto=0;
	
	disable_interrupts(INT_TIMER1);
	
	if(TELAS_DISPLAY.tempo_tela_inicial) TELAS_DISPLAY.tempo_tela_inicial--;
	if(TELAS_DISPLAY.tempo_atualiza_horario) TELAS_DISPLAY.tempo_atualiza_horario--;
	if(TELAS_DISPLAY.tempo_retorna_tela_anterior) TELAS_DISPLAY.tempo_retorna_tela_anterior--;
	if(TECLADO.tempo_tecla_p_press) TECLADO.tempo_tecla_p_press--;
	if(TELAS_DISPLAY.pisca_config) TELAS_DISPLAY.pisca_config--;
	if(TELAS_DISPLAY.conta_tela_freq) TELAS_DISPLAY.conta_tela_freq--;
	if(TELAS_DISPLAY.contar_tempo_ativo)
	{
		cont_segundo++;
		if(cont_segundo>=10)
		{
			cont_segundo=0;//Passou um segundo
			TELAS_DISPLAY.atualizar_tela=1;
			if(TELAS_DISPLAY.duracao_alarme_ativo && !TELAS_DISPLAY.seg_min_alarme_ativo) TELAS_DISPLAY.duracao_alarme_ativo--;
			cont_minuto++;
			if(cont_minuto>=60)
			{
				cont_minuto=0;//Passou um minuto
				if(TELAS_DISPLAY.duracao_alarme_ativo && TELAS_DISPLAY.seg_min_alarme_ativo) TELAS_DISPLAY.duracao_alarme_ativo--;
			}
		}
	}
	else 
	{
		cont_segundo=0;
		cont_minuto=0;
	}
	TECLADO.tempo_teclado=1;
	
	clear_interrupt(INT_TIMER1);	
	set_timer1(53036);
	enable_interrupts(INT_TIMER1);
}
//====================================================
void main(void)
{
	output_a(0);
	output_b(0);
	output_c(0);
	
	SET_TRIS_A(0b00010111);
	SET_TRIS_B(0x04);//RB2 entrada
	SET_TRIS_C(0b00010000);
	SET_TRIS_E(0b00001000);

	delay_ms(50);
	inicializa_lcd();
	caracteres_especiais_lcd();
	bq32000_init();
	bq32000_Enable_Out();
	init_Wtv020sd();
	
	TELAS_DISPLAY.esc_tela=TELA_INICIAL;
	TELAS_DISPLAY.atualizar_tela=1;
	TELAS_DISPLAY.posicao_seta=1;
	TELAS_DISPLAY.pisca_pontos=1;
	TELAS_DISPLAY.esc_indice_mostra_alarme=0;
	TELAS_DISPLAY.posicao_cursor_senha=3;
	TELAS_DISPLAY.indice_tela_config=0;
	TELAS_DISPLAY.senha_nova=0;
	TELAS_DISPLAY.tipo_alarme_evento=read_alarme_evento();
	TELAS_DISPLAY.ultimo_alarme_ativo=55;
	TELAS_DISPLAY.contar_tempo_ativo=0;
	TELAS_DISPLAY.conta_tela_freq=0;
	TELAS_DISPLAY.tecla_press_tela=0;
	TELAS_DISPLAY.escolhe_alarme_num=0;
	TECLADO.tecla_p_press=0;
	bq32000_get_time();
	bq32000_get_date();
	SET_TIME_DATE.hora=GET_TIME_DATE.hora;
	SET_TIME_DATE.minuto=GET_TIME_DATE.minuto;
	SET_TIME_DATE.segundo=0;
	SET_TIME_DATE.dia=GET_TIME_DATE.dia;
	SET_TIME_DATE.mes=GET_TIME_DATE.mes;
	SET_TIME_DATE.ano=GET_TIME_DATE.ano;
	SET_TIME_DATE.dia_semana=GET_TIME_DATE.dia_semana;
	read_alarmes();//Le os alarmes na eeprom
	TELAS_DISPLAY.senha=read_senha();//Le a senha salva
	valor_calib_reg_bq=read_calib_relogio();
	//valor_calib_reg_bq=bq32000_read_calib();
	if(valor_calib_reg_bq>0) inc_dec_calib=valor_calib_reg_bq/2;
	else inc_dec_calib=valor_calib_reg_bq/4;
	bq32000_set_calib(inc_dec_calib);
	valor_calib_reg_bq=bq32000_read_calib();
	
	
	
	CONT_FREQ=0;
	
	set_timer0(0);
	setup_timer_0(RTCC_INTERNAL|RTCC_DIV_32);
	
	enable_interrupts(INT_EXT2);              
	ext_int_edge(L_TO_H); 
	
	set_timer1(53036);
	setup_timer_1 ( T1_INTERNAL | T1_DIV_BY_8 );
	enable_interrupts(INT_TIMER1);
	enable_interrupts(GLOBAL);
	
	while(1)
	{
		delay_ms(2);
		if(TECLADO.tempo_teclado)
		{
			TECLADO.tempo_teclado=0;
			estado_teclado();
		}
		switch(TELAS_DISPLAY.esc_tela)
		{
			case TELA_INICIAL:t_inicial();break;
			case TELA_TESTE:t_teste();break;
			case TELA_PRINCIPAL:t_principal();break;
			case TELA_ALARMES_CONFIG:t_alarmes_config();break;
			case TELA_ALARME_EVENTO:t_alarme_evento();break;
			case TELA_MOSTRA_ALARMES:t_mostra_alarmes();break;
			case TELA_SENHA:t_senha();break;
			case TELA_CONFIG:t_config();break;
			case TELA_CONFIG_ALARME_EVENTO:t_config_alarme_evento();break;
			case TELA_CONFIG_ALARMES:t_config_alarmes();break;
			case TELA_AJUSTE_RELOGIO:t_ajuste_relogio();break;
			case TELA_AJUSTE_DATA:t_ajuste_data();break;
			case TELA_SET_SENHA:t_set_senha();break;
			case TELA_RESET:t_reset();break;
			case TELA_SALVAR_SENHA:t_salvar_senha();break;
			case TELA_ALARME_ATIVO:t_alarme_ativo();break;
			case TELA_CONFIG_REG_BQ:t_config_reg_bq();break;
			case TELA_FREQ:t_freq();break;
		}
	}
	
}
//=================================================================
void t_inicial(void)
{
	unsigned char str_kt450[]={"KT450"};
	unsigned char str_versao[]={"Ver. 1.0"};
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		posicao(1,2);
		escreve_string(str_kt450);
		posicao(2,1);
		escreve_string(str_versao);
		TELAS_DISPLAY.tempo_tela_inicial=25;//2,5 segundos
		//read_alarmes();//Le os alarmes na eeprom
		//ALARMES_KT[0].at_in=1;//tirar depois
		//TELAS_DISPLAY.senha=read_senha();//Le a senha salva
	}
	if(!TELAS_DISPLAY.tempo_tela_inicial)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
	}
	
}
//==============================================
void t_teste(void)
{
	unsigned char str_teste[]={"TESTE"};
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		posicao(1,1);
		escreve_string(str_teste);
	}
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_E: 
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
				break;
			}
			case TECLA_UP: posicao(1,8);escreve_lcd('U');break;
			case TECLA_DOWN: posicao(1,8);escreve_lcd('D');break;
			case TECLA_C: posicao(1,8);escreve_lcd('C');break;
			case TECLA_P: posicao(1,8);escreve_lcd('P');break;
		}
	}
}
//=====================================================
void t_principal(void)
{
	unsigned char str_horario_data[9];
	
	if(!TELAS_DISPLAY.tempo_atualiza_horario || TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_atualiza_horario=10;//1 segundo
		bq32000_get_time();
		escreve_numero(str_horario_data,GET_TIME_DATE.hora,2,0);
		escreve_numero(&str_horario_data[3],GET_TIME_DATE.minuto,2,0);
		escreve_numero(&str_horario_data[6],GET_TIME_DATE.segundo,2,0);
		if(TELAS_DISPLAY.pisca_pontos)str_horario_data[2]=str_horario_data[5]=':';
		else str_horario_data[2]=str_horario_data[5]=' ';
		TELAS_DISPLAY.pisca_pontos^=1;
		posicao(2,1);
		escreve_string(str_horario_data);
		bq32000_get_date();
		escreve_numero(str_horario_data,GET_TIME_DATE.dia,2,0);
		escreve_numero(&str_horario_data[3],GET_TIME_DATE.mes,2,0);
		escreve_numero(&str_horario_data[6],GET_TIME_DATE.ano,2,0);
		str_horario_data[2]=str_horario_data[5]='/';
		posicao(1,1);
		escreve_string(str_horario_data);
		verifica_alarmes();//
	}
	
	if(TECLADO.tecla_p_press && !TECLADO.tempo_tecla_p_press)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_ALARME_EVENTO;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_E: 
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
				break;
			}
			case TECLA_P:
			{
				if(!TECLADO.tecla_p_press) 
				{
					TECLADO.tecla_p_press=1;
					TECLADO.tempo_tecla_p_press=20;//2 segundos
				}
				break;
			}
			case TECLA_SOLTA:
			{
				TECLADO.tecla_p_press=0;
			}
		}
	}
}
//======================================================
void t_alarmes_config(void)
{
	unsigned char str_alarmes[]={"Alarmes"};
	unsigned char str_config[]={"Config"};
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		//TELA_DISPLAY.tela_anterior=TELA_PRINCIPAL;
		posicao(1,1);
		escreve_string(str_alarmes);
		posicao(2,1);
		escreve_string(str_config);
		posicao(TELAS_DISPLAY.posicao_seta,8);
		caracter_seta_esquerda();
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_C:
			case TECLA_P:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
				break;
			}
			case TECLA_UP:
			{
				TELAS_DISPLAY.posicao_seta=1;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_DOWN:
			{
				TELAS_DISPLAY.posicao_seta=2;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_E:
			{
				if(TELAS_DISPLAY.posicao_seta==1)
				{
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_MOSTRA_ALARMES;
				}
				if(TELAS_DISPLAY.posicao_seta==2)
				{
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_SENHA;
				}
				break;
			}
		}
	}
}
//=====================================================
void t_alarme_evento(void)
{
	unsigned char str_alarme[]={"Alarme"};
	unsigned char str_evento[]={"Evento"};
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		posicao(1,2);
		escreve_string(str_alarme);
		posicao(2,2);
		escreve_string(str_evento);
		switch(TELAS_DISPLAY.tipo_alarme_evento)
		{
			case MELODIA_0:
			case MELODIA_1:
			case MELODIA_2:
			case MELODIA_3:
			case MELODIA_4:
			case MELODIA_5:
			case MELODIA_6:
			case MELODIA_7:
			case MELODIA_8:
			case MELODIA_9:
			case MELODIA_10:
			case MELODIA_11:
			case MELODIA_12:
			case MELODIA_13:
			case MELODIA_14:
			case MELODIA_15:
			case MELODIA_16:
			case MELODIA_17:
			case MELODIA_18:
			case MELODIA_19:
			{
				output_high(LED_MELODIA);
				unmute_Wtv020sd();
				delay_ms(10);
				asyncPlayVoice_Wtv020sd(TELAS_DISPLAY.tipo_alarme_evento);
				break;
			}
			case RL_1: output_high(RELE_1);break;
			case RL_2: output_high(RELE_2);break;
		}
	}
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_SOLTA:
			{
				/*switch(TELAS_DISPLAY.tipo_alarme_evento)
				{
					case MELODIA_0:
					case MELODIA_1:
					case MELODIA_2:
					case MELODIA_3:
					case MELODIA_4:
					case MELODIA_5:
					case MELODIA_6:
					case MELODIA_7:
					case MELODIA_8:
					case MELODIA_9:
					{
						output_low(LED_MELODIA);
						break;
					}
					case RL_1: output_low(RELE_1);break;
					case RL_2: output_low(RELE_2);break;
				}*/
				stopVoice_Wtv020sd();
				output_low(LED_MELODIA);
				output_low(RELE_1);
				output_low(RELE_2);
				TECLADO.tecla_p_press=0;
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
				break;
			}
		}
	}
}
//=====================================================
void estado_teclado(void)
{
	static unsigned int tecla_anterior=0,cont_press=0;
	unsigned int tecla_press=0,aux=0;

	
	tecla_press=(PORTA&0x17);
	aux=input(TECLA_P_PIN);
	aux<<=3;
	tecla_press|=aux;
	tecla_press&=0x1F;
	
	if(tecla_press==tecla_anterior && cont_press<4 && !TECLADO.tratar_tecla)
	{
		tecla_press=0;
		cont_press++;
	}
	else
	{
		cont_press=0;
		TECLADO.tratar_tecla=1;
		if(tecla_press==0) TECLADO.tratar_tecla=0;
		tecla_anterior=tecla_press;
		
	}
	TECLADO.tecla_pressionada=tecla_press;

}
//=====================================================
void t_mostra_alarmes(void)
{
	/*unsigned char str_lig[]={"LIG"};
	unsigned char str_des[]={"DES"};
	unsigned char str_al[]={"AL"};
	unsigned char str_som[]={"SOM"};
	unsigned char str_rl1[]={"RL1"};
	unsigned char str_rl2[]={"RL2"};
	unsigned char str_num[3];
	unsigned int aux=0;
	//int1 esc_indice=0;*/
	
	/*if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		posicao(1,1);
		escreve_string(str_al);
		escreve_numero(str_num,TELAS_DISPLAY.escolhe_alarme_num,2,0);
		escreve_string(str_num);
		posicao(1,6);
		if(!TELAS_DISPLAY.esc_indice_mostra_alarme)
		{
			if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in) escreve_string(str_lig);
			else escreve_string(str_des);
			posicao(2,2);
			escreve_numero(str_num,ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora,2,0);
			escreve_string(str_num);
			escreve_lcd(':');
			escreve_numero(str_num,ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto,2,0);
			escreve_string(str_num);
		}
		else
		{
			aux=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high;
			aux<<=2;
			aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low;
			switch(aux)
			{
				case MELODIA_0:
				case MELODIA_1:
				case MELODIA_2:
				case MELODIA_3:
				case MELODIA_4:
				case MELODIA_5:
				case MELODIA_6:
			    case MELODIA_7:
		        case MELODIA_8:
	            case MELODIA_9:
					 {
						 escreve_string(str_som);
						 break;
					 }
				case RL_1:
					 {
						 escreve_string(str_rl1);
						 break;
					 }
				case RL_2:
					 {
						 escreve_string(str_rl2);
						 break;
					 }
			}
			posicao(2,1);
			aux=0x40;
			mostra_dias(aux);
			
		}
	}*/
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		//TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		//TELAS_DISPLAY.atualizar_tela=1;
		mostra_estado_alarmes(TELAS_DISPLAY.esc_indice_mostra_alarme,TELAS_DISPLAY.escolhe_alarme_num);
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_E:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_indice_mostra_alarme^=1;
				break;
			}
			case TECLA_UP:
			{
				TELAS_DISPLAY.escolhe_alarme_num++;
				if(TELAS_DISPLAY.escolhe_alarme_num>54) TELAS_DISPLAY.escolhe_alarme_num=0;
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_indice_mostra_alarme=0;
				break;
			}
			case TECLA_DOWN:
			{
				if(TELAS_DISPLAY.escolhe_alarme_num) TELAS_DISPLAY.escolhe_alarme_num--;
				else TELAS_DISPLAY.escolhe_alarme_num=54;
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_indice_mostra_alarme=0;
				break;
			}
			case TECLA_C:
			case TECLA_P:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
				break;
			}
		}
	}
}
//===========================================================================================
void t_senha(void)
{
	unsigned char str_senha[]={"Senha:"};
	unsigned char str_errada[]={" Errada! "};
	unsigned char str_correta[]={"Correta!"};
	unsigned char numeros[5];
	static unsigned long int senha_set=0;
	//unsigned long int aux=0;
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		posicao(1,2);
		escreve_string(str_senha);
		escreve_numero(numeros,senha_set,4,0);
		posicao(2,3);
		escreve_string(numeros);
		posicao(2,TELAS_DISPLAY.posicao_cursor_senha);
		comando_lcd(0x0E);
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
		TELAS_DISPLAY.posicao_cursor_senha=3;
		senha_set=0;
		comando_lcd(0x0C);
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_E:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.posicao_cursor_senha++;
				if(TELAS_DISPLAY.posicao_cursor_senha>6)
				{
					TELAS_DISPLAY.posicao_cursor_senha=3;
					
					if(senha_set==TELAS_DISPLAY.senha)
					{
						comando_lcd(0x0C);
						posicao(2,1);
						escreve_string(str_correta);
						delay_ms(800);
						TELAS_DISPLAY.atualizar_tela=1;
						TELAS_DISPLAY.posicao_seta=1;
						TELAS_DISPLAY.indice_tela_config=0;
						TELAS_DISPLAY.esc_tela=TELA_CONFIG;
					}
					else
					{
						comando_lcd(0x0C);
						posicao(2,1);
						escreve_string(str_errada);
						delay_ms(800);
						TELAS_DISPLAY.atualizar_tela=1;
						TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
					}
					senha_set=0;
				}
				break;
			}
			case TECLA_UP:
			{
				senha_set=inc_senha(TELAS_DISPLAY.posicao_cursor_senha,senha_set);
				/*switch(TELAS_DISPLAY.posicao_cursor_senha)
				{
					case 3: 
					{
						aux=senha_set/1000;
						senha_set-=(aux*1000);
						aux++;
						if(aux>9) aux=0;
						senha_set+=(aux*1000);
						break;
					}
					case 4: 
					{
						aux=((senha_set%1000)/100);
						senha_set-=(aux*100);
						aux++;
						if(aux>9) aux=0;
						senha_set+=(aux*100);
						break;
					}
					case 5: 
					{
						aux=(((senha_set%1000)%100)/10);
						senha_set-=(aux*10);
						aux++;
						if(aux>9) aux=0;
						senha_set+=(aux*10);
						break;
					}case 6: 
					{
						aux=(((senha_set%1000)%100)%10);
						senha_set-=aux;
						aux++;
						if(aux>9) aux=0;
						senha_set+=aux;
						break;
					}
				}*/
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_DOWN:
			{
				senha_set=dec_senha(TELAS_DISPLAY.posicao_cursor_senha,senha_set);
				/*switch(TELAS_DISPLAY.posicao_cursor_senha)
				{
					case 3: 
					{
						aux=senha_set/1000;
						senha_set-=(aux*1000);
						if(aux) aux--;
						else aux=9;
						senha_set+=(aux*1000);
						break;
					}
					case 4: 
					{
						aux=((senha_set%1000)/100);
						senha_set-=(aux*100);
						if(aux) aux--;
						else aux=9;
						senha_set+=(aux*100);
						break;
					}
					case 5: 
					{
						aux=(((senha_set%1000)%100)/10);
						senha_set-=(aux*10);
						if(aux) aux--;
						else aux=9;
						senha_set+=(aux*10);
						break;
					}case 6: 
					{
						aux=(((senha_set%1000)%100)%10);
						senha_set-=aux;
						if(aux) aux--;
						else aux=9;
						senha_set+=aux;
						break;
					}
				}*/
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_C:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
				TELAS_DISPLAY.posicao_cursor_senha=3;
				senha_set=0;
				break;
			}
			case TECLA_P:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				if(TELAS_DISPLAY.posicao_cursor_senha>3)TELAS_DISPLAY.posicao_cursor_senha--;
				else
				{
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
					comando_lcd(0x0C);
					TELAS_DISPLAY.posicao_cursor_senha=3;
					senha_set=0;
				}
				break;
			}
		}
	}
}
//======================================================================================
void t_config(void)
{
	unsigned char str_alarmes[]={"Alarmes"};
	unsigned char str_evento[]={"Evento"};
	unsigned char str_relogio[]={"Relogio"};
	unsigned char str_data[]={"Data"};
	unsigned char str_senha[]={"Senha"};
	unsigned char str_reset[]={"Reset"};
	unsigned char str_calibra[]={"Calibra"};
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		switch(TELAS_DISPLAY.indice_tela_config)
		{
			case 0:
			{
				posicao(1,1);
				escreve_string(str_alarmes);
				posicao(2,1);
				escreve_string(str_evento);
				break;
			}
			case 1:
			{
				posicao(1,1);
				escreve_string(str_evento);
				posicao(2,1);
				escreve_string(str_relogio);
				break;
			}
			case 2:
			{
				posicao(1,1);
				escreve_string(str_relogio);
				posicao(2,1);
				escreve_string(str_data);
				break;
			}
			case 3:
			{
				posicao(1,1);
				escreve_string(str_data);
				posicao(2,1);
				escreve_string(str_senha);
				break;
			}
			case 4:
			{
				posicao(1,1);
				escreve_string(str_senha);
				posicao(2,1);
				escreve_string(str_reset);
				break;
			}
			case 5:
			{
				posicao(1,1);
				escreve_string(str_reset);
				posicao(2,1);
				escreve_string(str_calibra);
				break;
			}
			
		}
		posicao(TELAS_DISPLAY.posicao_seta,8);
		caracter_seta_esquerda();
		
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
		TELAS_DISPLAY.posicao_seta=1;
		TELAS_DISPLAY.indice_tela_config=0;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_E:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				if(TELAS_DISPLAY.posicao_seta==1)
				{
					switch(TELAS_DISPLAY.indice_tela_config)
					{
						case 0:
						{
							//TELAS_DISPLAY.atualizar_tela=1;
							TELAS_DISPLAY.esc_tela=TELA_CONFIG_ALARMES;
							TELAS_DISPLAY.indice_tela_config=0;
							TELAS_DISPLAY.estado_pisca=1;
							TELAS_DISPLAY.esc_at_in=0;
							TELAS_DISPLAY.posicao_seta=1;
							TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
							break;
						}
						case 1:
						{
							//TELAS_DISPLAY.atualizar_tela=1;
							TELAS_DISPLAY.esc_tela=TELA_CONFIG_ALARME_EVENTO;
							break;
						}
						case 2:
						{
							TELAS_DISPLAY.esc_tela=TELA_AJUSTE_RELOGIO;
							TELAS_DISPLAY.indice_tela_config=0;
							TELAS_DISPLAY.estado_pisca=1;
							bq32000_get_date();
							bq32000_get_time();
							SET_TIME_DATE.hora=GET_TIME_DATE.hora;
							SET_TIME_DATE.minuto=GET_TIME_DATE.minuto;
							break;
						}
						case 3:
						{
							TELAS_DISPLAY.esc_tela=TELA_AJUSTE_DATA;
							TELAS_DISPLAY.indice_tela_config=0;
							TELAS_DISPLAY.estado_pisca=1;
							bq32000_get_date();
							bq32000_get_time();
							SET_TIME_DATE.dia=GET_TIME_DATE.dia;
							SET_TIME_DATE.mes=GET_TIME_DATE.mes;
							SET_TIME_DATE.ano=GET_TIME_DATE.ano;
							SET_TIME_DATE.dia_semana=GET_TIME_DATE.dia_semana;
							break;
						}
						case 4:
						{
							TELAS_DISPLAY.esc_tela=TELA_SET_SENHA;
							break;
						}
						case 5:
						{
							TELAS_DISPLAY.esc_tela=TELA_RESET;
							break;
						}
					}
				}
				else
				{
					switch(TELAS_DISPLAY.indice_tela_config)
					{
						case 0:
						{
							//TELAS_DISPLAY.atualizar_tela=1;
							TELAS_DISPLAY.esc_tela=TELA_CONFIG_ALARME_EVENTO;
							break;
						}
						case 1:
						{
							TELAS_DISPLAY.esc_tela=TELA_AJUSTE_RELOGIO;
							TELAS_DISPLAY.indice_tela_config=0;
							TELAS_DISPLAY.estado_pisca=1;
							SET_TIME_DATE.hora=GET_TIME_DATE.hora;
							SET_TIME_DATE.minuto=GET_TIME_DATE.minuto;
							break;
						}
						case 2:
						{
							TELAS_DISPLAY.esc_tela=TELA_AJUSTE_DATA;
							TELAS_DISPLAY.indice_tela_config=0;
							TELAS_DISPLAY.estado_pisca=1;
							SET_TIME_DATE.dia=GET_TIME_DATE.dia;
							SET_TIME_DATE.mes=GET_TIME_DATE.mes;
							SET_TIME_DATE.ano=GET_TIME_DATE.ano;
							SET_TIME_DATE.dia_semana=GET_TIME_DATE.dia_semana;
							break;
						}
						case 3:
						{
							TELAS_DISPLAY.esc_tela=TELA_SET_SENHA;
							break;
						}
						case 4:
						{
							TELAS_DISPLAY.esc_tela=TELA_RESET;
							break;
						}
						case 5:
						{
							TELAS_DISPLAY.esc_tela=TELA_CONFIG_REG_BQ;
							break;
						}
					}
				}
				break;
			}
			case TECLA_UP:
			{
				if(TELAS_DISPLAY.posicao_seta==2) 
				{
					TELAS_DISPLAY.posicao_seta=1;
				}
				else
				{
					if(TELAS_DISPLAY.indice_tela_config) TELAS_DISPLAY.indice_tela_config--;
				}
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_DOWN:
			{
				if(TELAS_DISPLAY.posicao_seta==1) 
				{
					TELAS_DISPLAY.posicao_seta=2;
				}
				else
				{
					TELAS_DISPLAY.indice_tela_config++;
					if(TELAS_DISPLAY.indice_tela_config>5) TELAS_DISPLAY.indice_tela_config=5;
				}
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_C:
			case TECLA_P:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
				TELAS_DISPLAY.posicao_seta=1;
				TELAS_DISPLAY.indice_tela_config=0;
				break;
			}
		}
	}
}
//=================================================================================
void t_config_alarme_evento(void)
{
	unsigned char str_saida[]={"Saida:"};
	unsigned char str_melodia_x[]={"Melod:"};
	unsigned char str_rele_x[]={"Rele"};
	static unsigned int reproduzindo=0;
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		posicao(1,2);
		escreve_string(str_saida);
		
		switch(TELAS_DISPLAY.tipo_alarme_evento)
		{
			case MELODIA_0:
			case MELODIA_1:
			case MELODIA_2:
			case MELODIA_3:
			case MELODIA_4:
			case MELODIA_5:
			case MELODIA_6:
			case MELODIA_7:
			case MELODIA_8:
			case MELODIA_9:
			case MELODIA_10:
			case MELODIA_11:
			case MELODIA_12:
			case MELODIA_13:
			case MELODIA_14:
			case MELODIA_15:
			case MELODIA_16:
			case MELODIA_17:
			case MELODIA_18:
			case MELODIA_19:
			{
				posicao(2,1);
				escreve_string(str_melodia_x);
				escreve_lcd((TELAS_DISPLAY.tipo_alarme_evento/10)+'0');
				escreve_lcd((TELAS_DISPLAY.tipo_alarme_evento%10)+'0');
				break;
			}
			case RL_1:
			case RL_2:
			{
				posicao(2,2);
				escreve_string(str_rele_x);
				escreve_lcd((TELAS_DISPLAY.tipo_alarme_evento-19)+'0');
				break;
			}
		}
		
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		write_alarme_evento(TELAS_DISPLAY.tipo_alarme_evento);
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_CONFIG;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_UP:
			{
				TELAS_DISPLAY.tipo_alarme_evento++;
				if(TELAS_DISPLAY.tipo_alarme_evento>RL_2) TELAS_DISPLAY.tipo_alarme_evento=MELODIA_0;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_DOWN:
			{
				if(TELAS_DISPLAY.tipo_alarme_evento) TELAS_DISPLAY.tipo_alarme_evento--;
				else TELAS_DISPLAY.tipo_alarme_evento=RL_2;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_C:
			case TECLA_E:
			{
				write_alarme_evento(TELAS_DISPLAY.tipo_alarme_evento);
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				break;
			}
			case TECLA_P:
			{
				TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
				switch(TELAS_DISPLAY.tipo_alarme_evento)
				{
					case MELODIA_0:
					case MELODIA_1:
					case MELODIA_2:
					case MELODIA_3:
					case MELODIA_4:
					case MELODIA_5:
					case MELODIA_6:
					case MELODIA_7:
					case MELODIA_8:
					case MELODIA_9:
					case MELODIA_10:
					case MELODIA_11:
					case MELODIA_12:
					case MELODIA_13:
					case MELODIA_14:
					case MELODIA_15:
					case MELODIA_16:
					case MELODIA_17:
					case MELODIA_18:
					case MELODIA_19:
					{
						if(!reproduzindo)
						{
							reproduzindo=1;
							unmute_Wtv020sd();
							delay_ms(10);
							asyncPlayVoice_Wtv020sd(TELAS_DISPLAY.tipo_alarme_evento);
							output_high(LED_MELODIA);
						}
						break;
					}
					case RL_1: output_high(RELE_1);break;
					case RL_2: output_high(RELE_2);break;
				}
				
				break;
			}
			case TECLA_SOLTA:
			{
				reproduzindo=0;
				stopVoice_Wtv020sd();
				output_low(LED_MELODIA);
				output_low(RELE_1);
				output_low(RELE_2);
				break;
			}
		}
	}	
}
//=============================================================================================
void t_config_alarmes(void)
{
	unsigned char str_alarmes[]={"Alarmes"};
	unsigned char str_ativos[]={"  Ativos"};
	unsigned char str_inativos[]={"Inativos"};
	unsigned char str_alarme[]={"Alarme"};
	unsigned char str_ativo[]={"   Ativo"};
	unsigned char str_inativo[]={" Inativo"};
	/*unsigned char str_lig[]={"LIG"};
	unsigned char str_des[]={"DES"};
	unsigned char str_al[]={"AL"};
	unsigned char str_som[]={"SOM"};
	unsigned char str_rl1[]={"RL1"};
	unsigned char str_rl2[]={"RL2"};*/
	unsigned char str_horario[]={"Horario:"};
	unsigned char str_duracao[]={"Duracao:"};
	unsigned char str_seg[]={" Seg"};
	unsigned char str_min[]={" Min"};
	unsigned char str_saida[]={"Saida:"};
	unsigned char str_melodia[]={"Melod:"};
	unsigned char str_rele1[]={"Rele1"};
	unsigned char str_rele2[]={"Rele2"};
	unsigned char str_dias[]={"Dias:"};
	unsigned char str_num[3];
	unsigned char str_num2[6];
	//unsigned char str_mel[3];
	unsigned int aux=0,aux_2=0;
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		//TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		//TELAS_DISPLAY.pisca_config=5;//500 milesegundos
		//TELAS_DISPLAY.estado_pisca=1;
		switch(TELAS_DISPLAY.indice_tela_config)
		{
			case 0:
			{
				posicao(1,1);
				escreve_string(str_alarmes);
				posicao(2,1);
				if(!TELAS_DISPLAY.esc_at_in) 
				{
					escreve_string(str_ativos);
					posicao(2,1);
					if(TELAS_DISPLAY.estado_pisca) 
					{
						escreve_lcd(str_ativos[0]);
					}
					else caracter_seta_direita();
				}
				else
				{
					escreve_string(str_inativos);
					posicao(2,1);
					if(TELAS_DISPLAY.estado_pisca) 
					{
						escreve_lcd(str_inativos[0]);
					}
					else caracter_seta_direita();
				}
				break;
			}
			case 1:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				mostra_estado_alarmes(TELAS_DISPLAY.estado_pisca,TELAS_DISPLAY.escolhe_alarme_num);
				/*posicao(1,1);
				escreve_string(str_al);
				escreve_numero(str_num,TELAS_DISPLAY.escolhe_alarme_num,2,0);
				escreve_string(str_num);
				posicao(1,6);
				if(!TELAS_DISPLAY.estado_pisca)
				{
					if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in) escreve_string(str_lig);
					else escreve_string(str_des);
					posicao(2,2);
					escreve_numero(str_num,ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora,2,0);
					escreve_string(str_num);
					escreve_lcd(':');
					escreve_numero(str_num,ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto,2,0);
					escreve_string(str_num);
				}
				else
				{
					aux=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high;
					aux<<=2;
					aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low;
					switch(aux)
					{
						case MELODIA_0:
						case MELODIA_1:
						case MELODIA_2:
						case MELODIA_3:
						case MELODIA_4:
						case MELODIA_5:
						case MELODIA_6:
						case MELODIA_7:
						case MELODIA_8:
						case MELODIA_9:
							 {
								 escreve_string(str_som);
								 break;
							 }
						case RL_1:
							 {
								 escreve_string(str_rl1);
								 break;
							 }
						case RL_2:
							 {
								 escreve_string(str_rl2);
								 break;
							 }
					}
					posicao(2,1);
					aux=0x40;
					mostra_dias(aux);
					
				}*/
				break;
			}
			case 2:
			{
				posicao(1,1);
				escreve_string(str_alarme);
				escreve_numero(str_num,TELAS_DISPLAY.escolhe_alarme_num,2,0);
				escreve_string(str_num);
				posicao(2,1);
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in) escreve_string(str_ativo);
				else escreve_string(str_inativo);
				if(TELAS_DISPLAY.estado_pisca)
				{
					posicao(2,1);
					caracter_seta_direita();
				}
				break;
			}
			case 3:
			{
				posicao(1,1);
				escreve_string(str_horario);
				posicao(2,2);
				escreve_numero(str_num2,ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora,2,0);
				escreve_numero(&str_num2[3],ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto,2,0);
				str_num2[2]=':';
				escreve_string(str_num2);
				posicao(2,3);
				break;
			}
			case 4:
			{
				posicao(1,1);
				escreve_string(str_horario);
				posicao(2,2);
				escreve_numero(str_num2,ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora,2,0);
				escreve_numero(&str_num2[3],ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto,2,0);
				str_num2[2]=':';
				escreve_string(str_num2);
				posicao(2,6);
				break;
			}
			case 5:
			{
				posicao(1,1);
				escreve_string(str_duracao);
				posicao(2,1);
				escreve_string(str_seg);
				escreve_string(str_min);
				if(TELAS_DISPLAY.estado_pisca)
				{
					
					if(!ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min)posicao(2,1);
					else posicao(2,5);
					caracter_seta_direita();
				}
				break;
			}
			case 6:
			{
				posicao(1,2);
				escreve_string(str_saida);
				
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high) aux=1;
				else aux=0;
				//aux=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high;
				aux<<=2;
				aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high;
				aux<<=2;
				aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low;
				switch(aux)
				{
					case MELODIA_0:
					case MELODIA_1:
					case MELODIA_2:
					case MELODIA_3:
					case MELODIA_4:
					case MELODIA_5:
					case MELODIA_6:
					case MELODIA_7:
					case MELODIA_8:
					case MELODIA_9:
					case MELODIA_10:
					case MELODIA_11:
					case MELODIA_12:
					case MELODIA_13:
					case MELODIA_14:
					case MELODIA_15:
					case MELODIA_16:
					case MELODIA_17:
					case MELODIA_18:
					case MELODIA_19:
					 {
						 posicao(2,1);
						 escreve_string(str_melodia);
						 escreve_lcd((aux/10)+'0');
						 escreve_lcd((aux%10)+'0');
						 posicao(2,8);
						 break;
					 }
					case RL_1:
					 {
						 posicao(2,2);
						 escreve_string(str_rele1);
						 posicao(2,6);
						 break;
					 }
					case RL_2:
					 {
						 posicao(2,2);
						 escreve_string(str_rele2);
						 posicao(2,6);
						 break;
					 }
				}
				break;
			}
			case 7:
			{
				posicao(1,1);
				escreve_string(str_duracao);
				escreve_numero(str_num,ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao,2,0);
				if(TELAS_DISPLAY.estado_pisca)
				{
					posicao(2,2);
					escreve_string(str_num);
					posicao(2,4);
					if(!ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min) escreve_string(str_seg);
					else escreve_string(str_min);
					
				}
				break;
			}
			case 8:
			{
				posicao(1,1);
				escreve_string(str_dias);
				posicao(2,1);
				aux=0x40;
				mostra_dias(aux);
				/*if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & aux) escreve_lcd('S');
				else  escreve_lcd('s');
				aux>>=1;
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & aux) escreve_lcd('T');
				else  escreve_lcd('t');
				aux>>=1;
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & aux) escreve_lcd('Q');
				else  escreve_lcd('q');
				aux>>=1;
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & aux) escreve_lcd('Q');
				else  escreve_lcd('q');
				aux>>=1;
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & aux) escreve_lcd('S');
				else  escreve_lcd('s');
				aux>>=1;
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & aux) escreve_lcd('S');
				else  escreve_lcd('s');
				aux>>=1;
				if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & aux) escreve_lcd('D');
				else  escreve_lcd('d');*/
				posicao(2,TELAS_DISPLAY.posicao_seta);
				break;
			}
		}
	}
	
	if(!TELAS_DISPLAY.pisca_config)
	{
		TELAS_DISPLAY.pisca_config=5;
		TELAS_DISPLAY.estado_pisca^=1;
		TELAS_DISPLAY.atualizar_tela=1;
		/*posicao(2,1);
		if(TELAS_DISPLAY.estado_pisca) 
		{
			escreve_lcd(str_ativos[0]);
		}
		else caracter_seta_direita();*/
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.posicao_seta=1;
		TELAS_DISPLAY.indice_tela_config=0;
		TELAS_DISPLAY.esc_tela=TELA_CONFIG;
		comando_lcd(0x0C);
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_UP:
			{
				switch(TELAS_DISPLAY.indice_tela_config)
				{
					case 0:
					{
						TELAS_DISPLAY.esc_at_in^=1;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 1:
					{
						TELAS_DISPLAY.escolhe_alarme_num++;
						if(TELAS_DISPLAY.escolhe_alarme_num>54) TELAS_DISPLAY.escolhe_alarme_num=0;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 2:
					{
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in=0;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in=1;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 3:
					{
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora++;
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora>23) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora=0;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 4:
					{
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto++;
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto>59) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto=0;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 5:
					{
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min=0;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min=1;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 6:
					{
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high) aux=1;
						else aux=0;
						//aux=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high;
						aux<<=2;
						aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high;
						aux<<=2;
						aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low;
						aux++;
						if(aux>RL_2) aux=MELODIA_0;
						aux_2=((aux>>4)&0x01);
						if(aux_2) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high=1;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high=0;
						/*aux_2=0;
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high) aux_2=1;
						else aux_2=0;*/
						//ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high=1;
						//aux_2=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high;
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low=(aux&0x03);
						//aux_2=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low;
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high=((aux>>2)&0x03);
						//aux_2=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 7:
					{
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao++;
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao>59) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao=0;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 8:
					{
						TELAS_DISPLAY.posicao_seta++;
						if(TELAS_DISPLAY.posicao_seta>7) TELAS_DISPLAY.posicao_seta=7;
						TELAS_DISPLAY.atualizar_tela=1;
					}
				}
				TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
				break;
			}
			case TECLA_DOWN:
			{
				switch(TELAS_DISPLAY.indice_tela_config)
				{
					case 0:
					{
						TELAS_DISPLAY.esc_at_in^=1;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 1:
					{
						if(TELAS_DISPLAY.escolhe_alarme_num) TELAS_DISPLAY.escolhe_alarme_num--;
						else TELAS_DISPLAY.escolhe_alarme_num=54;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 2:
					{
						//ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in^=1;
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in=0;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in=1;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 3:
					{
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora--;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].hora=23;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 4:
					{
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto--;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].minuto=59;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 5:
					{
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min=0;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].seg_min=1;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 6:
					{
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high) aux=1;
						else aux=0;
						//aux=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high;
						aux<<=2;
						aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high;
						aux<<=2;
						aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low;
						if(aux) aux--;
						else aux=RL_2;
						aux_2=((aux>>4)&0x01);
						if(aux_2) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high=1;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high=0;
						//ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high=((aux>>4)&0x01);
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_low=(aux&0x03);
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_high=((aux>>2)&0x03);
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 7:
					{
						//ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao++;
						if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao) ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao--;
						else ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].duracao=59;
						TELAS_DISPLAY.atualizar_tela=1;
						break;
					}
					case 8:
					{
						//TELAS_DISPLAY.posicao_seta++;
						if(TELAS_DISPLAY.posicao_seta>1) TELAS_DISPLAY.posicao_seta--;
						//else TELAS_DISPLAY.posicao_seta
						TELAS_DISPLAY.atualizar_tela=1;
					}
				}
				TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
				break;
			}
			case TECLA_E:
			{
				TELAS_DISPLAY.indice_tela_config++;
				if(TELAS_DISPLAY.indice_tela_config==3 || TELAS_DISPLAY.indice_tela_config==4 || TELAS_DISPLAY.indice_tela_config==6 || TELAS_DISPLAY.indice_tela_config==8) comando_lcd(0x0E);
				else comando_lcd(0x0C);
				if(TELAS_DISPLAY.indice_tela_config>8) 
				{
					if(TELAS_DISPLAY.ultimo_alarme_ativo==TELAS_DISPLAY.escolhe_alarme_num) TELAS_DISPLAY.ultimo_alarme_ativo=55;//teste
					write_alarme(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num],TELAS_DISPLAY.escolhe_alarme_num);
					TELAS_DISPLAY.indice_tela_config=0;
					TELAS_DISPLAY.posicao_seta=1;
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				}
				TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
				break;
			}
			case TECLA_P:
			{
				if(TELAS_DISPLAY.indice_tela_config!=8)
				{
					if(TELAS_DISPLAY.indice_tela_config) TELAS_DISPLAY.indice_tela_config--;
					else
					{
						TELAS_DISPLAY.indice_tela_config=0;
						TELAS_DISPLAY.posicao_seta=1;
						TELAS_DISPLAY.atualizar_tela=1;
						TELAS_DISPLAY.esc_tela=TELA_CONFIG;
					}
					if(TELAS_DISPLAY.indice_tela_config==3 || TELAS_DISPLAY.indice_tela_config==4 || TELAS_DISPLAY.indice_tela_config==6 || TELAS_DISPLAY.indice_tela_config==8) comando_lcd(0x0E);
					else comando_lcd(0x0C);
				}
				else
				{
					TELAS_DISPLAY.atualizar_tela=1;
					//aux=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias;
					switch(TELAS_DISPLAY.posicao_seta)
					{
						case 1: aux=0b01000000;break;
						case 2: aux=0b00100000;break;
						case 3: aux=0b00010000;break;
						case 4: aux=0b00001000;break;
						case 5: aux=0b00000100;break;
						case 6: aux=0b00000010;break;
						case 7: aux=0b00000001;break;
					}
					if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias&aux) 
					{
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias &= ~aux;
					}
					else  
					{
						ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias |= aux;
					}
					aux=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias;
				}
				
				TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
				break;
			}
			case TECLA_C:
			{
				comando_lcd(0x0C);
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				TELAS_DISPLAY.indice_tela_config=0;
				TELAS_DISPLAY.posicao_seta=1;
				TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
				break;
			}
		}
	}
}
//============================================================================================
void t_ajuste_relogio(void)
{
	unsigned char str_hora[]={"Hora:"};
	unsigned char str_minuto[]={"Minuto"};
	unsigned char str_relogio[]={"Relogio"};
	unsigned char str_alterado[]={"Alterado"};
	unsigned char str_valor[3];
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		//TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		TELAS_DISPLAY.pisca_config=8;//800 milisegundos
		if(!TELAS_DISPLAY.indice_tela_config)
		{
			posicao(1,2);
			escreve_string(str_hora);
			posicao(2,4);
			if(TELAS_DISPLAY.estado_pisca)
			{
				escreve_numero(str_valor,SET_TIME_DATE.hora,2,0);
				escreve_string(str_valor);
			}
			else 
			{
				escreve_lcd(' ');
				escreve_lcd(' ');
			}
		}
		else
		{
			posicao(1,1);
			escreve_string(str_minuto);
			posicao(2,4);
			if(TELAS_DISPLAY.estado_pisca)
			{
				escreve_numero(str_valor,SET_TIME_DATE.minuto,2,0);
				escreve_string(str_valor);
			}
			else 
			{
				escreve_lcd(' ');
				escreve_lcd(' ');
			}
		}
	}
	
	if(!TELAS_DISPLAY.pisca_config)
	{
		TELAS_DISPLAY.pisca_config=8;
		TELAS_DISPLAY.estado_pisca^=1;
		TELAS_DISPLAY.atualizar_tela=1;
	}
	
	/*if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
	}*/
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_UP:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				if(!TELAS_DISPLAY.indice_tela_config)
				{
					SET_TIME_DATE.hora++;
					if(SET_TIME_DATE.hora>23) SET_TIME_DATE.hora=0;
				}
				else
				{
					SET_TIME_DATE.minuto++;
					if(SET_TIME_DATE.minuto>59) SET_TIME_DATE.minuto=0;
				}
				break;
			}
			case TECLA_DOWN:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				if(!TELAS_DISPLAY.indice_tela_config)
				{
					
					if(SET_TIME_DATE.hora) SET_TIME_DATE.hora--;
					else SET_TIME_DATE.hora=23;
				}
				else
				{
					if(SET_TIME_DATE.minuto) SET_TIME_DATE.minuto--;
					else SET_TIME_DATE.minuto=23;
				}
				break;
			}
			case TECLA_E:
			{
				TELAS_DISPLAY.indice_tela_config++;
				if(TELAS_DISPLAY.indice_tela_config>1) 
				{
					//bq32000_set_date_time();
					bq32000_set_time();
					limpa_display();
					posicao(1,1);
					escreve_string(str_relogio);
					posicao(2,1);
					escreve_string(str_alterado);
					delay_ms(1200);
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				}
				//TELAS_DISPLAY.pisca_config=8;
				//TELAS_DISPLAY.estado_pisca=1;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_P:
			{
				if(TELAS_DISPLAY.indice_tela_config) TELAS_DISPLAY.indice_tela_config--;
				else 
				{
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				}
				//TELAS_DISPLAY.pisca_config=8;
				//TELAS_DISPLAY.estado_pisca=1;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_C:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				break;
			}
		}
	}
	
}
//=============================================================================================
void t_ajuste_data(void)
{
	unsigned char str_ano[]={"Ano:"};
	unsigned char str_mes[]={"Mes:"};
	//unsigned char str_ano[]={"Ano:"};
	unsigned char str_dia[]={"Dia:"};
	unsigned char str_jan[]={"JAN"};
	unsigned char str_fev[]={"FEV"};
	unsigned char str_mar[]={"MAR"};
	unsigned char str_abr[]={"ABR"};
	unsigned char str_mai[]={"MAI"};
	unsigned char str_jun[]={"JUN"};
	unsigned char str_jul[]={"JUL"};
	unsigned char str_ago[]={"AGO"};
	unsigned char str_set[]={"SET"};
	unsigned char str_out[]={"OUT"};
	unsigned char str_nov[]={"NOV"};
	unsigned char str_dez[]={"DEZ"};
	//unsigned char str_meses[][]={str_jan,str,fev,str_mar,str_abr,str_mai,str_jun,str_jul,str_ago,str_set,str_out,str_nov,str_dez};
	unsigned char str_seg[]={"SEG"};
	unsigned char str_ter[]={"TER"};
	unsigned char str_qua[]={"QUA"};
	unsigned char str_qui[]={"QUI"};
	unsigned char str_sex[]={"SEX"};
	unsigned char str_sab[]={"SAB"};
	unsigned char str_dom[]={"DOM"};
	unsigned char str_data[]={"Data"};
	unsigned char str_alterada[]={"Alterada"};
	unsigned char str_valor[3];
	
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		//TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		TELAS_DISPLAY.pisca_config=8;//800 milisegundos
		posicao(1,3);
		//escreve_string(str_ano);
		switch(TELAS_DISPLAY.indice_tela_config)
		{
			case 0:
			{
				escreve_string(str_ano);
				//escreve_numero(str_valor,SET_TIME_DATE.ano,2,0);
				posicao(2,4);
				if(TELAS_DISPLAY.estado_pisca)
				{
					escreve_numero(str_valor,SET_TIME_DATE.ano,2,0);
					escreve_string(str_valor);
				}
				else 
				{
					escreve_lcd(' ');
					escreve_lcd(' ');
				}
				break;
			}
			case 1:
			{
				escreve_string(str_mes);
				//escreve_numero(str_valor,SET_TIME_DATE.ano,2,0);
				posicao(2,3);
				if(TELAS_DISPLAY.estado_pisca)
				{
					switch(SET_TIME_DATE.mes)
					{
						case 1: escreve_string(str_jan);break;
						case 2: escreve_string(str_fev);break;
						case 3: escreve_string(str_mar);break;
						case 4: escreve_string(str_abr);break;
						case 5: escreve_string(str_mai);break;
						case 6: escreve_string(str_jun);break;
						case 7: escreve_string(str_jul);break;
						case 8: escreve_string(str_ago);break;
						case 9: escreve_string(str_set);break;
						case 10: escreve_string(str_out);break;
						case 11: escreve_string(str_nov);break;
						case 12: escreve_string(str_dez);break;
						
					}
				}
				else 
				{
					escreve_lcd(' ');
					escreve_lcd(' ');
				}
				break;
			}
			case 2:
			{
				escreve_string(str_dia);
				//escreve_numero(str_valor,SET_TIME_DATE.dia,2,0);
				posicao(2,4);
				if(TELAS_DISPLAY.estado_pisca)
				{
					escreve_numero(str_valor,SET_TIME_DATE.dia,2,0);
					escreve_string(str_valor);
				}
				else 
				{
					escreve_lcd(' ');
					escreve_lcd(' ');
				}
				break;
			}
			case 3:
			{
				escreve_string(str_dia);
				//escreve_numero(str_valor,SET_TIME_DATE.ano,2,0);
				posicao(2,3);
				if(TELAS_DISPLAY.estado_pisca)
				{
					switch(SET_TIME_DATE.dia_semana)
					{
						case 1: escreve_string(str_dom);break;
						case 2: escreve_string(str_seg);break;
						case 3: escreve_string(str_ter);break;
						case 4: escreve_string(str_qua);break;
						case 5: escreve_string(str_qui);break;
						case 6: escreve_string(str_sex);break;
						case 7: escreve_string(str_sab);break;
						
					}
				}
				else 
				{
					escreve_lcd(' ');
					escreve_lcd(' ');
				}
				break;
			}
		}
	}
	
	/*if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
	}*/
	
	if(!TELAS_DISPLAY.pisca_config)
	{
		TELAS_DISPLAY.pisca_config=8;
		TELAS_DISPLAY.estado_pisca^=1;
		TELAS_DISPLAY.atualizar_tela=1;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_UP:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				switch(TELAS_DISPLAY.indice_tela_config)
				{
					case 0:
					{
						SET_TIME_DATE.ano++;
						if(SET_TIME_DATE.ano>99) SET_TIME_DATE.hora=0;
						break;
					}
					case 1:
					{
						SET_TIME_DATE.mes++;
						if(SET_TIME_DATE.mes>12) SET_TIME_DATE.mes=1;
						break;
					}
					case 2:
					{
						SET_TIME_DATE.dia++;
						if(SET_TIME_DATE.dia>31) SET_TIME_DATE.dia=1;
						break;
					}
					case 3:
					{
						SET_TIME_DATE.dia_semana++;
						if(SET_TIME_DATE.dia_semana>7) SET_TIME_DATE.dia_semana=1;
						break;
					}
				}
				break;
			}
			case TECLA_DOWN:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				switch(TELAS_DISPLAY.indice_tela_config)
				{
					case 0:
					{
						if(SET_TIME_DATE.ano) SET_TIME_DATE.ano--;
						else SET_TIME_DATE.ano=99;
						break;
					}
					case 1:
					{
						if(SET_TIME_DATE.mes>1) SET_TIME_DATE.mes--;
						else SET_TIME_DATE.mes=12;
						break;
					}
					case 2:
					{
						if(SET_TIME_DATE.dia>1) SET_TIME_DATE.dia--;
						else SET_TIME_DATE.dia=31;
						break;
					}
					case 3:
					{
						if(SET_TIME_DATE.dia_semana>1) SET_TIME_DATE.dia_semana--;
						else SET_TIME_DATE.dia_semana=7;
						break;
					}
				}
				
				break;
			}
			case TECLA_E:
			{
				TELAS_DISPLAY.indice_tela_config++;
				if(TELAS_DISPLAY.indice_tela_config>3) 
				{
					//bq32000_set_date_time();
					bq32000_set_date();
					limpa_display();
					posicao(1,3);
					escreve_string(str_data);
					posicao(2,1);
					escreve_string(str_alterada);
					delay_ms(1200);
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				}
				//TELAS_DISPLAY.pisca_config=8;
				//TELAS_DISPLAY.estado_pisca=1;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_P:
			{
				if(TELAS_DISPLAY.indice_tela_config) TELAS_DISPLAY.indice_tela_config--;
				else 
				{
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				}
				//TELAS_DISPLAY.pisca_config=8;
				//TELAS_DISPLAY.estado_pisca=1;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_C:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				break;
			}
		}
	}
}
//=============================================================================================
void t_set_senha(void)
{
	unsigned char str_senha_nv[]={"SenhaNV"};
	unsigned char numeros[5];
//	unsigned long int senha_nova=0;
//	unsigned long int aux=0;
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		posicao(1,2);
		escreve_string(str_senha_nv);
		escreve_numero(numeros,TELAS_DISPLAY.senha_nova,4,0);
		posicao(2,3);
		escreve_string(numeros);
		posicao(2,TELAS_DISPLAY.posicao_cursor_senha);
		comando_lcd(0x0E);
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_CONFIG;
		TELAS_DISPLAY.posicao_cursor_senha=3;
		TELAS_DISPLAY.senha_nova=0;
		comando_lcd(0x0C);
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_E:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.posicao_cursor_senha++;
				if(TELAS_DISPLAY.posicao_cursor_senha>6)
				{
					TELAS_DISPLAY.posicao_cursor_senha=3;
					comando_lcd(0x0C);
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_SALVAR_SENHA;
					//TELAS_DISPLAY.senha_nova=0;
				}
				break;
			}
			case TECLA_UP:
			{
				TELAS_DISPLAY.senha_nova=inc_senha(TELAS_DISPLAY.posicao_cursor_senha,TELAS_DISPLAY.senha_nova);
				/*switch(TELAS_DISPLAY.posicao_cursor_senha)
				{
					case 3: 
					{
						aux=TELAS_DISPLAY.senha_nova/1000;
						TELAS_DISPLAY.senha_nova-=(aux*1000);
						aux++;
						if(aux>9) aux=0;
						TELAS_DISPLAY.senha_nova+=(aux*1000);
						break;
					}
					case 4: 
					{
						aux=((TELAS_DISPLAY.senha_nova%1000)/100);
						TELAS_DISPLAY.senha_nova-=(aux*100);
						aux++;
						if(aux>9) aux=0;
						TELAS_DISPLAY.senha_nova+=(aux*100);
						break;
					}
					case 5: 
					{
						aux=(((TELAS_DISPLAY.senha_nova%1000)%100)/10);
						TELAS_DISPLAY.senha_nova-=(aux*10);
						aux++;
						if(aux>9) aux=0;
						TELAS_DISPLAY.senha_nova+=(aux*10);
						break;
					}case 6: 
					{
						aux=(((TELAS_DISPLAY.senha_nova%1000)%100)%10);
						TELAS_DISPLAY.senha_nova-=aux;
						aux++;
						if(aux>9) aux=0;
						TELAS_DISPLAY.senha_nova+=aux;
						break;
					}
				}*/
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_DOWN:
			{
				TELAS_DISPLAY.senha_nova=dec_senha(TELAS_DISPLAY.posicao_cursor_senha,TELAS_DISPLAY.senha_nova);
				/*switch(TELAS_DISPLAY.posicao_cursor_senha)
				{
					case 3: 
					{
						aux=TELAS_DISPLAY.senha_nova/1000;
						TELAS_DISPLAY.senha_nova-=(aux*1000);
						if(aux) aux--;
						else aux=9;
						TELAS_DISPLAY.senha_nova+=(aux*1000);
						break;
					}
					case 4: 
					{
						aux=((TELAS_DISPLAY.senha_nova%1000)/100);
						TELAS_DISPLAY.senha_nova-=(aux*100);
						if(aux) aux--;
						else aux=9;
						TELAS_DISPLAY.senha_nova+=(aux*100);
						break;
					}
					case 5: 
					{
						aux=(((TELAS_DISPLAY.senha_nova%1000)%100)/10);
						TELAS_DISPLAY.senha_nova-=(aux*10);
						if(aux) aux--;
						else aux=9;
						TELAS_DISPLAY.senha_nova+=(aux*10);
						break;
					}case 6: 
					{
						aux=(((TELAS_DISPLAY.senha_nova%1000)%100)%10);
						TELAS_DISPLAY.senha_nova-=aux;
						if(aux) aux--;
						else aux=9;
						TELAS_DISPLAY.senha_nova+=aux;
						break;
					}
				}*/
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_C:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
				TELAS_DISPLAY.posicao_cursor_senha=3;
				TELAS_DISPLAY.senha_nova=0;
				break;
			}
			case TECLA_P:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				if(TELAS_DISPLAY.posicao_cursor_senha>3)TELAS_DISPLAY.posicao_cursor_senha--;
				else
				{
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_ALARMES_CONFIG;
					comando_lcd(0x0C);
					TELAS_DISPLAY.posicao_cursor_senha=3;
					TELAS_DISPLAY.senha_nova=0;
				}
				break;
			}
		}
	}
}	
//==============================================================================================
void t_reset(void)
{
	unsigned char str_zerar[]={"Zerar"};
	unsigned char str_alarmes[]={"Alarmes"};
	unsigned char str_operacao[]={"Operacao"};
	unsigned char str_abortada[]={"Abortada"};
	unsigned char str_apagado[]={"Apagado!"};
	unsigned char str_apagando[]={"Apagando"};
	unsigned int aux=0;
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		posicao(1,2);
		escreve_string(str_zerar);
		posicao(2,1);
		escreve_string(str_alarmes);
		escreve_lcd('?');
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_CONFIG;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_C:
			case TECLA_P:
			case TECLA_UP:
			case TECLA_DOWN:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				limpa_display();
				posicao(1,1);
				escreve_string(str_operacao);
				posicao(2,1);
				escreve_string(str_abortada);
				delay_ms(1200);
				break;
			}
			case TECLA_E:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				limpa_display();
				posicao(1,1);
				escreve_string(str_apagando);
				posicao(2,1);
				escreve_string(str_alarmes);
				zerar_alarmes();
				for(aux=0;aux<55;aux++)
				{
					ALARMES_KT[aux].at_in=0;
					ALARMES_KT[aux].seg_min=0;
					ALARMES_KT[aux].minuto=0;
					ALARMES_KT[aux].hora=0;
					ALARMES_KT[aux].melodia_rl_low=0;
					ALARMES_KT[aux].duracao=0;
					ALARMES_KT[aux].melodia_rl_high=0;
					ALARMES_KT[aux].dias=0x7F;
				}
				delay_ms(400);
				limpa_display();
				posicao(1,1);
				escreve_string(str_apagado);
				delay_ms(1000);
				break;
			}
		}
	}
}
//========================================================================================
void t_salvar_senha(void)
{
	unsigned char str_gravar[]={"Gravar"};
	unsigned char str_senha[]={"Senha"};
	unsigned char str_alterada[]={"Alterada"};
	unsigned char str_operacao[]={"Operacao"};
	unsigned char str_abortada[]={"Abortada"};
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		posicao(1,2);
		escreve_string(str_gravar);
		posicao(2,2);
		escreve_string(str_senha);
		escreve_lcd('?');
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_CONFIG;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_C:
			case TECLA_P:
			case TECLA_UP:
			case TECLA_DOWN:
			{
				TELAS_DISPLAY.senha_nova=0;
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				limpa_display();
				posicao(1,1);
				escreve_string(str_operacao);
				posicao(2,1);
				escreve_string(str_abortada);
				delay_ms(1200);
				break;
			}
			case TECLA_E:
			{
				TELAS_DISPLAY.senha=TELAS_DISPLAY.senha_nova;
				write_senha(TELAS_DISPLAY.senha_nova);
				TELAS_DISPLAY.senha_nova=0;
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				limpa_display();
				posicao(1,2);
				escreve_string(str_senha);
				posicao(2,1);
				escreve_string(str_alterada);
				delay_ms(1200);
				break;
			}
		}
	}
}
//===============================================================================================
void mostra_dias(unsigned int rot)
{
	if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & rot) escreve_lcd('S');
	else  escreve_lcd('s');
	rot>>=1;
	if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & rot) escreve_lcd('T');
	else  escreve_lcd('t');
	rot>>=1;
	if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & rot) escreve_lcd('Q');
	else  escreve_lcd('q');
	rot>>=1;
	if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & rot) escreve_lcd('Q');
	else  escreve_lcd('q');
	rot>>=1;
	if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & rot) escreve_lcd('S');
	else  escreve_lcd('s');
	rot>>=1;
	if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & rot) escreve_lcd('S');
	else  escreve_lcd('s');
	rot>>=1;
	if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].dias & rot) escreve_lcd('D');
	else  escreve_lcd('d');
}
//================================================================================================
void mostra_estado_alarmes(unsigned int indice,unsigned int num_alarme)
{
	unsigned char str_lig[]={"LIG"};
	unsigned char str_des[]={"DES"};
	unsigned char str_al[]={"AL"};
	unsigned char str_som[]={"SOM"};
	unsigned char str_rl1[]={"RL1"};
	unsigned char str_rl2[]={"RL2"};
	unsigned char str_num[3];
	unsigned int aux=0;
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		//limpa_display();
		//TELAS_DISPLAY.tempo_retorna_tela_anterior=130;//13 segundos
		posicao(1,1);
		escreve_string(str_al);
		escreve_numero(str_num,num_alarme,2,0);
		escreve_string(str_num);
		posicao(1,6);
		if(!indice)
		{
			//if(ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].at_in) escreve_string(str_lig);
			if(ALARMES_KT[num_alarme].at_in) escreve_string(str_lig);
			else escreve_string(str_des);
			posicao(2,2);
			escreve_numero(str_num,ALARMES_KT[num_alarme].hora,2,0);
			escreve_string(str_num);
			escreve_lcd(':');
			escreve_numero(str_num,ALARMES_KT[num_alarme].minuto,2,0);
			escreve_string(str_num);
		}
		else
		{
			if(ALARMES_KT[num_alarme].melodia_rl_b_high) aux=1;
			else aux=0;
			//aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high;
			aux<<=2;
			aux|=ALARMES_KT[num_alarme].melodia_rl_high;
			aux<<=2;
			aux|=ALARMES_KT[num_alarme].melodia_rl_low;
			switch(aux)
			{
				case MELODIA_0:
				case MELODIA_1:
				case MELODIA_2:
				case MELODIA_3:
				case MELODIA_4:
				case MELODIA_5:
				case MELODIA_6:
			    case MELODIA_7:
		        case MELODIA_8:
	            case MELODIA_9:
				case MELODIA_10:
				case MELODIA_11:
				case MELODIA_12:
				case MELODIA_13:
				case MELODIA_14:
				case MELODIA_15:
				case MELODIA_16:
			    case MELODIA_17:
		        case MELODIA_18:
	            case MELODIA_19:
					 {
						 escreve_string(str_som);
						 break;
					 }
				case RL_1:
					 {
						 escreve_string(str_rl1);
						 break;
					 }
				case RL_2:
					 {
						 escreve_string(str_rl2);
						 break;
					 }
			}
			posicao(2,1);
			aux=0x40;
			mostra_dias(aux);
			
		}
	}
}
//=======================================================================================================
unsigned long int inc_senha(unsigned int indice_s,unsigned long int valor_atual)
{
	unsigned long int aux=0;
	
	switch(indice_s)
	{
		case 3: 
		{
			aux=valor_atual/1000;
			valor_atual-=(aux*1000);
			aux++;
			if(aux>9) aux=0;
			valor_atual+=(aux*1000);
			break;
		}
		case 4: 
		{
			aux=((valor_atual%1000)/100);
			valor_atual-=(aux*100);
			aux++;
			if(aux>9) aux=0;
			valor_atual+=(aux*100);
			break;
		}
		case 5: 
		{
			aux=(((valor_atual%1000)%100)/10);
			valor_atual-=(aux*10);
			aux++;
			if(aux>9) aux=0;
			valor_atual+=(aux*10);
			break;
		}case 6: 
		{
			aux=(((valor_atual%1000)%100)%10);
			valor_atual-=aux;
			aux++;
			if(aux>9) aux=0;
			valor_atual+=aux;
			break;
		}
	}
	return valor_atual;
}
//=========================================================================================================
unsigned long int dec_senha(unsigned int indice_s,unsigned long int valor_atual)
{
	unsigned long int aux=0;
	
	switch(indice_s)
	{
		case 3: 
		{
			aux=valor_atual/1000;
			valor_atual-=(aux*1000);
			if(aux) aux--;
			else aux=9;
			valor_atual+=(aux*1000);
			break;
		}
		case 4: 
		{
			aux=((valor_atual%1000)/100);
			valor_atual-=(aux*100);
			if(aux) aux--;
			else aux=9;
			valor_atual+=(aux*100);
			break;
		}
		case 5: 
		{
			aux=(((valor_atual%1000)%100)/10);
			valor_atual-=(aux*10);
			if(aux) aux--;
			else aux=9;
			valor_atual+=(aux*10);
			break;
		}case 6: 
		{
			aux=(((valor_atual%1000)%100)%10);
			valor_atual-=aux;
			if(aux) aux--;
			else aux=9;
			valor_atual+=aux;
			break;
		}
	}
	return valor_atual;
}
//=====================================================================================
void t_alarme_ativo(void)
{
	unsigned char str_alarme[]={"Alarme"};
	unsigned char str_seg[]={" Seg"};
	unsigned char str_min[]={" Min"};
	unsigned char str_num[3];
	static unsigned int reproduzindo_musica=0;
	
	unsigned int aux=0;
	
	if(!input(BUSY) && reproduzindo_musica)//Verifica se a musica acabou 
	{
		if(ALARMES_KT[TELAS_DISPLAY.alarme_ativo_atual].melodia_rl_b_high) aux=1;
		else aux=0;
		//aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high;
		aux<<=2;
		aux|=ALARMES_KT[TELAS_DISPLAY.alarme_ativo_atual].melodia_rl_high;
		aux<<=2;
		aux|=ALARMES_KT[TELAS_DISPLAY.alarme_ativo_atual].melodia_rl_low;
		unmute_Wtv020sd();
		delay_ms(10);
		asyncPlayVoice_Wtv020sd(aux);
		delay_ms(10);
	}
	
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.contar_tempo_ativo=1;
		posicao(1,1);
		escreve_string(str_alarme);
		escreve_numero(str_num,TELAS_DISPLAY.alarme_ativo_atual,2,0);
		escreve_string(str_num);
		posicao(2,2);
		escreve_numero(str_num,TELAS_DISPLAY.duracao_alarme_ativo,2,0);
		escreve_string(str_num);
		posicao(2,4);
		if(!TELAS_DISPLAY.seg_min_alarme_ativo)
		{
			escreve_string(str_seg);
		}
		else
		{
			escreve_string(str_min);
		}
		if(ALARMES_KT[TELAS_DISPLAY.alarme_ativo_atual].melodia_rl_b_high) aux=1;
		else aux=0;
		//aux|=ALARMES_KT[TELAS_DISPLAY.escolhe_alarme_num].melodia_rl_b_high;
		aux<<=2;
		aux|=ALARMES_KT[TELAS_DISPLAY.alarme_ativo_atual].melodia_rl_high;
		aux<<=2;
		aux|=ALARMES_KT[TELAS_DISPLAY.alarme_ativo_atual].melodia_rl_low;
		switch(aux)
		{
			case MELODIA_0:
			case MELODIA_1:
			case MELODIA_2:
			case MELODIA_3:
			case MELODIA_4:
			case MELODIA_5:
			case MELODIA_6:
			case MELODIA_7:
			case MELODIA_8:
			case MELODIA_9:
			case MELODIA_10:
			case MELODIA_11:
			case MELODIA_12:
			case MELODIA_13:
			case MELODIA_14:
			case MELODIA_15:
			case MELODIA_16:
			case MELODIA_17:
			case MELODIA_18:
			case MELODIA_19:
			{
				if(!reproduzindo_musica)
				{
					reproduzindo_musica=1;
					unmute_Wtv020sd();
					delay_ms(10);
					asyncPlayVoice_Wtv020sd(aux);
					delay_ms(10);
					output_high(LED_MELODIA);
				}
				break;
			}
			case RL_1: output_high(RELE_1);break;
			case RL_2: output_high(RELE_2);break;
		}
		
	}
	
	if(!TELAS_DISPLAY.duracao_alarme_ativo)
	{
		TELAS_DISPLAY.contar_tempo_ativo=0;
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
		reproduzindo_musica=0;
		stopVoice_Wtv020sd();
		output_low(LED_MELODIA);
		output_low(RELE_1);
		output_low(RELE_2);
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_C:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_PRINCIPAL;
				stopVoice_Wtv020sd();
				reproduzindo_musica=0;
				output_low(LED_MELODIA);
				output_low(RELE_1);
				output_low(RELE_2);
				break;
			}
		}
	}
	
}
//========================================================================================
void verifica_alarmes(void)
{
	unsigned int teste_alarme=0;
	unsigned int aux=0;
	
	for(;teste_alarme<55;teste_alarme++)
	{
		if(ALARMES_KT[teste_alarme].at_in)
		{
			if(ALARMES_KT[teste_alarme].hora==GET_TIME_DATE.hora && ALARMES_KT[teste_alarme].minuto==GET_TIME_DATE.minuto && TELAS_DISPLAY.ultimo_alarme_ativo!=teste_alarme)
			{
				if(GET_TIME_DATE.dia_semana==0x01 && (ALARMES_KT[teste_alarme].dias&0x01)) aux=1;
				else if(GET_TIME_DATE.dia_semana==0x02 && (ALARMES_KT[teste_alarme].dias&0x40)) aux=1;
				else if(GET_TIME_DATE.dia_semana==0x03 && (ALARMES_KT[teste_alarme].dias&0x20)) aux=1;
				else if(GET_TIME_DATE.dia_semana==0x04 && (ALARMES_KT[teste_alarme].dias&0x10)) aux=1;
				else if(GET_TIME_DATE.dia_semana==0x05 && (ALARMES_KT[teste_alarme].dias&0x08)) aux=1;
				else if(GET_TIME_DATE.dia_semana==0x06 && (ALARMES_KT[teste_alarme].dias&0x04)) aux=1;
				else if(GET_TIME_DATE.dia_semana==0x07 && (ALARMES_KT[teste_alarme].dias&0x02)) aux=1;
				if(aux)
				{
					TELAS_DISPLAY.ultimo_alarme_ativo=TELAS_DISPLAY.alarme_ativo_atual=teste_alarme;
					TELAS_DISPLAY.duracao_alarme_ativo=ALARMES_KT[teste_alarme].duracao;
					TELAS_DISPLAY.seg_min_alarme_ativo=ALARMES_KT[teste_alarme].seg_min;
					TELAS_DISPLAY.atualizar_tela=1;
					TELAS_DISPLAY.esc_tela=TELA_ALARME_ATIVO;
				}
				break;
			}
		}
	}
}
//===========================================================================================
void t_config_reg_bq(void)
{
	unsigned char str_valor_C[]={"Valor_C:"};
	unsigned char str_valor[]={"Valor"};
	unsigned char str_alterado[]={"Alterado"};
	//unsigned int valor_modificado=0;
	unsigned char str_num[5];
//	unsigned char aux=0;
//	long long aux2=0;
	//static unsigned char tecla_press_tela=0;
	
	if(TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;
		//TELAS_DISPLAY.contar_tempo_ativo=1;
		posicao(1,1);
		escreve_string(str_valor_C);
		posicao(2,1);
		escreve_numero_sinal(str_num,valor_calib_reg_bq,4,0);
		posicao(2,3);
		escreve_string(str_num);
	}
	
	if(TELAS_DISPLAY.tecla_press_tela && !TELAS_DISPLAY.conta_tela_freq) 
	{
		TELAS_DISPLAY.tecla_press_tela=0;
		TELAS_DISPLAY.conta_tela_freq=0;
		//TELAS_DISPLAY.conta_tela_freq=40;
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_FREQ;
	}
	
	if(!TELAS_DISPLAY.tempo_retorna_tela_anterior)
	{
		TELAS_DISPLAY.atualizar_tela=1;
		TELAS_DISPLAY.esc_tela=TELA_CONFIG;
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_UP:
			{
				//valor_calib_reg_bq++;
				inc_dec_calib++;
				if(inc_dec_calib>31) inc_dec_calib=31;
				if(inc_dec_calib>0) valor_calib_reg_bq=inc_dec_calib*2.0345;
				else valor_calib_reg_bq=inc_dec_calib*4.069;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_DOWN:
			{
				inc_dec_calib--;
				if(inc_dec_calib<-31) inc_dec_calib=-31;
				if(inc_dec_calib>0) valor_calib_reg_bq=inc_dec_calib*2.0345;
				else valor_calib_reg_bq=inc_dec_calib*4.069;
				TELAS_DISPLAY.atualizar_tela=1;
				break;
			}
			case TECLA_C:
			//case TECLA_P:
			{
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				
				break;
			}
			case TECLA_E:
			{
				//valor_modificado|=0x40;
				//if(valor_calib_reg_bq<0) valor_modificado|=0x20;
				//valor_modificado|=(valor_calib_reg_bq&0x1f);
				bq32000_set_calib(inc_dec_calib);
				write_calib_relogio(valor_calib_reg_bq);
				limpa_display();
				posicao(1,3);
				escreve_string(str_valor);
				posicao(2,1);
				escreve_string(str_alterado);
				delay_ms(1500);
				valor_calib_reg_bq=bq32000_read_calib();
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				break;
			}
			case TECLA_P:
			{
				if(!TELAS_DISPLAY.tecla_press_tela)
				{
					TELAS_DISPLAY.tecla_press_tela=1;
					TELAS_DISPLAY.conta_tela_freq=40;
				}
				break;
			}
			case TECLA_SOLTA:
			{
				if(TELAS_DISPLAY.tecla_press_tela) 
				{
					TELAS_DISPLAY.tecla_press_tela=0;
					TELAS_DISPLAY.conta_tela_freq=0;
				}
				break;
			}
		}
	}
	
}
//=================================================================================================
void t_freq(void)
{
	unsigned char str_freq[]={"FREQ:"};
//	unsigned char str_hz[]={"Hz"};
	unsigned long long valor=0;
	unsigned char str_num[6];
	
	if(TELAS_DISPLAY.at_tela_freq || TELAS_DISPLAY.atualizar_tela)
	{
		TELAS_DISPLAY.atualizar_tela=0;
		TELAS_DISPLAY.at_tela_freq=0;
		
		limpa_display();
		TELAS_DISPLAY.tempo_retorna_tela_anterior=130;
		//TELAS_DISPLAY.contar_tempo_ativo=1;
		posicao(1,2);
		escreve_string(str_freq);
		valor=CONT_FREQ.diferenca;
		//valor*=10;
		//valor/=3125;
		//valor=10000/valor;
		escreve_numero(str_num,valor,5,0);
		posicao(2,1);
		escreve_string(str_num);
		//escreve_string(str_hz);
		
	}
	
	if(TECLADO.tratar_tecla)
	{
		TECLADO.tratar_tecla=0;
		switch(TECLADO.tecla_pressionada)
		{
			case TECLA_C:
			{
				TELAS_DISPLAY.tecla_press_tela=0;
				TELAS_DISPLAY.conta_tela_freq=0;
				TELAS_DISPLAY.atualizar_tela=1;
				TELAS_DISPLAY.esc_tela=TELA_CONFIG;
				
				break;
			}
		}
	}
}