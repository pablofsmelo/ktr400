#ifndef _WTV020SD_LIB_H
#define _WTV020SD_LIB_H

#define SDO		PIN_C5
#define SCK 	PIN_C3
#define RST		PIN_B7
#define BUSY	PIN_E3

//Definições do modulo
#define PLAY_PAUSE  0xFFFE
#define STOP 		0xFFFF
#define VOLUME_MIN  0xFFF0
#define VOLUME_MAX  0xFFF7


void init_Wtv020sd(void);
void send_comand_Wtv020sd(unsigned long int command);
void playVoice_Wtv020sd(unsigned long int voiceNumber);
void asyncPlayVoice_Wtv020sd(unsigned long int voiceNumber);
void stopVoice_Wtv020sd(void);
void pauseVoice_Wtv020sd(void);
void mute_Wtv020sd(void);
void unmute_Wtv020sd(void);
void setVolume_Wtv020sd(unsigned long int volume);

//============================================
void init_Wtv020sd(void)
{
	output_high(SCK);
	output_high(RST);
	//Reset pulse.
	output_low(RST);
	delay_ms(5);
	output_high(RST);
	delay_ms(300);
}
//=================================================
void send_comand_Wtv020sd(unsigned long int command)
{
	unsigned long int mask = 0x8000;
	
	//Start bit Low level pulse.
	output_low(SCK);
	// Wait
	delay_us(1950);
	for (; mask > 0; mask >>= 1) 
	{
		//Clock low level pulse.
		output_low(SCK);
		delay_us(50);
		//Write data setup.
		if (command & mask) 
		{
		  output_high(SDO);
		} 
		else 
		{
		  output_low(SDO);
		}
		delay_us(50);
		//Clock high level pulse.
		output_high(SCK);
		delay_us(100);
    }
	delay_us(1900);
}
//====================================================
void playVoice_Wtv020sd(unsigned long int voiceNumber)
{
	unsigned int busy_pin=0;
	
	send_comand_Wtv020sd(voiceNumber);
	// Wait 20ms for busy to be active
	delay_ms(20);
	// Wait till song ends
	busy_pin=input(BUSY);
	while(busy_pin==1)
	{
		busy_pin=input(Busy);
	}
}
//=========================================================
void asyncPlayVoice_Wtv020sd(unsigned long int voiceNumber)
{
  send_comand_Wtv020sd(voiceNumber);
}
//======================================================
void stopVoice_Wtv020sd(void)
{
  send_comand_Wtv020sd(STOP);
}
//=================================================
void pauseVoice_Wtv020sd(void)
{
  send_comand_Wtv020sd(PLAY_PAUSE);
}
//===================================================
void mute_Wtv020sd(void)
{
  send_comand_Wtv020sd(VOLUME_MIN);
}
//====================================================
void unmute_Wtv020sd(void)
{
  send_comand_Wtv020sd(VOLUME_MAX);
}
//=================================================
void setVolume_Wtv020sd(unsigned long int volume)
{
	unsigned long int currentVolume=0;
  if(volume < 0x0008) 
  {
    currentVolume = VOLUME_MIN + volume;
    send_comand_Wtv020sd(currentVolume);
  }
}

#endif