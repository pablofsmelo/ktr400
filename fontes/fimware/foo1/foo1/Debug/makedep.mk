################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

src\ApplicationDef.c

src\ASF\common\services\serial\usart_serial.c

src\ASF\common\services\usb\class\cdc\device\udi_cdc.c

src\ASF\common\services\usb\udc\udc.c

src\ASF\common\utils\stdio\read.c

src\ASF\common\utils\stdio\write.c

src\ASF\sam\drivers\usbc\usbc_device.c

src\ASF\sam\drivers\wdt\wdt_sam4l.c

src\ConfigHW_PlatformARM.c

src\Display7seg_PlatformARM.c

src\DisplayLCD_PlatformARM.c

src\RS485_PlatformARM.c

src\RtcDS1307_PlatformARM.c

src\SensorLM35_PlatformARM.c

src\SPI_PlatformARM.c

src\ASF\common\services\usb\class\cdc\device\udi_cdc_desc.c

src\ASF\sam\drivers\tc\tc.c

src\ASF\sam\drivers\twim\twim.c

src\ASF\sam\drivers\usart\usart.c

src\ASF\sam\drivers\eic\eic.c

src\ASF\sam\drivers\gpio\gpio.c

src\ASF\common\services\delay\sam\cycle_counter.c

src\ASF\common\services\sleepmgr\sam4l\sleepmgr.c

src\ASF\sam\drivers\dacc\dacc.c

src\ASF\common\services\clock\sam4l\dfll.c

src\ASF\common\services\clock\sam4l\osc.c

src\ASF\common\services\clock\sam4l\pll.c

src\ASF\common\services\clock\sam4l\sysclk.c

src\ASF\sam\drivers\adcife\adcife.c

src\ASF\sam\drivers\bpm\bpm.c

src\ASF\sam\drivers\flashcalw\flashcalw.c

src\ASF\sam\drivers\pdca\pdca.c

src\ASF\common\boards\user_board\init.c

src\ASF\common\utils\interrupt\interrupt_sam_nvic.c

src\ASF\sam\utils\cmsis\sam4l\source\templates\exceptions.c

src\ASF\sam\utils\cmsis\sam4l\source\templates\gcc\startup_sam4l.c

src\ASF\sam\utils\cmsis\sam4l\source\templates\system_sam4l.c

src\ASF\sam\utils\syscalls\gcc\syscalls.c

src\IO_PlatformARM.c

src\main.c

