/**
 * \file
 *
 * \brief User board initialization template
 *
 */
 /**
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>

#define ioport_set_pin_peripheral_mode(pino, mode) \
do {\
	ioport_set_pin_mode(pino, mode);\
	ioport_disable_pin(pino);\
} while (0)

void board_init(void)
{
	/* This function is meant to contain board-specific initialization code
	 * for, e.g., the I/O pins. The initialization can rely on application-
	 * specific board configuration, found in conf_board.h.
	 */

	
	// configure UART pins
	//ioport_set_port_mode(IOPORT_PIOA, PIO_PA9A_URXD0 | PIO_PA10A_UTXD0, IOPORT_MODE_MUX_A);
	//ioport_disable_port(IOPORT_PIOA, PIO_PA9A_URXD0 | PIO_PA10A_UTXD0);
	
	ioport_set_pin_dir(LM35_PIN,IOPORT_DIR_INPUT);
	ioport_set_pin_peripheral_mode(LM35_PIN, LM35_ACC_MUX);
	
	ioport_set_pin_dir(RD_485,IOPORT_DIR_OUTPUT);
	ioport_set_pin_peripheral_mode(RX_485, MUX_RX_485);
	ioport_set_pin_peripheral_mode(TX_485, MUX_TX_485);
	
	ioport_set_pin_dir(BUTTON_1,IOPORT_DIR_INPUT);
	ioport_set_pin_dir(BUTTON_2,IOPORT_DIR_INPUT);
	ioport_set_pin_dir(BUTTON_3,IOPORT_DIR_INPUT);
	ioport_set_pin_dir(BUTTON_4,IOPORT_DIR_INPUT);
	ioport_set_pin_dir(BUTTON_5,IOPORT_DIR_INPUT);
	ioport_set_pin_dir(BUTTON_6,IOPORT_DIR_INPUT);
	ioport_set_pin_dir(BUTTON_7,IOPORT_DIR_INPUT);
	ioport_set_pin_dir(BUTTON_8,IOPORT_DIR_INPUT);
	
	ioport_set_pin_mode(BUTTON_1, IOPORT_MODE_PULLUP);
	ioport_set_pin_mode(BUTTON_2, IOPORT_MODE_PULLUP);
	ioport_set_pin_mode(BUTTON_3, IOPORT_MODE_PULLUP);
	ioport_set_pin_mode(BUTTON_4, IOPORT_MODE_PULLUP);
	ioport_set_pin_mode(BUTTON_5, IOPORT_MODE_PULLUP);
	ioport_set_pin_mode(BUTTON_6, IOPORT_MODE_PULLUP);
	ioport_set_pin_mode(BUTTON_7, IOPORT_MODE_PULLUP);
	ioport_set_pin_mode(BUTTON_8, IOPORT_MODE_PULLUP);
	
	//ioport_set_pin_dir(LED_D3,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_D4,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_D5,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_D6,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_D7,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_D8,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_D9,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_D10,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	
	
	ioport_set_pin_dir(RS_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(EN_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D0_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D1_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D2_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D3_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D4_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D5_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D6_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D7_LCD, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
}
