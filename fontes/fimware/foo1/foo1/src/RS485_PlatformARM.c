/*
 * RS_485.c
 *
 * Created: 13/03/2015 16:47:51
 *  Author: User
 */ 
#include "ApplicationDef.h"
#include "RS485_PlatformARM.h"

const sam_usart_opt_t usart_485_console_settings = {
	USART_485_BAUDRATE,
	USART_485_CHAR_LENGTH,
	USART_485_PARITY,
	USART_485_STOP_BIT,
	US_MR_CHMODE_NORMAL
};

void init_485(void)
{
	//usart_init_rs485(USART_485,&usart_485_console_settings,sysclk_get_cpu_hz());
	usart_init_rs232(USART_485,&usart_485_console_settings,sysclk_get_cpu_hz());
	usart_enable_tx(USART_485);
	usart_enable_rx(USART_485);
	//usart_drive_RTS_pin_low(USART_485);
	ioport_set_pin_level(RD_485,IOPORT_PIN_LEVEL_LOW);
}

void enable_clock_485(void)
{
	sysclk_enable_peripheral_clock(USART_485);
}

void send_buffer_485(uint8_t *bf, uint8_t leg)
{
	uint8_t qtd=0;
	//usart_drive_RTS_pin_high(USART_485);
	ioport_set_pin_level(RD_485,IOPORT_PIN_LEVEL_HIGH);
	for(qtd=0;qtd<leg;qtd++)
	{
		//ioport_set_pin_level(RD_485,IOPORT_PIN_LEVEL_HIGH);
		//while(!usart_is_tx_buf_empty(USART_485)){}
		//usart_putchar(USART_485,bf[qtd]);
		//while(!usart_is_tx_empty(USART_485)){}
		while(usart_write(USART_485,bf[qtd])){}
	}
	//usart_write_line(USART_485,bf);
	//usart_drive_RTS_pin_low(USART_485);
	//ioport_set_pin_level(RD_485,IOPORT_PIN_LEVEL_HIGH);
	delay_ms(2);
	ioport_set_pin_level(RD_485,IOPORT_PIN_LEVEL_LOW);
}

void single_send_485(uint8_t snd)
{
	//usart_drive_RTS_pin_high(USART_485);
	ioport_set_pin_level(RD_485,IOPORT_PIN_LEVEL_HIGH);
	usart_write(USART_485,snd);
	//usart_drive_RTS_pin_low(USART_485);
	ioport_set_pin_level(RD_485,IOPORT_PIN_LEVEL_LOW);
}

void init_interrupt_485(void)
{
	usart_enable_interrupt(USART_485, US_IER_RXRDY);
	//NVIC_EnableIRQ(1);
	irq_register_handler(USART0_IRQn,1);
}
