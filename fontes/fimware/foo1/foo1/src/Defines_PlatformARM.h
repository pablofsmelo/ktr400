/*
 * Defines_PlatformARM.h
 *
 * Created: 25/09/2017 17:59:12
 *  Author: Pablo Melo
 */ 

#ifndef DEFINES_PLATFORMARM_H_
#define DEFINES_PLATFORMARM_H_

/*-------------- Peripherals Platform KNNT --------------*/

/* Defines to access ledPlatform ARM */
#define D2		0 /* LED_D3  (Schematics) PIN_PA04(uC)	*/
#define D3		1 /* LED_D4  (Schematics) PIN_PA05(uC)  */
#define D4		2 /* LED_D5  (Schematics) PIN_PA06(uC)  */
#define D5		3 /* LED_D6  (Schematics) PIN_PA07(uC)  */
#define D6		4 /* LED_D7  (Schematics) PIN_PA08(uC)  */
#define D7		5 /* LED_D8  (Schematics) PIN_PA09(uC)  */
#define D8		6 /* LED_D9  (Schematics) PIN_PA10(uC)  */
#define D9		7 /* LED_D10 (Schematics) PIN_PA11(uC)  */

#define BT1		0 /* BUTTON_1 (Schematics) PIN_PA15(uC)	*/
#define BT2		1 /* BUTTON_2 (Schematics) PIN_PA16(uC)	*/
#define BT3		2 /* BUTTON_3 (Schematics) PIN_PA17(uC)	*/
#define BT4		3 /* BUTTON_4 (Schematics) PIN_PA18(uC)	*/
#define BT5		4 /* BUTTON_5 (Schematics) PIN_PA19(uC)	*/
#define BT6		5 /* BUTTON_6 (Schematics) PIN_PA20(uC)	*/
#define BT7		6 /* BUTTON_7 (Schematics) PIN_PA13(uC)	*/
#define BT8		7 /* BUTTON_8 (Schematics) PIN_PA14(uC)	*/


/* Defines to DisplayLCD_PlatormARM.h and .c */
#define TIME_DISPLAY_100us			100
#define TIME_DISPLAY_200us			200
#define TIME_DISPLAY_300us			300
#define TIME_DISPLAY_500us			500

#define TIME_DISPLAY_40ms			 40
#define TIME_DISPLAY_5ms			  5

#define START_DISPLAY_CMD		   0x03
#define RETURN_TO_HOME_CMD		   0x02
#define CONFIG_4_BITS_CMD		   0x28
#define DISPLAY_ON_CMD			   0x0C
#define CLEAR_DISPLAY_CMD		   0x01

/*-------------- Config Hardware ATSAM4L2BA --------------*/

/* Defines for ADC Converter */
#define ENABLE_ADC_CLOCK()	sysclk_enable_peripheral_clock(ADC_USER)
#define DISABLE_ADC_CLOCK()	sysclk_disable_peripheral_clock(ADC_USER)
#define TIME_500us			249
#define TIME_1ms			499
#define TIME_10ms			4999
#define LM35_CHANNEL		0

/* Defines for TC Timer*/
#define Timer_tc_enable()		sysclk_enable_peripheral_clock(TC_TIMER)
#define Timer_tc_disable()		sysclk_disable_peripheral_clock(TC_TIMER)
#define Timer_tc_start()		tc_start(TC_TIMER, TC_CHANNEL_TIMER)
#define Timer_tc_stop()			tc_stop(TC_TIMER,TC_CHANNEL_TIMER)
#define Timer_tc_init()			tc_init(TC_TIMER,TC_CHANNEL_TIMER,(TC_CMR_TCCLKS_TIMER_CLOCK3|TC_CMR_CPCTRG_1|TC_CMR_BURST_NOT_GATED))
#define Timer_tc_config_rc()	tc_write_rc(TC_TIMER,TC_CHANNEL_TIMER,60000)
#define Timer_init_interrupt()  tc_enable_interrupt(TC_TIMER,TC_CHANNEL_TIMER,TC_IER_CPCS_1)

#endif /* DEFINES_PLATFORMARM_H_ */