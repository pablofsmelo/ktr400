/*
 */ 

/*  Bibliotecas
* Adicionar todos #includes aqui
*
***************************************************/
#include "ApplicationDef.h"

/*  Defini��es
* Adicionar todos #defines aqui
*
***************************************************/



/*  Prot�tipos de Fun��es
* Adicionar todos os prot�tipos de fun��es chamadas
* por este m�dulo, exceto para ISR's.
*
***************************************************/

bool nearlyEqual(float a, float b, float epsilon)
{
	if (((a - epsilon) < b) && ((a + epsilon) > b))
	{
		return true;
	}
	else
	{
		return false;
	}
}