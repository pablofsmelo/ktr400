/*
 * DisplayLCD_PlatformARM.c
 *
 * Created: 03/10/2017 09:20:48
 *  Author: Pablo Melo
 */ 

#include "DisplayLCD_PlatformARM.h"
#include "Defines_PlatformARM.h"
#include <asf.h>

uint32_t acc_var;

void lcdSendNibble(char data_bit){
	
	if(data_bit&0x01) ioport_set_pin_level(D4_LCD, HIGH); 
	else			  ioport_set_pin_level(D4_LCD, LOW);  

	if(data_bit&0x02) ioport_set_pin_level(D5_LCD, HIGH); 
	else			  ioport_set_pin_level(D5_LCD, LOW);  

	if(data_bit&0x04) ioport_set_pin_level(D6_LCD, HIGH); 
	else		      ioport_set_pin_level(D6_LCD, LOW);  

	if(data_bit&0x08) ioport_set_pin_level(D7_LCD, HIGH); 
	else			  ioport_set_pin_level(D7_LCD, LOW);  
	
	delay_us(TIME_DISPLAY_200us);
	
	ioport_set_pin_level(EN_LCD, HIGH); 
	delay_us(TIME_DISPLAY_500us);
	ioport_set_pin_level(EN_LCD, LOW); 
	delay_us(TIME_DISPLAY_500us);
}

void lcdSendCmd(char command) {
	ioport_set_pin_level(RS_LCD, LOW);  
	delay_us(TIME_DISPLAY_500us);
	ioport_set_pin_level(EN_LCD, LOW); 
	lcdSendNibble((command>>4)&0x0F);
	delay_us(TIME_DISPLAY_100us);
	lcdSendNibble(command&0x0F);
	delay_us(TIME_DISPLAY_100us);
}

void lcdStart(void) { 
	
	delay_ms(TIME_DISPLAY_40ms);
	
	ioport_set_pin_level(RS_LCD, LOW);  
	ioport_set_pin_level(EN_LCD, LOW);  
	ioport_set_pin_level(D4_LCD, LOW);
	ioport_set_pin_level(D5_LCD, LOW);
	ioport_set_pin_level(D6_LCD, LOW);
	ioport_set_pin_level(D7_LCD, LOW);
	
	lcdSendNibble(START_DISPLAY_CMD);			
	delay_ms(TIME_DISPLAY_5ms);		  
	lcdSendNibble(START_DISPLAY_CMD);  
	delay_us(TIME_DISPLAY_300us);		 
	lcdSendNibble(START_DISPLAY_CMD);  
	lcdSendNibble(RETURN_TO_HOME_CMD);  
	lcdSendCmd(CONFIG_4_BITS_CMD);	  
	lcdSendCmd(DISPLAY_ON_CMD);	  
	lcdSendCmd(CLEAR_DISPLAY_CMD);	  
}

void lcdPrintChar (char data){
	
	ioport_set_pin_level(RS_LCD, HIGH);  
	delay_us(TIME_DISPLAY_500us);
	ioport_set_pin_level(EN_LCD, LOW); 
	lcdSendNibble((data>>4)&0x0F);
	delay_us(TIME_DISPLAY_100us);
	lcdSendNibble(data&0x0F);
	delay_us(TIME_DISPLAY_100us);
}

void lcdSetCursor(char row, char column) {
	if(row == 0x01 && column <= 0x0F) lcdSendCmd(0x80|column);
	if(row == 0x02 && column <= 0x0F) lcdSendCmd(0x80|0x40|column);
}

void lcdPrintString(char *str) {
	int i;
	for(i=0;str[i]!='\0';i++) lcdPrintChar(str[i]);
}	


void writeNumber(unsigned char *str, long long valor, unsigned char len, char c) {
	unsigned char i, x;
	
	for(i=1; i<=len; i++) {
		if(c && i==c+1)
			x = ',';
		else
			x = (valor%10) + 0x30;
		valor /= 10;
		
		str[len-i] = (char)x;
		str[len] = 0;
	}
}


void lcdWriteFloat (float data_value) {
		
	unsigned char i, dig[4], cont_dig;
	unsigned int a;
		
	if(data_value<100.0)					   cont_dig = 3;
	if(data_value>=100.0 && data_value<=1000.0) cont_dig = 4;
		
	a = (unsigned int)(data_value * 10.0 + 0.5);
		
	for(i =0; i<cont_dig; i++) {
		dig[i] = (unsigned char)(a % 10);
		a /= 10;
	}
		
	if(cont_dig == 3) {
		if(!dig[2]) lcdPrintChar(' ');
		else		lcdPrintChar(dig[2] + '0');
			
		lcdPrintChar(dig[1] + '0');
		lcdPrintChar('.');
		lcdPrintChar(dig[0] + '0');		
	}
	if(cont_dig == 4) {
		if(!dig[3]) lcdPrintChar(' ');
		else		lcdPrintChar(dig[3] + '0');
			
		lcdPrintChar(dig[2] + '0');
		lcdPrintChar(dig[1] + '0');
		lcdPrintChar('.');
		lcdPrintChar(dig[0] + '0');
	}
}