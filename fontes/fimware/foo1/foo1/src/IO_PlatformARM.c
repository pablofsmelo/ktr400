/*
 * IO_PlatformARM.c
 *
 * Created: 25/09/2017 17:44:05
 *  Author: Pablo Melo
 */ 

#include <asf.h>
#include "IO_PlatformARM.h"
#include "Defines_PlatformARM.h"

bool readButton(char number_button) {

	char current_state_button[8];
	bool last_state_button;
	
	switch (number_button) {
		
		case BT1:
		{
			last_state_button = current_state_button[0] = ioport_get_pin_level(BUTTON_1);
			break;
		}
		
		case BT2:
		{
			last_state_button = current_state_button[1] = ioport_get_pin_level(BUTTON_2);
			break;
		}
		
		case BT3:
		{
			last_state_button = current_state_button[2] = ioport_get_pin_level(BUTTON_3);
			break;
		}
		
		case BT4:
		{
			last_state_button = current_state_button[3] = ioport_get_pin_level(BUTTON_4);
			break;
		}
		
		case BT5:
		{
			last_state_button = current_state_button[4] = ioport_get_pin_level(BUTTON_5);
			break;
		}
		
		case BT6:
		{
			last_state_button = current_state_button[5] = ioport_get_pin_level(BUTTON_6);
			break;
		}
		
		case BT7:
		{
			last_state_button = current_state_button[6] = ioport_get_pin_level(BUTTON_7);
			break;
		}
		
		case BT8:
		{
			last_state_button = current_state_button[7] = ioport_get_pin_level(BUTTON_8);
			break;
		}
		
		default:
			last_state_button = - 1;
			break;
	}
	return last_state_button;
}

void writeLed(char led_name, bool set_state) {
	
	switch (led_name) {
		
		case D2:
		{
			//ioport_set_pin_level(LED_D3, set_state);	
			break;
		}
		case D3:
		{
			ioport_set_pin_level(LED_D4, set_state);
			break;
		}
		case D4:
		{
			ioport_set_pin_level(LED_D5, set_state);
			break;
		}
		case D5:
		{	ioport_set_pin_level(LED_D6, set_state);
			break;
		}
		case D6:
		{	ioport_set_pin_level(LED_D7, set_state);
			break;
		}
		
		case D7:
		{	ioport_set_pin_level(LED_D8, set_state);
			break;
		}
		
		case D8:
		{	ioport_set_pin_level(LED_D9, set_state);
			break;
		}
		
		case D9:
		{	ioport_set_pin_level(LED_D10, set_state);
			break;
		}
	}
}

