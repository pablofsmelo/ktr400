/*
 */ 


#ifndef APPLICATIONDEF_H_
#define APPLICATIONDEF_H_

/*  Bibliotecas
* Adicionar todos #includes aqui
*
***************************************************/
#include <asf.h>
#include <stdint.h>

/*  Defini��es
* Adicionar todos #defines aqui
*
***************************************************/

/*Habilitar DEBUG_CONSOLE somente para exibir informa��es junto do nome do arquivo e n�mero da linha*/
/*Compilar vers�o final sem essa op��o*/
#define DEBUG_CONSOLE 1

/*definir somente se for utilizar o printf da toolchain*/
//#include USE_PRINTF 1


#ifndef NULL
	#define NULL (void *)0
#endif

/*Fun��es INLINE de acordo com o compilador utilizado*/
#if defined(__CC_ARM)
	#define INLINE   __forceinline
#elif (defined __GNUC__)
	#define INLINE   inline __attribute__((__always_inline__))
#elif (defined __ICCARM__)
	#define INLINE   _Pragma("inline=forced")
#endif

/*Estruturas alinhadas byte a byte*/
#define STRUCT_PACKED __attribute__ ((__packed__))


#define false 0
#define true 1


bool nearlyEqual(float a, float b, float epsilon);



#endif /* APPLICATIONDEF_H_ */