/*
 * ConfigHW_PlatformARM.h
 *
 * Created: 26/09/2017 10:49:39
 *  Author: User
 */ 

#include <asf.h>
#include "Defines_PlatformARM.h"

#ifndef CONFIGHW_PLATFORMARM_H_
#define CONFIGHW_PLATFORMARM_H_

/* Functions to ADC converter */ 
void	 initADCUser	  (void);
void	 selectChannelADC (uint8_t);
void	 initConvADC	  (void);
bool	 waitConvADC	  (void);
uint16_t readConvADCUser  (void); 
uint16_t readADC		  (void);		
void	 periodSample	  (uint16_t tmp);
void	 enableADC		  (void);
void	 disableADC		  (void);

/* Functions to USB */
void    USB_clock_enable	  (void);
void	USB_start			  (void);
void	USB_stop			  (void);
void	USB_write_char		  (char data);
char	USB_read_char		  (void);
void    USB_write_buf         (const void* buf, uint32_t buf_size);
void	USB_read_buf		  (void* buf, uint8_t buf_size);
uint8_t	USB_get_number_bytes  (void);
bool	USB_check_is_connected(void);


#endif /* CONFIGHW_PLATFORMARM_H_ */

