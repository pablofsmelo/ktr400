/*
 * IO_PlatformARM.h
 *
 * Created: 25/09/2017 17:43:27
 *  Author: Pablo Melo
 */ 


#ifndef IO_PLATFORMARM_H_
#define IO_PLATFORMARM_H_

/* Input: number button of user */
/* Application: This function return the state of select button */
/* Return: state of button		*/
bool readButton (char button_name);

/* Input: led_name = led platform ARM KNNT (D2 to D9), set_state = 0(LOW) or 1(HIGH)*/
/* Application: this function connects the pin according to the user's desire  */
/* Return: */
void writeLed	(char led_name, bool set_state);


#endif /* IO_PLATFORMARM_H_ */