/*
 * DisplayLCD_PlatformARM.h
 *
 * Created: 03/10/2017 09:00:00
 * Author: Pablo Melo
 * Company: Kenntech - Tecnologia Eletrônica ltda.
 */ 

#ifndef DISPLAYLCD_PLATFORMARM_H_
#define DISPLAYLCD_PLATFORMARM_H_

/*****************************************************
 *			Function Name: lcdSendNibble			 *
 * Input: Nibble that will be sent to the display	 *
 * Output: Nothing									 *
 * Description: This function check the bits of		 *
 *				interest for port,generates an enable*
 *				pulse and to change Enable state.	 *
 ****************************************************/
void lcdSendNibble(char data_bit);


/*****************************************************
 *			Function Name: lcdSendCmd				 *
 * Input: Command to initialize display				 *
 * Output: Nothing									 *
 * Description: This function sends commands to the  *
 *				port for startup. The list of		 *
 *				commands can be checked in the file	 *
 *				"Define_PlatformARM.h"				 *
 ****************************************************/
void lcdSendCmd(char command);


/*****************************************************
 *			Function Name: lcdStart					 *
 * Input: Nothing									 *
 * Output: Nothing									 *
 * Description: Display initialization function that *
 *			    configures it to work with 2 nibbles *
 *				and to receive commands or settings. *
 ****************************************************/
void lcdStart(void); 


//void lcdSetCursor(char posAddr);


/*****************************************************
 *			Function Name: lcdPrintChar				 *
 * Input: data that will write in display			 *
 * Output: Nothing									 *
 * Description:	 This function sends char to the	 *
 *				 port. 					
 ****************************************************/
void lcdPrintChar(char data);


/*****************************************************
 *			Function Name: lcdPrintString			 *
 * Input: char *a									 *
 * Output:											 *
 * Description: This function sends String to the	 *
 *				 port.								 *
 ****************************************************/
void lcdPrintString (char *a);


/*****************************************************
 *			Function Name: lcdSetCursor				 *
 * Input: numbers row and column					 *
 * Output: posAddr (line 1: 00|01|02...0F)			 *
 *				   (line 2: 40|41|42...4F)			 *
 * Description: Set the cursor position of the lcd	 *
 ****************************************************/
void lcdSetCursor(char row, char column);


/*****************************************************
 *			Function Name: lcdWriteFloat			 *
 * Input:  data float to print in display			 *
 * Output: posAddr (line 1: 00|01|02...0F)			 *
 *				   (line 2: 40|41|42...4F)			 *
 * Description: Set the cursor position of the lcd	 *
 ****************************************************/
void lcdWriteFloat (float data_value);



void testeNumber (float number);


#endif /* DISPLAYLCD_PLATFORMARM_H_ */