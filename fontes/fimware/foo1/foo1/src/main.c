/*********************************************************/
/*		Project: Application M200 - RS485 test.
/*
/* Company: Kenntech - Tecnologia Eletrônica
/* Author : Pablo Melo - Embedded Systems Developer
/* Data: 29/09/2017
/* Notes:
/*
/* Project of development department
/********************************************************/

/* List of includes for application */
#include <asf.h>
#include <stdio.h>
#include "ConfigHW_PlatformARM.h"
#include "Defines_PlatformARM.h"
#include "Display7seg_PlatformARM.h"
#include "DisplayLCD_PlatformARM.h"
#include "IO_PlatformARM.h"
#include "M200_Protocol.h"
#include "RS485_PlatformARM.h"
#include "RtcDS1307_PlatformARM.h"
#include "SensorLM35_PlatformARM.h"
#include "SPI_PlatformARM.h"

#define READ_ACCEL		1
#define READ_TEMPE		2
#define CHECK_ERROR		3

uint8_t buffer_serial[10],buffer_send[30],buffer_485[10],buffer_485_send[21];
uint8_t cont_buffer_serial=0,cont_buffer_485=0;

bool flag_serial=false;
bool copying_buffer=false;
static bool my_flag_autorize_cdc_transfert = 0;
unsigned char str_valores[5];
char tasks = 1;
bool status_x, status_y, status_z;
bool status_s1, status_s2, status_s3;
bool status_sensor_temp;
char cont_erro = 0;

bool sensor_is_connected = false;
bool flag_bt1 = false;
bool flag_bt2 = false;
bool flag_bt3 = false;

char MSBR, LSBR;
uint16_t concatena_dados_accel;
float vibracao_x, vibracao_y, vibracao_z;

uint16_t temperatura_1, temperatura_2, temperatura_3;

uint16_t cont_tempo_acel_desconectado = 0;
bool flag_aciona_led = false;

bool flag_timer_error_accel= false;
bool accel_is_disconnnected = false;

uint16_t cont_timer_test = 0;
bool flag_led_on = 0;

void adc_read_conv_result(void);
uint8_t checksum(uint8_t*,uint8_t);
uint8_t checksum_485(uint8_t*,uint8_t);
void comando_vel_eixos(void);
void testaRS458();


void USART0_Handler()
{
	uint32_t dw_status = usart_get_status(USART_485);
	if (dw_status & US_CSR_RXRDY) /* verifica se interrupção foi disparada */
	{
		usart_read(USART_485, &buffer_485[cont_buffer_485]); /*armazena dados seriais no buffer */
		cont_buffer_485++;
		if(cont_buffer_485>9)cont_buffer_485=0;
	}
}


/* Main Routine */

int main (void)
{
	irq_initialize_vectors();
	Enable_global_interrupt();
	
	sysclk_init();
	
	sysclk_enable_peripheral_clock(DACC_BASE);
	enable_clock_485();
	
	board_init();
	
	init_485();
	init_interrupt_485();
	delay_init(sysclk_get_cpu_hz());
	
	irq_register_handler(TC10_IRQn,0);
	delay_ms(3000);
	while (1) {
		//$PSRF103,00,6,00,0*23
		
		buffer_485_send[0] = "$";
		buffer_485_send[1] = "P";
		buffer_485_send[2] = "S";
		buffer_485_send[3] = "R";
		buffer_485_send[4] = "F";
		buffer_485_send[5] = "1";
		buffer_485_send[6] = "0";
		buffer_485_send[7] = "3";
		buffer_485_send[8] = ",";
		buffer_485_send[9] = "0";
		buffer_485_send[10] = "0";
		buffer_485_send[11] = ",";
		buffer_485_send[12] = "6";
		buffer_485_send[13] = ",";
		buffer_485_send[14] = "0";
		buffer_485_send[15] = "0";
		buffer_485_send[16] = ",";
		buffer_485_send[17] = "0";
		buffer_485_send[18] = "*";
		buffer_485_send[19] = "2";
		buffer_485_send[20] = "3";
		
		//buffer_485_send[5] = checksum_485(buffer_485_send, 6);
		send_buffer_485(buffer_485_send, 21);
	
		delay_ms(200);
		
		//writeLed(D9, HIGH);
		//delay_ms(100);
		//writeLed(D9,LOW);
	}
}

uint8_t checksum_485(uint8_t *bf,uint8_t n_b)
{
	uint16_t val_check=0;
	for(uint8_t inc=0;inc<n_b-1;inc++)
	{
		val_check+=bf[inc]; //Soma os bytes
	}
	return (val_check&0x00ff);//Realiza a logica and e retorna o valor
}



	