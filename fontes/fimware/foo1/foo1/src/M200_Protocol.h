/*
 * M200_Protocol.h
 *
 * Created: 09/10/2017 11:23:19
 *  Author: User
 */ 


#ifndef M200_PROTOCOL_H_
#define M200_PROTOCOL_H_

#define START_GIGA_T_MASTER_CMD	   100
#define START_ACCEL_SLAVE_CMD		10
#define AXIS_X_CMD					 1
#define AXIS_Y_CMD					 2
#define AXIS_Z_CMD					 3

#define START_TEMP_SLAVE_CMD		20
#define SENSOR1_CMD					 4
#define SENSOR2_CMD					 5
#define SENSOR3_CMD					 6

bool MasterRequestStatus (char device, char command);

float	MasterRequestAxisX(void);
float	MasterRequestAxisY(void);
float	MasterRequestAxisZ(void);

float	MasterRequestSensor1(void);
float	MasterRequestSensor2(void);
float	MasterRequestSensor3(void);


#endif /* M200_PROTOCOL_H_ */