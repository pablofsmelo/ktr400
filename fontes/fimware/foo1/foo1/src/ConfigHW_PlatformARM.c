/*
 *	ConfigHW_PlatformARM.C
 *
 *	Created: 26/09/2017 10:50:06
 *  Author: Pablo Melo
 */ 

#include <asf.h>
#include "Defines_PlatformARM.h"
#include "ConfigHW_PlatformARM.h"

extern void adc_read_conv_result(void);

struct adc_config adc_cfg = {
	
	
	.prescal = ADC_PRESCAL_DIV32,	   /* System clock division factor is 32 */
	.clksel = ADC_CLKSEL_APBCLK,	  /* The APB clock is used */
	.speed = ADC_SPEED_225K,		 /* Max speed is 225K */
	.refsel = ADC_REFSEL_2,			/* ADC Reference voltage is external ref 2 */
	.start_up = CONFIG_ADC_STARTUP /* Enables the Startup time*/
};

struct adc_seq_config adc_x_cfg = {
	
	// Select Vref for shift cycle
	.zoomrange = ADC_ZOOMRANGE_0, 
	// Pad Ground
	.muxneg = ADC_MUXNEG_1,
	// ADC0
	.muxpos = ADC_MUXPOS_3,
	// Enables the internal voltage sources
	.internal = ADC_INTERNAL_2,
	// Disables the ADC gain error reduction
	.gcomp = ADC_GCOMP_DIS,
	// Disables the HWLA mode
	.hwla = ADC_HWLA_DIS,
	//Trigger por software
	//.trgsel = ADC_TRIG_SW,
	.trgsel = ADC_TRIG_INTL_TIMER,
	// 12-bits resolution
	.res = ADC_RES_12_BIT,
	// Enables the single-ended mode
	.bipolar = ADC_BIPOLAR_SINGLEENDED
};

struct adc_ch_config adc_x_ch_cfg={
	.seq_cfg = &adc_x_cfg,
	//.internal_timer_max_count=1499,
};

struct adc_dev_inst g_adc_inst;

void initADCUser(void) {
	adc_init(&g_adc_inst, ADC_USER, &adc_cfg);
	adc_enable(&g_adc_inst);
	adc_set_callback(&g_adc_inst, ADC_SEQ_SEOC, adc_read_conv_result, ADCIFE_IRQn, 0);
}

void initConvADC(void) {
	adc_start_itimer(&g_adc_inst); /* Start the ADC internal timer */
}

bool waitConvADC(void) {
	if ((adc_get_status(&g_adc_inst) & ADCIFE_SR_SEOC) != ADCIFE_SR_SEOC) return true;
	else																  return false;
}

uint16_t readConvADCUser(void) {
	
	uint16_t val_conv = 0;
	
	val_conv =  adc_get_last_conv_value(&g_adc_inst);
	adc_clear_status(&g_adc_inst, ADCIFE_SCR_SEOC);
	
	return val_conv;
}

void selectChannelADC(uint8_t channel) {
	if(channel == LM35_CHANNEL) adc_ch_set_config(&g_adc_inst, &adc_x_ch_cfg);
}

uint16_t readADC(void) {
	//while(waitConvADC()){};
	return readConvADCUser();
}

void periodSample (uint16_t tmp) {
	adc_configure_itimer_period(&g_adc_inst,tmp);
}

void enableADC(void) {
	adc_enable(&g_adc_inst);
}

void disableADC(void) {
	adc_disable(&g_adc_inst);
}


/* ------------------ USB functions configuration ------------------ */
void USB_clock_enable(void) {
	sysclk_enable_peripheral_clock(USBC);
}

void USB_start(void) {
	udc_start();
}

void USB_stop(void) {
	udc_stop();
}

void USB_write_char(char data) {
	udi_cdc_putc(data);
}

char USB_read_char(void) {
	char data_aux = udi_cdc_getc();
}

void USB_write_buf(const void* buf, uint32_t buf_size) {
	udi_cdc_write_buf(buf, buf_size);
}

void USB_read_buf(void* buf, uint8_t buf_size) {
	udi_cdc_read_buf(buf, buf_size);
}

uint8_t USB_get_number_bytes(void) {
	return udi_cdc_get_nb_received_data();
}

bool USB_check_is_connected(void) {
	bool state_usb;
	
	if (!ioport_get_pin_level(USB_SENSE)) state_usb = true;
	else                                  state_usb = false;
	
	return state_usb;
}
