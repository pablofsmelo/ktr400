/**
 * \file
 *
 * \brief Board configuration.
 *
 * Copyright (c) 2012-2014 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
 /**
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef CONF_BOARD_H_INCLUDED
#define CONF_BOARD_H_INCLUDED

// External oscillator frequency
#define BOARD_XOSC_HZ				12000000		/* externo */
#define CONFIG_PLL0_SOURCE			PLL_SRC_OSC0

// External oscillator type.
//!< External clock signal
//#define BOARD_XOSC_TYPE        XOSC_TYPE_EXTERNAL
//!< 32.768 kHz resonator on TOSC
//#define BOARD_XOSC_TYPE        XOSC_TYPE_32KHZ
//!< 0.4 to 16 MHz resonator on XTALS
#define BOARD_XOSC_TYPE				XOSC_TYPE_XTAL

// External oscillator startup time
#define BOARD_XOSC_STARTUP_US		50000
#define BOARD_OSC0_HZ				BOARD_XOSC_HZ
#define BOARD_OSC0_STARTUP_US		BOARD_XOSC_STARTUP_US
#define BOARD_FREQ_SLCK_BYPASS		0
#define BOARD_FREQ_MAINCK_BYPASS	0
#define XOSC_TYPE_XTAL				SYSCLK_SRC_OSC0
#define BOARD_OSC0_IS_XTAL			true


//#define LED_D3						PIN_PA04
#define LED_D4						PIN_PA05
#define LED_D5						PIN_PA06
#define LED_D6						PIN_PA07
#define LED_D7						PIN_PA08
#define LED_D8						PIN_PA09
#define LED_D9						PIN_PA10
#define LED_D10						PIN_PA11

#define BUTTON_1					PIN_PA15
#define BUTTON_2					PIN_PA16
#define BUTTON_3					PIN_PA17
#define BUTTON_4					PIN_PA18
#define BUTTON_5					PIN_PA19
#define BUTTON_6					PIN_PA20
#define BUTTON_7					PIN_PA13
#define BUTTON_8					PIN_PA14

#define ADC_USER					ADCIFE
//#define LM35_PIN					PIN_PB02

#define LM35_PIN					PIN_PA04
#define LM35_ACC_MUX				PIN_PA04A_ADCIFE_AD0

#define DACC_BASE					DACC

#define RD_485						PIN_PA02
#define TX_485						PIN_PB15A_USART0_TXD
#define RX_485						PIN_PB14A_USART0_RXD
#define RTS_485						PIN_PA02
#define MUX_TX_485					MUX_PB15A_USART0_TXD
#define MUX_RX_485					MUX_PB14A_USART0_RXD
#define MUX_RTS_485					MUX_PA06B_USART0_RTS 

#define RS_LCD						PIN_PB12
#define EN_LCD						PIN_PB13
#define D0_LCD						PIN_PB04
#define D1_LCD						PIN_PB05
#define D2_LCD						PIN_PB06
#define D3_LCD						PIN_PB07
#define D4_LCD						PIN_PB08
#define D5_LCD						PIN_PB09
#define D6_LCD						PIN_PB10
#define D7_LCD						PIN_PB11

#define TC_TIMER					TC1
#define TC_TIMER_Handler			TC10_Handler
#define TC_TIMER_IRQn				TC10_IRQn
#define TC_CHANNEL_TIMER			0


/** Enable Com Port. */
#define CONF_BOARD_UART_CONSOLE

//! [tc_define_peripheral]
/* Use TC Peripheral 0. */
#define TC							TC0
#define TC_PERIPHERAL				0
//! [tc_define_peripheral]

//! [tc_define_ch1]
/* Configure TC0 channel 1 as waveform output. */
#define TC_CHANNEL_WAVEFORM			1
#define ID_TC_WAVEFORM				ID_TC1
#define PIN_TC_WAVEFORM				PIN_TC0_TIOA1
#define PIN_TC_WAVEFORM_MUX			PIN_TC0_TIOA1_MUX
//! [tc_define_ch1]

//! [tc_define_ch2]
/* Configure TC0 channel 2 as capture input. */
#define TC_CHANNEL_CAPTURE			2
#define ID_TC_CAPTURE				ID_TC2
#define PIN_TC_CAPTURE				PIN_TC0_TIOA2
#define PIN_TC_CAPTURE_MUX			PIN_TC0_TIOA2_MUX
//! [tc_define_ch2]

//! [tc_define_irq_handler]
/* Use TC2_Handler for TC capture interrupt. */
#define TC_Handler					TC2_Handler
#define TC_IRQn						TC2_IRQn
//! [tc_define_irq_handler]

#endif /* CONF_BOARD_H_INCLUDED */