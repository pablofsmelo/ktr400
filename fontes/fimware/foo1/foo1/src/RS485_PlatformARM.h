/*
 * RS485_PlatformARM.h
 *
 * Created: 26/09/2017 09:59:31
 *  Author: User
 */ 


#ifndef RS485_PLATFORMARM_H_
#define RS485_PLATFORMARM_H_

#define USART_485                   USART0
#define USART_485_ID				ID_USART0
#define USART_SERIAL_ISR_HANDLER    USART0_Handler
#define USART_485_BAUDRATE			38400//24000//9600
#define USART_485_CHAR_LENGTH		US_MR_CHRL_8_BIT
#define USART_485_PARITY			US_MR_PAR_NO
#define USART_485_STOP_BIT			US_MR_NBSTOP_1_BIT

void init_485(void);
void enable_clock_485(void);
void single_send_485(uint8_t);
void send_buffer_485(uint8_t*,uint8_t);
void init_interrupt_485(void);

#endif /* RS485_PLATFORMARM_H_ */