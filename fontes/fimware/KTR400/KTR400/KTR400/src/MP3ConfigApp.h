/*********************************************************************
 *					File Name: MP3ConfigApp.h						 *
 * Project Name: KTR400												 *
 * Created: 10/04/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: This module deals with the DFPLAYER MINI 					 *
 ********************************************************************/
#ifndef MP3CONFIG_H_
#define MP3CONFIG_H_

/*===================== Defines USART for MP3 ======================*/
#define USART_MP3               USART1
#define USART_MP3_ID		    ID_USART1
#define USART_MP3_BAUDRATE	    9600
#define USART_MP3_CHAR_LENGTH   US_MR_CHRL_8_BIT
#define USART_MP3_PARITY	    US_MR_PAR_NO
#define USART_MP3_STOP_BIT	    US_MR_NBSTOP_1_BIT

/*===================== Defines Comands for MP3=====================*/
#define MAX_LENGHT_BUFFER_MP3	 10
#define START_BYTE_CMD			 0x7E
#define END_BYTE_CMD			 0xEF
#define VERSION_BYTE_CMD         0xFF
#define NUMBER_OF_BYTES_CMD		 0x06
#define SPECIFY_SONG_CMD		 0x03
#define SPECIFY_VOLUME_CMD		 0x06
#define SPECIFY_RESET_CMD		 0x0C
#define SPECIFY_STOP_CMD		 0x16
#define VALUE_VOLUME_MAX		 30
#define VALUE_VOLUME_MIN		 0
#define TIME_TO_WRITE_CMD		 200
#define VOLUME_DEFAULT			 0x0F
#define SONG_DEFAULT			 0x05
#define TIME_TO_WRITE_LCD        200
#define TIME_TO_WRITE_SUCCESS	 2000
#define TIME_TO_WRITE_LAST_VOL	 2000
#define TIME_TO_WRITE_NEW_VOL	 500

#define SPECIAL_CHARACTER_VOLUME 0b11111111

/*=================== Functions Control for MP3 ===================*/
void MP3UartInit		 (void);
void MP3SendUART		 (uint8_t len);
void MP3Checksum		 (uint8_t *str, uint8_t len);
void MP3PlayMusic	     (uint8_t val_music);
void MP3SendCmd		     (uint8_t cmd, uint16_t feedback, uint16_t dat);  /*This function send command for DFPLAYER MINI */
void MP3ResetModule		 (void);
void MP3StopModule		 (void);
void MP3NewVolume		 (void); 
void MP3AjustVolume		 (uint8_t val_volume);

/*=============== Functions Applicaiton using MP3 ================*/
void MP3VolumeDefaultMem (void);

#endif /* MP3CONFIG_H_ */