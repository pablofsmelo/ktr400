/*********************************************************************
 *				  File Name: FlashHardware.h					     *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#ifndef FLASHHARDWARE_H_
#define FLASHHARDWARE_H_

/*================ Defines for Flash Intern ATSAM4LS ===============*/
#define BASE_ADDRESS		FLASH_USER_PAGE_ADDR + 8
#define USER_PAGE_ADDRESS   (BASE_ADDRESS)
#define USER_VAR_IS_INIT	(USER_PAGE_ADDRESS + 0)
#define USER_PASSWORD_MEM	(USER_VAR_IS_INIT  + 3)

/*============== Functions for Flash Intern ATSAM4LS ==============*/
void StoreDataOnUserPage(void const * const user_page_address, void const * const source, uint16_t length);
void GetDataFromUserPage(void const * const user_page_address, void const * const destination, uint16_t length);

#endif /* FLASHHARDWARE_H_ */