/*********************************************************************
 *				  File Name: LCDConfigApp.h						     *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#ifndef LCDCONFIG_H_
#define LCDCONFIG_H_

/*===================== Defines for LCD Settings ====================*/
#define START_LCD_CMD		0x03
#define RETURN_TO_HOME_CMD	0x02
#define LCD_4_BITS_CMD		0x28
#define LCD_ON_CMD			0x0C
#define CLEAR_LCD_CMD		0x01
#define FORCE_CURSOR_1_LINE 0x80

/*==================== Functions for LCD Settings ==================*/
void LCDInit		(void);
void LCDSendNibble	(char data_bit);
void LCDSendCmd		(char command);
void LCDStart		(void);
void LCDPrintChar	(char data);
void LCDSetCursor	(char row, char column);
void LCDPrintString	(char row, char column, char *str);
void LCDWriteFloat	(float data_value);
void LCDWriteInt	(int data_value);
void LCDClear		(void);

#endif /* LCDCONFIG_H_ */