/*******************************************************************
 File: Config Clock System
 Author: Pablo Melo
 Data: 19/02/2018
 Project: KTR400
 Notes: The clock is configured based on an external 12MHz crystal
*******************************************************************/

#ifndef CONF_CLOCK_H_INCLUDED
#define CONF_CLOCK_H_INCLUDED

/* External oscillator startup time */
#define BOARD_XOSC_STARTUP_US		50000
#define BOARD_OSC0_HZ				12000000
#define BOARD_OSC0_STARTUP_US		BOARD_XOSC_STARTUP_US
#define BOARD_FREQ_SLCK_BYPASS		0
#define BOARD_FREQ_MAINCK_BYPASS	0
#define XOSC_TYPE_XTAL				SYSCLK_SRC_OSC0
#define BOARD_OSC0_IS_XTAL			true

/* Setting the system clock thought of PLL0 module */
#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_PLL0 

/* 0: disable PicoCache, 1: enable PicoCache  */
#define CONFIG_HCACHE_ENABLE        1

/* Keeping peripherals bus ASF default */ 
#define CONFIG_SYSCLK_CPU_DIV       0
#define CONFIG_SYSCLK_PBA_DIV       0
#define CONFIG_SYSCLK_PBB_DIV       0
#define CONFIG_SYSCLK_PBC_DIV       0
#define CONFIG_SYSCLK_PBD_DIV       0

#define CONFIG_USBCLK_SOURCE        USBCLK_SRC_PLL0

/* Fusb = Fsys / USB_div */
#define CONFIG_USBCLK_DIV			1

/* Configured the PLL0 for to use extern oscilator */
#define CONFIG_PLL0_SOURCE          PLL_SRC_OSC0 

/* Fpll0 = (Fclk * PLL_mul) / PLL_div */
//#define CONFIG_PLL0_MUL				(48000000UL / BOARD_OSC0_HZ)
#define CONFIG_PLL0_MUL				4
#define CONFIG_PLL0_DIV             1

#endif /* CONF_CLOCK_H_INCLUDED */
