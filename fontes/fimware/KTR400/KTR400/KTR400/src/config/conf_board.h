/**********************************
 * File Name: conf_board.h
 * Project Name: KTR400	
 * Created: 19/02/2018 14:29:59
 * Author: Pablo Melo
 * Notes:
**********************************/ 
#ifndef CONF_BOARD_H
#define CONF_BOARD_H

#define ON_OFF_GPS_PIN		PIN_PA02
#define ENTER_BT_PIN		PIN_PA04
#define UP_BT_PIN			PIN_PA05
#define	DOWN_BT_PIN			PIN_PA06
#define CANCEL_BT_PIN		PIN_PA07
#define PLAY_BT_PIN			PIN_PA08
#define EN_LCD_PIN			PIN_PA09
#define RS_LCD_PIN			PIN_PA10
#define D4_LCD_PIN			PIN_PA13
#define D5_LCD_PIN			PIN_PA14
#define D6_LCD_PIN			PIN_PA17
#define D7_LCD_PIN			PIN_PA18
#define IRQ_BQ_RTC_PIN		PIN_PA31
#define PPS_SIGNAL_PIN		PIN_PB14
#define WP_MEM_PIN			PIN_PB13
#define RST_MEM_PIN			PIN_PB12
#define LED_PPS_PIN			PIN_PB11
#define LED_OP_PIN			PIN_PB10
#define LED_ALARM_PIN		PIN_PB09
#define RD_RS485_PIN		PIN_PB08

/* Defined pins for USART0 */
#define RX0_USART0_PIN		PIN_PA11A_USART0_RXD
#define TX0_USART0_PIN		PIN_PA12A_USART0_TXD
//#define RTS Is not defined in Schematics
#define RX0_USART0_MUX		MUX_PA11A_USART0_RXD
#define TX0_USART0_MUX		MUX_PA12A_USART0_TXD
//Maybe to define RTS MUX

/* Defined pins for USART1 */
#define RX1_USART1_PIN		PIN_PA15A_USART1_RXD
#define TX1_USART1_PIN		PIN_PA16A_USART1_TXD
#define RX1_USART1_MUX		MUX_PA15A_USART1_RXD
#define TX1_USART1_MUX		MUX_PA16A_USART1_TXD

/* Defined pins for USART2 */
#define RX2_USART2_PIN		PIN_PA19A_USART2_RXD
#define TX2_USART2_PIN		PIN_PA20A_USART2_TXD
#define RX2_USART2_MUX		MUX_PA19A_USART2_RXD
#define TX2_USART2_MUX		MUX_PA20A_USART2_TXD

/* Defined pins for I2C (TWI) */
#define SDA_TWD_PIN			PIN_PA21E_TWIMS2_TWD
#define SCL_TWCK_PIN		PIN_PA22E_TWIMS2_TWCK
#define SDA_TWD_MUX			MUX_PA21E_TWIMS2_TWD
#define SCL_TWD_MUX			MUX_PA22E_TWIMS2_TWCK

/* Defined pins for USB */
#define SENSE_USB_PIN		PIN_PB15
#define DM_USB_PIN			PIN_PA25A_USBC_DM
#define DP_USB_PIN			PIN_PA26A_USBC_DP
#define DM_USB_MUX			MUX_PA25A_USBC_DM
#define DP_USB_MUX			MUX_PA26A_USBC_DP

/* Defined pins for SPI */
#define SPI_USER			SPI

#define MISO_SPI_PIN		PIN_PA27A_SPI_MISO
#define MOSI_SPI_PIN		PIN_PA28A_SPI_MOSI	
#define SCK_SPI_PIN			PIN_PA29A_SPI_SCK
#define CS_SPI_MEM_PIN		PIN_PA30A_SPI_NPCS0

#define MISO_SPI_MUX		MUX_PA27A_SPI_MISO
#define MOSI_SPI_MUX		MUX_PA28A_SPI_MOSI
#define SCK_SPI_MUX			MUX_PA29A_SPI_SCK
#define CS_SPI_MEM_MUX		MUX_PA30A_SPI_NPCS0

#endif // CONF_BOARD_H
