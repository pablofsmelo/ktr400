/****************************************************************
*				File Name: RS232PanelConfigApp.c				*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes:														*
****************************************************************/
#include <asf.h>
#include "RS232PanelConfigApp.h"

const sam_usart_opt_t usart_232_console_settings = {
	USART_232_PANEL_BAUDRATE,
	USART_232_PANEL_CHAR_LENGTH,
	USART_232_PANEL_PARITY,
	USART_232_PANEL_STOP_BIT,
	US_MR_CHMODE_NORMAL
};

/*=============== Functions for RS232 Protocol ================*/
void Rs232PanelInit(void) {
	usart_init_rs232(USART_232_PANEL, &usart_232_console_settings, sysclk_get_main_hz());
	usart_enable_tx(USART_232_PANEL);
	usart_enable_rx(USART_232_PANEL);
}

void Rs232PanelEnableClock(void) {
	sysclk_enable_peripheral_clock(USART_232_PANEL);
}

void Rs232PanelSendBuffer(uint8_t *bf, uint8_t leg) {
	//USARTWriteBuffer(USART_232_PANEL,bf, leg);
}

void Rs232PanelSendSingle(uint8_t snd) {
	//USARTWriteChar(USART_232_PANEL,snd);
}

void Rs232PanelInitInterrupt(void) {
	//USARTEnableInterrupt(USART_232_PANEL, 1);
}
/*================================================================*/
