/****************************************************************
*					File Name: RTCConfigApp.c					*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes: This library was created based on DS1307/DS1339		*
****************************************************************/
#include <asf.h>
#include "MyApp.h"
#include "RTCConfigApp.h"
#include "TWIHardware.h"
#include "FlashHardware.h"
#include "LCDConfigApp.h"

const uint8_t rtc_init_buffer[] = {0x00};
uint8_t insert_case_new_rtc;
uint8_t val_digit_rtc = 0;

uint16_t flag_rtc_already_initialized;
bool flag_new_rtc_is_ready = false;
bool flag_return_dig = false;

struct date_rtc date_now_aux;
struct date_rtc modify_date;
struct init_mem PARAMETERS_INIT;


/*================ Functions for RTC Settings ==================*/
void RTCSytemInit(void) {
	TWIClockEnable();
	TWIInit();
	RTCDS1307Init();
	
	PARAMETERS_INIT.rtc_is_initialized = false;
	GetDataFromUserPage(USER_VAR_IS_INIT, (uint8_t*)&PARAMETERS_INIT, sizeof(PARAMETERS_INIT));
	
	if(PARAMETERS_INIT.rtc_is_initialized != 1) {
		RTCDS1307StartDefault();
		flag_rtc_already_initialized = 1;
			
		PARAMETERS_INIT.rtc_is_initialized = 1;
		StoreDataOnUserPage(USER_VAR_IS_INIT,&PARAMETERS_INIT,sizeof(PARAMETERS_INIT));
	}
}

void RTCDS1307Init(void) {
	TWIBufferSend(0x07,1,ADDR_WRITE_MODE>>1,(void *)rtc_init_buffer, sizeof(rtc_init_buffer)); 
}

void RTCDS1307SetDateTime(uint32_t address_command, uint8_t data) {
	uint8_t rtc_send_data_to_write[] = {data};
		
	TWIBufferSend(address_command, 1, ADDR_WRITE_MODE>>1,(void *)rtc_send_data_to_write, sizeof(rtc_send_data_to_write));
}

uint16_t RTCDS1307GetDateTime(uint32_t address_command) {
	uint16_t receive_data_ds1307[1];
	uint8_t rtc_send_data_to_read[] = {};
	
	TWIBufferSend(address_command, 1, ADDR_WRITE_MODE>>1,(void *)rtc_send_data_to_read, sizeof(rtc_send_data_to_read));
	delay_us(100);
	TWIBufferReceive(0, 0, 0xD1>>1, &receive_data_ds1307[0], 2);
	
	return receive_data_ds1307[0];
}

uint8_t RTCDS1307BcdToDec(uint16_t val_bcd) {
	uint16_t decimal;
	decimal = (((val_bcd & 0xF0)>>4)*10)+(val_bcd&0x0F);
	return decimal;
}

uint16_t RTCDS1307DecToBcd(uint16_t num) {
    unsigned int ones = 0;
    unsigned int tens = 0;
    unsigned int temp = 0;

    ones = num%10; 
    temp = num/10; 
    tens = temp<<4;  
                     
    return (tens + ones);
}

/*================ Functions for RTC Application =================*/

void RTCDS1307StartDefault(void) {
	RTCDS1307SetDateTime(ADDR_SEC_RTC,   SEC_RTC_DEFAULT);
	RTCDS1307SetDateTime(ADDR_MIN_RTC,   MIN_RTC_DEFAULT);
	RTCDS1307SetDateTime(ADDR_HOUR_RTC,  HOUR_RTC_DEFAULT);
	RTCDS1307SetDateTime(ADDR_DAY_RTC,   DAY_RTC_DEFAULT);
	RTCDS1307SetDateTime(ADDR_MONTH_RTC, MONTH_RTC_DEFAULT);
	RTCDS1307SetDateTime(ADDR_YEAR_RTC,  YEAR_RTC_DEFAULT);
}


void RTCDS1307NewDateTime(void) {
	flag_new_rtc_is_ready = false;
	val_digit_rtc = 1;
	insert_case_new_rtc = SELECT_NEW_DAY_RTC;
		
	LCDClear();
	LCDSetCursor(1,0);
	LCDPrintChar(SPECIAL_CHARACTER);
	LCDPrintChar(SPECIAL_CHARACTER);
	LCDPrintString(1,2, "/00/00");
	LCDPrintString(2,0,	"00:00:00");
	delay_ms(TIME_MENSAGE_INIT_RTC);
	
	flag_en_exit_time = false;

	while (flag_new_rtc_is_ready == false) {

		switch (insert_case_new_rtc)
		{
			case SELECT_NEW_DAY_RTC:
				LCDSetCursor(1,0);
				LCDPrintChar(SPECIAL_CHARACTER);
				LCDPrintChar(SPECIAL_CHARACTER);
				delay_ms(TIME_WRITE_SPECIAL_CHAR);
			
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					LCDSetCursor(1,0);
					LCDWriteInt(val_digit_rtc);
					LCDPrintString(1,2, "/00/00");
					LCDPrintString(2,0, "00:00:00");
					
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_new_rtc_is_ready = true;
						val_digit_rtc = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc >= PAR_DAY_MAX) {
							val_digit_rtc =  1;
							flag_return_dig = true;
						}
						if(val_digit_rtc <= PAR_DAY_MAX){
							LCDSetCursor(1,0);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							
							if(flag_return_dig == false) val_digit_rtc++;
							flag_return_dig = false;
							
							if(val_digit_rtc == PAR_DAY_MAX) {
								LCDSetCursor(1,0);								
								LCDWriteInt(val_digit_rtc);
							}				
						}
					}
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc >= 1) {
							LCDSetCursor(1,0);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							val_digit_rtc--;
						}
						if(val_digit_rtc < 1) val_digit_rtc = PAR_DAY_MAX;
					}
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						flag_en_exit_time = false;
						modify_date.day = val_digit_rtc;
						val_digit_rtc = 1;
						flag_return_dig = false;
						insert_case_new_rtc = SELECT_NEW_MONTH_RTC;
						delay_ms(TIME_BUTTON_RTC);
						break;
					}
				}
			break; /* case SELECT_NEW_DAY_RTC */ 
			
			case SELECT_NEW_MONTH_RTC:
				LCDSetCursor(1,3);
				LCDPrintChar(SPECIAL_CHARACTER);
				LCDPrintChar(SPECIAL_CHARACTER);
				delay_ms(TIME_WRITE_SPECIAL_CHAR);
								
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					LCDSetCursor(1,0);
					LCDWriteInt(modify_date.day);
					LCDSetCursor(1,3);
					LCDWriteInt(val_digit_rtc);
					LCDPrintString(1,5, "/00");
					LCDPrintString(2,0, "00:00:00");
					
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_new_rtc_is_ready = true;
						val_digit_rtc = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc >= PAR_MONTH_MAX) {
							val_digit_rtc = 1;
							flag_return_dig = true;
						}
						if(val_digit_rtc <= PAR_MONTH_MAX){
							LCDSetCursor(1,3);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							
							if(flag_return_dig == false) val_digit_rtc++;
							flag_return_dig = false;
							
							if(val_digit_rtc == PAR_MONTH_MAX) {
								LCDSetCursor(1,3);
								LCDWriteInt(val_digit_rtc);
							}
						}
					}
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc >= 1) {
							LCDSetCursor(1,3);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							val_digit_rtc--;
						}
						if(val_digit_rtc < 1) val_digit_rtc = PAR_MONTH_MAX;
					}
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						flag_en_exit_time = false;
						modify_date.month = val_digit_rtc;
						val_digit_rtc = 1;
						flag_return_dig = false;
						insert_case_new_rtc = SELECT_NEW_YEAR_RTC;
						delay_ms(TIME_BUTTON_RTC);
						break;
					}
				}
			break; /* case SELECT_NEW_MONTH_RTC */
				
			case SELECT_NEW_YEAR_RTC:
				LCDSetCursor(1,6);
				LCDPrintChar(SPECIAL_CHARACTER);
				LCDPrintChar(SPECIAL_CHARACTER);
				delay_ms(TIME_WRITE_SPECIAL_CHAR);
				
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					LCDSetCursor(1,0);
					LCDWriteInt(modify_date.day);
					LCDPrintChar('/');
					LCDWriteInt(modify_date.month);
					LCDSetCursor(1,6);
					LCDWriteInt(val_digit_rtc);
					LCDPrintString(2,0, "00:00:00");
					
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_new_rtc_is_ready = true;
						val_digit_rtc = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc >= PAR_YEAR_MAX) {
							val_digit_rtc = 1;
							flag_return_dig = true;
						}
						
						if(val_digit_rtc <= PAR_YEAR_MAX){
							LCDSetCursor(1,6);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							
							if(flag_return_dig == false) val_digit_rtc++;
							flag_return_dig = false;
							
							if(val_digit_rtc == PAR_YEAR_MAX) {
								LCDSetCursor(1,6);
								LCDWriteInt(val_digit_rtc);
							}
						}
					}
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc >= 1) {
							LCDSetCursor(1,6);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							val_digit_rtc--;
						}
						if(val_digit_rtc < 1) val_digit_rtc = PAR_YEAR_MAX;
					}
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						flag_en_exit_time = false;
						modify_date.year = val_digit_rtc;
						val_digit_rtc = 0;
						flag_return_dig = false;
						insert_case_new_rtc = SELECT_NEW_HOUR_RTC;
						delay_ms(TIME_BUTTON_RTC);
						break;
					}
				}
			break; /* case SELECT_NEW_YEAR_RTC */
				
			case SELECT_NEW_HOUR_RTC:
				LCDSetCursor(2,0);
				LCDPrintChar(SPECIAL_CHARACTER);
				LCDPrintChar(SPECIAL_CHARACTER);
				delay_ms(TIME_WRITE_SPECIAL_CHAR);
				
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					LCDSetCursor(2,0);
					LCDWriteInt(val_digit_rtc);
					LCDPrintString(2,2, ":00:00");
					
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_new_rtc_is_ready = true;
						val_digit_rtc = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc>= PAR_HOUR_MAX) {
							val_digit_rtc = 0;
							flag_return_dig = true;
						}
						if(val_digit_rtc <= PAR_HOUR_MAX){
							LCDSetCursor(2,0);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							
							if(flag_return_dig == false) val_digit_rtc++;
							flag_return_dig = false;
							
							if(val_digit_rtc == PAR_HOUR_MAX) 
								LCDSetCursor(2,0);
								LCDWriteInt(val_digit_rtc);
						}		
					}
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc > 0){
							val_digit_rtc--;
							LCDSetCursor(2,0);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
						}
						else {
							val_digit_rtc = PAR_HOUR_MAX+1;
						}
					}		
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						flag_en_exit_time = false;
						modify_date.hour = val_digit_rtc;
						val_digit_rtc = 0;
						flag_return_dig = false;
						insert_case_new_rtc = SELECT_NEW_MIN_RTC;
						delay_ms(TIME_BUTTON_RTC);
						break;
					}
				}
			break; /* case SELECT_NEW_HOUR_RTC */
				
			case SELECT_NEW_MIN_RTC:
				LCDSetCursor(2,3);
				LCDPrintChar(SPECIAL_CHARACTER);
				LCDPrintChar(SPECIAL_CHARACTER);
				delay_ms(TIME_WRITE_SPECIAL_CHAR);
				
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					LCDSetCursor(2,0);
					LCDWriteInt(modify_date.hour);
					LCDSetCursor(2,3);
					LCDWriteInt(val_digit_rtc);
					LCDPrintString(2,5, ":00");
					
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_new_rtc_is_ready = true;
						val_digit_rtc = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc>= PAR_MINUTE_MAX) {
							val_digit_rtc = 0;
							flag_return_dig = true;
						}
						if(val_digit_rtc <= PAR_MINUTE_MAX) {
							LCDSetCursor(2,3);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							
							if(flag_return_dig == false) val_digit_rtc++;
							flag_return_dig = false;
							
							if(val_digit_rtc == PAR_MINUTE_MAX) {
								LCDSetCursor(2,3);
								LCDWriteInt(val_digit_rtc);
							}
						}
					}
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc > 0) {
							val_digit_rtc--;
							LCDSetCursor(2,3);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
						}
						else {
							val_digit_rtc = PAR_MINUTE_MAX+1;
						}
					}
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						flag_en_exit_time = false;
						modify_date.minute = val_digit_rtc;
						val_digit_rtc = 0;
						flag_return_dig = false;
						insert_case_new_rtc = SELECT_NEW_SEC_RTC;
						delay_ms(TIME_BUTTON_RTC);
						break;
					}
				}
			break; /* case SELECT_NEW_MIN_RTC */
				
			case SELECT_NEW_SEC_RTC:
				LCDSetCursor(2,6);
				LCDPrintChar(SPECIAL_CHARACTER);
				LCDPrintChar(SPECIAL_CHARACTER);
				delay_ms(TIME_WRITE_SPECIAL_CHAR);
				
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					LCDSetCursor(2,0);
					LCDWriteInt(modify_date.hour);
					LCDPrintChar(':');
					LCDWriteInt(modify_date.minute);
					LCDSetCursor(2,6);
					LCDWriteInt(val_digit_rtc);
					
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_new_rtc_is_ready = true;
						val_digit_rtc = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc>= PAR_SECOND_MAX) {
							val_digit_rtc = 0;
							flag_return_dig = true;
						}
						if(val_digit_rtc <= PAR_SECOND_MAX){
							LCDSetCursor(2,6);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
							
							if(flag_return_dig == false) val_digit_rtc++;
							flag_return_dig = false;
							
							if(val_digit_rtc == PAR_SECOND_MAX) {
								LCDSetCursor(2,6);
								LCDWriteInt(val_digit_rtc);
							}
						}
					}
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_rtc > 0) {
							val_digit_rtc--;
							LCDSetCursor(2,6);
							LCDWriteInt(val_digit_rtc);
							delay_ms(TIME_WRITE_CHAR);
						}
						else {
							val_digit_rtc = PAR_SECOND_MAX+1;
						}
					}
					
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						flag_en_exit_time = false;
						modify_date.second = val_digit_rtc;
						flag_new_rtc_is_ready = true;
						val_digit_rtc = 0;
						flag_return_dig = false;
						
						insert_case_new_rtc = SELECT_NEW_DAY_RTC;
						
						if(modify_date.day == PAR_DAY_MAX && modify_date.month%PAR_MONTH_IMPAR_OR_PAR == 0) {
							LCDClear();
							LCDPrintString(1,2, "Erro");
							LCDPrintString(2,0, "Tent.Nov");
							delay_ms(TIME_MENSAGE_ERR_OR_SUC);
						}
						else {
							RTCDS1307SetDateTime(ADDR_SEC_RTC,  (unsigned int)RTCDS1307DecToBcd(modify_date.second));
							RTCDS1307SetDateTime(ADDR_MIN_RTC,  (unsigned int)RTCDS1307DecToBcd(modify_date.minute));
							RTCDS1307SetDateTime(ADDR_HOUR_RTC, (unsigned int)RTCDS1307DecToBcd(modify_date.hour));
							RTCDS1307SetDateTime(ADDR_DAY_RTC,  (unsigned int)RTCDS1307DecToBcd(modify_date.day));
							RTCDS1307SetDateTime(ADDR_MONTH_RTC,(unsigned int)RTCDS1307DecToBcd(modify_date.month));
							RTCDS1307SetDateTime(ADDR_YEAR_RTC, (unsigned int)RTCDS1307DecToBcd(modify_date.year));
							
							LCDClear();
							LCDPrintString(1,0, "Nova Dat");
							LCDPrintString(2,0, "Sucesso.");
							delay_ms(TIME_MENSAGE_ERR_OR_SUC);
							LCDClear();
							
						}
						break; /* case SELECT_NEW_SEC_RTC */
					}
				}
			break; /* while (flag_new_rtc_is_ready == false) */
		}
	}
}

/*================================================================*/						