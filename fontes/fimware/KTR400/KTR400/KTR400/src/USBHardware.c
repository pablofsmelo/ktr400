/****************************************************************
*					File Name: USBHardware.c					*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes:														*
****************************************************************/
#include <asf.h>
#include "USBHardware.h"
#include "LCDConfigApp.h"

/*==== Functions for USB Hardware (based in Module UDC ASF) ===*/
void USBInit(void) {
	USBClockEnable();
	USBStart();
}

void USBClockEnable(void) {
	sysclk_enable_peripheral_clock(USBC);
}

void USBStart(void) {
	udc_start();
}

void USBStop(void) {
	udc_stop();
}

void USBWrite (unsigned char *buf, uint8_t buf_size) {
	udi_cdc_write_buf(buf, buf_size);
}

void USBRead(void* buf, uint8_t buf_size) {
	udi_cdc_read_buf(buf, buf_size);
}

uint8_t	USBGetNumberBytes(void) {
	return udi_cdc_get_nb_received_data();
}

bool USBCheckIsConnected(void) {
	bool state_usb;
	
	if (ioport_get_pin_level(SENSE_USB_PIN)) {
		LCDClear();
		LCDPrintString(1,1,"USB On");
		LCDPrintString(2,0, "Hor.Rot");
		delay_ms(2000);
	
		state_usb = true;
	}
	else {
		USBStop();
		LCDClear();
		LCDPrintString(1,1,"Erro !");
		LCDPrintString(2,0,"USB  OFF");
		delay_ms(3000);
		LCDClear();
		LCDPrintString(1,0,"Conectar");
		LCDPrintString(2,0,"Cabo USB");
		delay_ms(3000);
		LCDClear();
	
		state_usb = false;
	}
	return state_usb;
}
/*================================================================*/	




