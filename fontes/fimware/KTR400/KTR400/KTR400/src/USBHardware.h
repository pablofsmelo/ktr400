/****************************************************************
*					File Name: USBHardware.h					*
* Author: Pablo Melo;											*	
* Version FW/HW: 0.1											*
* Notes:														*
****************************************************************/
#ifndef USBCONFIG_H_
#define USBCONFIG_H_

#define MAX_LENGHT_BUFFER_USB 20

/*==== Functions for USB Hardware (based in Module UDC ASF) ===*/
void	USBInit				(void);
void    USBClockEnable		(void);
void	USBStart			(void);
void	USBStop				(void);
void    USBWrite            (unsigned char *buf, uint8_t buf_size);
void	USBRead				(void* buf, uint8_t buf_size);
uint8_t	USBGetNumberBytes   (void);
bool	USBCheckIsConnected (void);

#endif /* USBCONFIG_H_ */