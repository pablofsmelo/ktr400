/*********************************************************************
 *				  File Name: FlashHardware.c					     *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#include <asf.h>
#include "FlashHardware.h"

/*============== Functions for Flash Intern ATSAM4LS ==============*/
void StoreDataOnUserPage(void const * const user_page_address, void const * const source, uint16_t length) {
	flashcalw_memset((void * const)user_page_address, 0x0, 8, length, true);
	flashcalw_memcpy((volatile void *)user_page_address, (const void *)source, length, true);
}

void GetDataFromUserPage(void const * const user_page_address, void const * const destination, uint16_t length) {
	uint16_t index;
	uint8_t * src = (uint8_t *)user_page_address;
	uint8_t * dst = (uint8_t *)destination;
	
	for(index = 0; index < length; index++) {
		*dst++ = *src++;
	}
}
/*================================================================*/
