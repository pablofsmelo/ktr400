/**********************************
 * File Name: HardwareConfig.h
 * Project Name: KTR400	
 * Created: 19/02/2018 14:29:59
 * Author: Pablo Melo
 * Notes:
 **********************************/ 

#ifndef HARDWARECONFIG_H_
#define HARDWARECONFIG_H_


/* ------------------ Defines Hardware ------------------ */

/* Defines for TWI (I2C) */
#define TWIM_USER		 TWIM2
#define TWIM_SPEED		 100000 //100KHz

/* Defines for SPI */








/* ------------------ Functions Hardware------------------ */
/* Settings for TWI (I2C) */
void TWIClockEnable		(void);
void TWIInit			(void);
void TWIBufferSend		(uint32_t reg_addr,uint8_t reg_addr_l,uint8_t addres_chip,void *buffer,uint8_t length_buffer);
void TWIBufferReceive	(uint32_t reg_addr,uint8_t reg_addr_l,uint8_t addres_chip,void *buffer,uint8_t length_buffer);






















#endif /* HARDWARECONFIG_H_ */