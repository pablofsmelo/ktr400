/*********************************************************************
 *					File Name: MEMConfigApp.h						 *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#ifndef MEMCONFIG_H_
#define MEMCONFIG_H_

#include <asf.h>
#include "spi_master.h"


/*=============== Defines Commands for setting Memory =============*/
#define DATAFLASH_PAGES								1024
#define DATAFLASH_BUFFER_SIZE						264
#define DATAFLASH_BLOCKS							128
#define FLASH_SIZE									(uint32_t)((uint32_t)(DATAFLASH_PAGES-1)*(uint32_t)DATAFLASH_BUFFER_SIZE)		
#define BUFFER_WRITE_CMD							0x84
#define BUFFER_READ_CMD								0xD1
#define BUFFER_TO_MM_PAGE_PROG_WITH_ERASE_CMD		0x83
#define BUFFER_TO_MM_PAGE_PROG_WITHOUT_ERASE_CMD	0x88
#define MM_PAGE_PROG_THROUGH_BUFFER_CMD	            0x82
#define AUTO_PAGE_REWRITE_THROUGH_BUFFER_CMD		0x58
#define MM_PAGE_TO_BUFFER_COMPARE_CMD               0x60
#define MM_PAGE_TO_BUFFER_TRANSFER_CMD				0x53
#define STATUS_REGISTER_READ_CMD					0xD7
#define MAIN_MEMORY_PAGE_READ_CMD					0xD2
#define MAIN_MEMORY_CONTINUOUS_READ_CMD				0x03
#define PAGE_ERASE_CMD								0x81
#define BLOCK_ERASE_CMD								0x50

#define FREQUENCY_SPI_MEM_HZ					    1000000
#define BOARD_SPI_ID								0

#define ADDR_INIT_CLOCK_SCRIPT						0x02
#define ADDR_INIT_POSITION_SCRIPT				    (ADDR_INIT_CLOCK_SCRIPT+0x256) 
#define LENGHT_BUFF_MEM_CLOCK_SCRIPT				0x06
#define LENGHT_BUFF_MEM_POSITION_SCRIPT				0x0A

/*========== Structures to manipulate data stored in memory ========*/ 
struct DATA_CLOCK_MEM {
	uint8_t number_script;
	uint8_t first_digit_melody;
	uint8_t second_digit_melody;
	uint8_t third_digit_melody;
	uint8_t fourth_digit_melody;
	uint8_t hour;
	uint8_t minute;
};

struct DATA_POSITION_MEM {
	uint8_t number_script;
	uint8_t first_digit_melody;
	uint8_t second_digit_melody;
	uint8_t third_digit_melody;
	uint8_t fourth_digit_melody;
	uint8_t integer_part_lat;
	uint8_t low_dec_lat;
	uint8_t high_part_dec_lat;
	uint8_t interger_part_lon;
	uint8_t low_dec_lon;
	uint8_t high_dec_long;	
};

/*================== Functions of Memory Flash Extern ==================*/
void	MEMInit				 (void);
void	MEMErase			 (void);
uint8_t MEMCheckIsBusy		 (void);
void	MEMFlashToBuffer	 (uint16_t);
void	MEMBufferToFlash	 (uint16_t);
void	MEMRecordFlash		 (uint32_t, uint8_t *, uint8_t);
void	MEMReadFlash		 (uint32_t, uint8_t *, uint8_t);
void	MEMWriteBuffer		 (uint16_t, uint8_t *, uint8_t);
void	MEMReadID	         (void);

/*==================== Functions of Application =======================*/
void    MEMSetClockScript   (unsigned char *buff_usb, uint8_t val_script);
void    MEMSetPositionScript(unsigned char *buff_usb, uint8_t val_script);
void	MEMGetClockScript   (uint8_t val_script, struct DATA_CLOCK_MEM *datas);
void    MEMGetPositionScript(uint8_t val_script, struct DATA_POSITION_MEM*datas);


#endif /* MEMCONFIG_H_ */