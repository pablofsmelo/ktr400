#include <asf.h>
#include <board.h>
#include <conf_board.h>

#define ioport_set_pin_peripheral_mode(pino, mode) \
do {\
	ioport_set_pin_mode(pino, mode);\
	ioport_disable_pin(pino);\
} while (0)

void board_init(void)
{
	/* This function is meant to contain board-specific initialization code
	 * for, e.g., the I/O pins. The initialization can rely on application-
	 * specific board configuration, found in conf_board.h.
	 */
	ioport_set_pin_dir(RS_LCD_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(EN_LCD_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D4_LCD_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D5_LCD_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D6_LCD_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(D7_LCD_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	
	ioport_set_pin_dir(LED_PPS_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_OP_PIN, IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_dir(LED_ALARM_PIN,IOPORT_DIR_OUTPUT|IOPORT_PIN_LEVEL_LOW);
	
	ioport_set_pin_dir(ENTER_BT_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(ENTER_BT_PIN, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(UP_BT_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(UP_BT_PIN, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(DOWN_BT_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(DOWN_BT_PIN, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(CANCEL_BT_PIN,IOPORT_DIR_INPUT);
	ioport_set_pin_mode(CANCEL_BT_PIN, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(PLAY_BT_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(PLAY_BT_PIN, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_dir(SENSE_USB_PIN,IOPORT_DIR_INPUT);
	ioport_set_pin_mode(SENSE_USB_PIN, IOPORT_MODE_PULLUP);
	
	ioport_set_pin_peripheral_mode(DM_USB_PIN, DM_USB_MUX);
	ioport_set_pin_peripheral_mode(DP_USB_PIN, DP_USB_MUX);
	
	ioport_set_pin_peripheral_mode(SDA_TWD_PIN, SDA_TWD_MUX);
	ioport_set_pin_peripheral_mode(SCL_TWCK_PIN, SCL_TWD_MUX);
	
	ioport_set_pin_peripheral_mode(RX0_USART0_PIN, RX0_USART0_MUX);
	ioport_set_pin_peripheral_mode(TX0_USART0_PIN, TX0_USART0_MUX);
	
	ioport_set_pin_peripheral_mode(RX1_USART1_PIN, RX1_USART1_MUX);
	ioport_set_pin_peripheral_mode(TX1_USART1_PIN, TX1_USART1_MUX);
	
	ioport_set_pin_dir(RD_RS485_PIN,IOPORT_DIR_OUTPUT);
	ioport_set_pin_peripheral_mode(RX2_USART2_PIN, RX2_USART2_MUX);
	ioport_set_pin_peripheral_mode(TX2_USART2_PIN, TX2_USART2_MUX);
	
	ioport_set_pin_dir(WP_MEM_PIN,IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(RST_MEM_PIN,IOPORT_DIR_OUTPUT);
	
	ioport_set_pin_peripheral_mode(MOSI_SPI_PIN,MOSI_SPI_MUX);
	ioport_set_pin_peripheral_mode(MISO_SPI_PIN,MISO_SPI_MUX);
	ioport_set_pin_peripheral_mode(SCK_SPI_PIN,SCK_SPI_MUX);
	ioport_set_pin_peripheral_mode(CS_SPI_MEM_PIN,CS_SPI_MEM_MUX);
}
