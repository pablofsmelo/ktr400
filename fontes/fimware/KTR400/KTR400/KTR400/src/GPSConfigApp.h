/*********************************************************************
 *				  File Name: GPSConfigApp.h						     *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#ifndef GPSCONFIG_H_
#define GPSCONFIG_H_

#define pi 3.14159265358979323846
#define MAX_LENGHT_GPS_DATA_USART 500

/*======================= Define USART for GPS ======================*/
#define USART_GPS                   USART0
#define USART_GPS_ID				ID_USART0
#define USART_GPS_BAUDRATE			9600
#define USART_GPS_CHAR_LENGTH		US_MR_CHRL_8_BIT
#define USART_GPS_PARITY			US_MR_PAR_NO
#define USART_GPS_STOP_BIT			US_MR_NBSTOP_1_BIT

struct raw_data {	char cDataBuffer[MAX_LENGHT_GPS_DATA_USART];	int i;};extern struct raw_data raw_data_now;

extern bool flag_led_pps_on;

/*===================== Functions for GPS Hardware ==================*/
void GPSUsartInit            (void);
struct raw_data GPSGetRawData(void);
unsigned char *GPSParseNMEA	 (struct raw_data data_aux);
void GPSDataValid(void);
unsigned char *GPSRMCtoLatLon(struct raw_data data_aux);
//float stof(const char* s);
void stof(const char* s, double *lat_long_conv_f);
double convert_to_degrees(double NMEA_lat_long);

/*=================== Functions for GPS Application =================*/
double GPSDegToRad (double);
double GPSRadToDeg (double);
double GPSDistance (double lat_ref, double long_ref, double lat_now, double long_now, char unit_measure);

#endif /* GPSCONFIG_H_ */