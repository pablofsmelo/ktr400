/*********************************************************************
 *				  File Name: GPSConfigApp.h						     *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#include <asf.h>
#include "GPSConfigApp.h"
#include <math.h>
#include "USBHardware.h"
#include <string.h>

// TESTINGchar gps_data[500];uint16_t cont_data_buffer = 0;char c;uint32_t receive_byte_gps;uint32_t receive_byte_gps_aux;char rmc_model[5] = "GNRMC";bool flag_is_data_gps = false;bool flag_led_pps_on;struct raw_data raw_data_now;
const sam_usart_opt_t usart_GPS_console_settings = {
	USART_GPS_BAUDRATE,
	USART_GPS_CHAR_LENGTH,
	USART_GPS_PARITY,
	USART_GPS_STOP_BIT,
	US_MR_CHMODE_NORMAL
};

char rmc_on[] =  {"$PSRF103,04,00,01,01*20\r\n"};

/*===================== Functions for GPS Hardware ==================*/
void GPSUsartInit(void) {
	sysclk_enable_peripheral_clock(USART_GPS);
	usart_init_rs232(USART_GPS, &usart_GPS_console_settings,sysclk_get_main_hz());
	usart_enable_tx(USART_GPS);
	usart_enable_rx(USART_GPS);
	
	//usart_enable_interrupt(USART_GPS, US_IER_RXRDY);
	//irq_register_handler(USART0_IRQn,1);
	
	//usart_write_line(USART_GPS, rmc_on);
}

struct raw_data GPSGetRawData(void) {
	usart_getchar(USART_GPS, &receive_byte_gps);
	
	if(receive_byte_gps == '$') {
		
		for(raw_data_now.i=0; raw_data_now.i<sizeof(raw_data_now.cDataBuffer);raw_data_now.i++) {
			usart_getchar(USART_GPS, &receive_byte_gps_aux);
			c = receive_byte_gps_aux;
			
			if(c == '\r') {
				GPSDataValid();
				/* For debug */
				USBWrite(raw_data_now.cDataBuffer, raw_data_now.i); 
				udi_cdc_putc(0x0A);
				//------------------------
				raw_data_now.i = sizeof(raw_data_now.cDataBuffer);
				receive_byte_gps = 0;
			}
			else {
				raw_data_now.cDataBuffer[raw_data_now.i] = c;
			}
		}
	}
	return raw_data_now; 
}

void GPSDataValid(void) {
	if(strncmp(raw_data_now.cDataBuffer, rmc_model, 5) == 0 && raw_data_now.cDataBuffer[16] == 'A') {	
		/* For debug */
		//USBWrite(raw_data_now.cDataBuffer, raw_data_now.i);
		//udi_cdc_putc(0x0A);
		
		GPSRMCtoLatLon(raw_data_now);
		flag_led_pps_on = true;
	}
	else {
		flag_led_pps_on = false;
	}	
}
unsigned char *GPSRMCtoLatLon(struct raw_data data_aux) {
	char lat[10], lon[11];
	//float f_lat, f_lon;
	double f_lat, f_lon, dist;
	
	//lat[0] =  data_aux.cDataBuffer[18];
	//lat[1]  = data_aux.cDataBuffer[19];
	//lat[2]  = data_aux.cDataBuffer[20];
	//lat[3]  = data_aux.cDataBuffer[21];
	//lat[4]  = data_aux.cDataBuffer[22];
	//lat[5]  = data_aux.cDataBuffer[23];
	//lat[6]  = data_aux.cDataBuffer[24];
	//lat[7]  = data_aux.cDataBuffer[25];
	//lat[8]  = data_aux.cDataBuffer[26];
	//lat[9]  = data_aux.cDataBuffer[27];
	
	lat[0] = '2';
	lat[1] = '3';
	lat[2] = '3';
	lat[3] = '1';
	lat[4] = '.';
	lat[5] = '7';
	lat[6] = '1';
	lat[7] = '6';
	lat[8] = '6';
	lat[9] = '1';
	
	stof(lat, &f_lat);
	double lat_dec = convert_to_degrees(f_lat);

	//lon[0]  = data_aux.cDataBuffer[31];
	//lon[1]  = data_aux.cDataBuffer[32];
	//lon[2]  = data_aux.cDataBuffer[33];
	//lon[3]  = data_aux.cDataBuffer[34];
	//lon[4]  = data_aux.cDataBuffer[35];
	//lon[5]  = data_aux.cDataBuffer[36];
	//lon[6]  = data_aux.cDataBuffer[37];
	//lon[7]  = data_aux.cDataBuffer[38];
	//lon[8]  = data_aux.cDataBuffer[39];
	//lon[9]  = data_aux.cDataBuffer[40];	
	//lon[10] = data_aux.cDataBuffer[41];
	////lon[11] = data_aux.cDataBuffer[43];	
	
	lon[0] = '0';
	lon[1] = '4';
	lon[2] = '7';
	lon[3] = '2';
	lon[4] = '9';
	lon[5] = '.';
	lon[6] = '6';
	lon[7] = '7';
	lon[8] = '4';
	lon[9] = '0';
	lon[10] = '4';
	stof(lon, &f_lon);
	double lon_dec = convert_to_degrees(f_lon);
	
	lat_dec = lat_dec *(-1);
	lon_dec = lon_dec *(-1);
	
	double f_lat_ref = -23.532195;
	double f_lon_ref = -47.510166;
	
	GPSDistance (f_lat_ref, f_lon_ref, lat_dec, lon_dec, 'K');
	
	USBWrite(lat, sizeof(lat));
	USBWrite(lon, sizeof(lon));
	udi_cdc_putc(0x0A);
}
void stof(const char* s, double *lat_long_conv_f) {
	double rez = 0, fact =1;
    double ret_converter;	
	
	if (*s == '-'){
		s++;
		fact = -1;
	};
	for (int point_seen = 0; *s; s++){
		if (*s == '.'){
			point_seen = 1;
			continue;
		};
		int d = *s - '0';
		if (d >= 0 && d <= 9){
			if (point_seen) fact /= 10.0f;
			rez = rez * 10.0f + (float)d;
		};
	};
	
	(*lat_long_conv_f) = rez * fact;
};

double convert_to_degrees(double NMEA_lat_long){

	double minutes, dec_deg, decimal;
	int degrees;
	float position;

	degrees = (int)(NMEA_lat_long/100.00);
	minutes = NMEA_lat_long - degrees*100.00;
	dec_deg = minutes / 60.00;
	decimal = degrees + dec_deg;
	
	//if (N_S == 'S' || E_W == 'W') { // return negative
		//decimal *= -1;
	//}
	/* convert raw latitude/longitude into degree format */
	return decimal;
	}

/*=================== Functions for GPS Application =================*/
double GPSDegToRad (double deg) {
	return (deg * pi/180);
}

double GPSRadToDeg (double rad) {
	return (rad * 180/pi);	
}

double GPSDistance (double lat_ref, double long_ref, double lat_now, double long_now, char unit_measure) {
	 double theta, dist;
	 
	 theta = long_ref-long_now;
	 dist = sin(GPSDegToRad(lat_ref)) * sin(GPSDegToRad(lat_now)) + cos(GPSDegToRad(lat_ref)) * cos(GPSDegToRad(lat_now)) * cos(GPSDegToRad(theta));
	 dist = acos(dist);
	 dist = GPSRadToDeg(dist);
	 dist = dist * 60 * 1.1515;
	 
	 switch(unit_measure) {
		 case 'M':
			break;
		 case 'K':
			dist =  dist * 1.609344;
			break;
	     case 'N':
			dist = dist * 0.8684;
		 break;
	 }
	 return(dist);
}
/*================================================================*/