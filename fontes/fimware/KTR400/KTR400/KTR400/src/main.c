/****************************************************************
*					Project Name: KTR400						*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1.0/0.1.0;									*
* Data: 19/02/2018;												*
* Notes: main.c is the code of application;						*
*****************************************************************/

/*==================== List of Files System ====================*/
#include <asf.h>
#include <string.h>
#include <stdlib.h>
#include "MyApp.h"
#include "USBHardware.h"
#include "GPSConfigApp.h"

/*================ Definitions Global Variables ================*/
uint8_t buf_recv_pc[MAX_LENGHT_BUFFER_USB];
uint8_t flag_usb_is_recv_data;
static bool my_flag_autorize_cdc_transfert = false;
uint8_t cont_tick_led = 0;
uint8_t cont_buf_recv_data_pc = 0;
bool flag_to_active_melody = true;
bool flag_en_exit_time = false;
bool flag_led_pps_on = false;
bool state_led_op = false;
uint16_t cont_tick_time_func = 0;

struct raw_data raw_data_now_aux;
/*================ Signature Of Function Prototype =============*/
void (*PointerFunction)(void);

/*================= Functions Control Module USB ===============*/
bool my_callback_cdc_enable(void) {
	my_flag_autorize_cdc_transfert = true;
	return true;
}

void my_callback_cdc_disable(void) {
	my_flag_autorize_cdc_transfert = false;
}

void my_callback_rx_notify(uint8_t port) {	
	if (my_flag_autorize_cdc_transfert) {
		cont_buf_recv_data_pc = USBGetNumberBytes();
		USBRead(buf_recv_pc, cont_buf_recv_data_pc);
		flag_usb_is_recv_data = true;
	}
}
/*======================= Systick Timer ========================*/
void SysTick_Handler(void) {
	cont_tick_led++;	
	
	if(cont_tick_led >= TIME_PERIOD_LED) {
		cont_tick_led = 0;
		
		if(state_led_op) {
			ioport_set_pin_level(LED_OP_PIN, state_led_op);
			if(flag_led_pps_on)
				ioport_set_pin_level(LED_PPS_PIN, state_led_op);
			state_led_op = 0;
		}
		else {
			ioport_set_pin_level(LED_OP_PIN, state_led_op);
			if(flag_led_pps_on)
				ioport_set_pin_level(LED_PPS_PIN, state_led_op);
			state_led_op = 1;
		}
		if(!flag_led_pps_on) 
			ioport_set_pin_level(LED_PPS_PIN, LOW);
	}
	if(flag_en_exit_time == true) {
		cont_tick_time_func++;
		
		if(cont_tick_time_func >= TIME_MAX_WAIT_FUNC) {
			flag_en_exit_time = false;
			cont_tick_time_func = 0;
		}
	}
	else {
		cont_tick_time_func = 0;
	}    
}
/*====================== Main Function =========================*/
int main(void) {
	irq_initialize_vectors();
	Enable_global_interrupt();
	
	InitMicrocontroller();
	PointerFunction = MySystemInit;
	ioport_set_pin_level(LED_PPS_PIN,HIGH);
	
	while (true) {
		//(*PointerFunction)();
		GPSRMCtoLatLon(raw_data_now_aux);
	}
}
/*==============================================================*/
