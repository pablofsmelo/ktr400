/*********************************************************************
 *					File Name: MP3ConfigApp.h						 *
 * Project Name: KTR400												 *
 * Created: 10/04/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: This module deals with the DFPLAYER MINI 					 *
 ********************************************************************/
#include <asf.h>
#include "MyApp.h"
#include "MP3ConfigApp.h"
#include "LCDConfigApp.h"
#include "FlashHardware.h"
#include "MEMConfigApp.h"

static uint8_t send_buf [MAX_LENGHT_BUFFER_MP3] = {0};
bool flag_volume_is_ready = false;	
struct init_mem PARAMETERS_INIT;

const sam_usart_opt_t usart_MP3_console_settings = {
	USART_MP3_BAUDRATE,
	USART_MP3_CHAR_LENGTH,
	USART_MP3_PARITY,
	USART_MP3_STOP_BIT,
	US_MR_CHMODE_NORMAL
};

/*=================== Functions Control for MP3 ===================*/
void MP3UartInit(void) {
	sysclk_enable_peripheral_clock(USART_MP3);
	usart_init_rs232(USART_MP3, &usart_MP3_console_settings,sysclk_get_main_hz());
	usart_enable_tx(USART_MP3);
	usart_enable_rx(USART_MP3);
	
	GetDataFromUserPage(USER_VAR_IS_INIT, (uint8_t*)&PARAMETERS_INIT, sizeof(PARAMETERS_INIT));
	
	MP3SendCmd(0x60, 0, PARAMETERS_INIT.value_volume_system);
}

void MP3SendUART(uint8_t len) {
	uint8_t i = 0;
	usart_putchar(USART_MP3, START_BYTE_CMD);
	
	for(i=0; i<len ; i++) {
		usart_putchar(USART_MP3, send_buf[i]);
	}
	usart_putchar(USART_MP3, END_BYTE_CMD);
}

void MP3Checksum(uint8_t *str, uint8_t len) {
	uint16_t xorsum = 0;
	uint8_t i;
	
	for(i = 0; i<len; i++) {
		xorsum = xorsum + str[i];
	}
	xorsum = 0-xorsum;
	
	*(str+i) = (uint8_t)(xorsum>>8);
	*(str+1+i) = (uint8_t)(xorsum&0x00ff);
}

void MP3PlayMusic(uint8_t val_music) {
	MP3SendCmd(SPECIFY_SONG_CMD, 0, val_music);
}

void MP3SendCmd (uint8_t cmd, uint16_t feedback, uint16_t dat) {
	send_buf[0] = VERSION_BYTE_CMD;
	send_buf[1] = NUMBER_OF_BYTES_CMD;
	send_buf[2] = cmd;
	send_buf[3] = feedback;
	send_buf[4] = (uint8_t)(dat>>8);
	send_buf[5] =(uint16_t)(dat);
	MP3Checksum(&send_buf[0], 6);
	MP3SendUART(8);
}

void MP3ResetModule(void) {
	MP3SendCmd(SPECIFY_RESET_CMD, 0, 0x00);
}

void MP3StopModule(void) {
	MP3SendCmd(SPECIFY_STOP_CMD, 0, 0x00);
}

void MP3AjustVolume (uint8_t val_volume) {
	MP3SendCmd(SPECIFY_VOLUME_CMD, 0, val_volume);
}

/*=============== Functions Applicaiton using MP3 ================*/
void MP3NewVolume(void) {
	uint8_t cont_volume = 0;
	flag_volume_is_ready = false;

	GetDataFromUserPage(USER_VAR_IS_INIT, (uint8_t*)&PARAMETERS_INIT, sizeof(PARAMETERS_INIT));
		
	LCDClear();
	LCDPrintString(1,0, "Ult.Vol:");
	LCDSetCursor(2,3);
	LCDWriteInt(PARAMETERS_INIT.value_volume_system);
	delay_ms(TIME_TO_WRITE_LAST_VOL);
	
	LCDClear();
	LCDPrintString(1,1, "Volume");
	LCDSetCursor(2,3);
	LCDWriteInt(cont_volume);
	delay_ms(TIME_TO_WRITE_NEW_VOL);
	
	flag_en_exit_time = false;
	
	while(flag_volume_is_ready == false) {
		flag_en_exit_time = true;
		LCDSetCursor(2,3);
		LCDWriteInt(cont_volume);
		
		if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
			flag_volume_is_ready = true;
			PARAMETERS_INIT.value_volume_system = cont_volume;
			break;
		}
		if(!ioport_get_pin_level(UP_BT_PIN)) {
			flag_en_exit_time = false;
			if(cont_volume < VALUE_VOLUME_MAX) cont_volume++;
			delay_ms(TIME_TO_WRITE_LCD);
		}	
		if(!ioport_get_pin_level(DOWN_BT_PIN)) {
			flag_en_exit_time = false;
			if(cont_volume > VALUE_VOLUME_MIN) cont_volume--;
			delay_ms(TIME_TO_WRITE_LCD);
		}
		if(!ioport_get_pin_level(ENTER_BT_PIN)) {
			flag_en_exit_time = false;
			PARAMETERS_INIT.value_volume_system = cont_volume;
			StoreDataOnUserPage(USER_VAR_IS_INIT,&PARAMETERS_INIT,sizeof(PARAMETERS_INIT));
			LCDClear();
			LCDPrintString(1,0, "Nov.Vol");
			LCDPrintString(2,0, "Sucesso");
			delay_ms(TIME_TO_WRITE_SUCCESS);
			flag_volume_is_ready = true;
			break;
		}
		if(!ioport_get_pin_level(CANCEL_BT_PIN)) {
			flag_volume_is_ready = true;
			break;
		}
	}
}

void MP3VolumeDefaultMem(void) {
	PARAMETERS_INIT.value_volume_system = VOLUME_DEFAULT;
	StoreDataOnUserPage(USER_VAR_IS_INIT,&PARAMETERS_INIT,sizeof(PARAMETERS_INIT));
}