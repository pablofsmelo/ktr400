/*********************************************************************
 *						File Name: ProtocolCOM.c					 *
 * Project Name: KTR400												 *
 * Created: 23/02/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: Protocol of Communication between Microcontroller and PC	 *
 ********************************************************************/
#include <asf.h>
#include "ProtocolCOM.h"
#include "LCDConfigApp.h"
#include "USBHardware.h"
#include "MEMConfigApp.h"

uint8_t cont_script = 0;
uint8_t buf_recv_pc[MAX_LENGHT_BUFFER_USB];
extern uint8_t cont_buf_recv_data_pc;
struct DATA_POSITION_MEM script_pos_mem;
bool flag_script_is_ready = false;

/*==== Functions for communication protocol between PC and device =====*/
void SendACK(void) {
	char buf_ack[LENGHT_BUF_ACK];
	
	buf_ack[SOURCE_COMMUNICATION] = ADDRESS_KTR400_CMD;
	buf_ack[DESTINATION_COMMUNICATION] = ADDRESS_PC_CMD;
	buf_ack[COMMAND_COMMUNICATION] = ACK_CMD;
	buf_ack[QTD_BYTES_COMMUNICATION]= 0;
	buf_ack[CHECKSUM_BYTE_COMMUNICATION]= CHECKSUM_ACK_VALUE;
	
	USBWrite(buf_ack, LENGHT_BUF_ACK);
}

/*================== Script Clock ==================*/
bool CheckIsInitScriptClock(unsigned char *buf) {
	bool state_init_position = false;
	
	if (buf[0] == ADDRESS_PC_CMD  && buf[1] == ADDRESS_KTR400_CMD &&
		buf[2] == START_CLOCK_CMD && buf[4] == CHECKSUM_CLOCK_CMD) {
		LCDClear();
		LCDPrintString(1,1, "Inicio");
		LCDPrintString(2,0, "Gravacao");
		delay_ms(2000);
		LCDClear();
		
		state_init_position = true;
	}
	return state_init_position;
}

bool CheckIsFinishScriptClock(unsigned char *data_bytes) {
	if(data_bytes[0] = 0x00 && data_bytes[1] == 0x01 && data_bytes[2] == 0x21 && data_bytes[4] == data_bytes[0x22]) 
		return true;
	else 
		return false;
}

void ReceiveScriptClock(void) {
	flag_script_is_ready = false;
	
	if(USBCheckIsConnected()) {
		while (flag_script_is_ready == false) {
			LCDClear();
			LCDPrintString(1,0,"Esp.Grav");
			LCDPrintString(2,1, "Hor.PC");
							
			while(!CheckIsInitScriptClock(buf_recv_pc)) {
				if(!ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_script_is_ready = true;
					break;
				}
			}
			if(flag_script_is_ready == false) {
				LCDPrintString(1,0, "Apag.Rot");
				LCDPrintString(2,0, "Antigos.");
				MEMErase();			
				SendACK();
			
				LCDClear();
				LCDPrintString(1,0, "Grav.Hor");
				LCDPrintString(2,1, "Rot:");
				LCDWriteInt(cont_script);

				while(cont_buf_recv_data_pc != 6 && buf_recv_pc[2] != 0x21 && buf_recv_pc[4] != 0x22) {
					LCDSetCursor(2,5);
					LCDWriteInt(cont_script);
								
					if(cont_buf_recv_data_pc == 6) {
						cont_script == 0? cont_script = 1 : cont_script++;
						MEMSetClockScript(buf_recv_pc, cont_script);
						SendACK();
						cont_buf_recv_data_pc = 0;
					}
					if(buf_recv_pc[2] == 0x21 && buf_recv_pc[4] == 0x22) {
						SendACK();
						LCDClear();
						LCDPrintString(1,0, "Grav.Rot");
						LCDPrintString(2,0, "Sucesso.");
						delay_ms(3000);
						cont_script = 0;
						flag_script_is_ready = true;
						LCDClear();
					}
					if(cont_script >= QTD_MAX_SCRIPTS) {
						LCDClear();
						LCDPrintString(1,0, "Grav.Rot");
						LCDPrintString(2,0, "Max: 100");
						//LCDPrintString(2,3, "Rot.");
						delay_ms(3000);
						cont_script = 0;
						flag_script_is_ready = true;
						break;
					}
				}
			}
		}	
	}
}

/*================== Script Position ==================*/

bool CheckIsInitScriptPosition(unsigned char *buf) {
	bool state_init_position = false;
	
	if (buf[0] == ADDRESS_PC_CMD     && buf[1] == ADDRESS_KTR400_CMD &&
	    buf[2] == START_POSITION_CMD && buf[4] == CHECKSUM_POSITION_CMD) {
		LCDClear();
		LCDPrintString(1,1, "Inicio");
		LCDPrintString(2,0, "Gravacao");
		delay_ms(2000);
		LCDClear();
		
		state_init_position = true;
	}
	return state_init_position;
}

void ReceiveScriptPosition(void) {
	flag_script_is_ready = false;
	
	if(USBCheckIsConnected()) {	
		while (flag_script_is_ready == false) {
			LCDClear();
			LCDPrintString(1,0,"Esp.Grav");
			LCDPrintString(2,1,"GPS.PC");
							
			while(!CheckIsInitScriptPosition(buf_recv_pc)) {
				if(!ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_script_is_ready = true;
					break;
				}
			}
			if(flag_script_is_ready == false) {	
				LCDPrintString(1,0, "Apag.Rot");
				LCDPrintString(2,0, "Antigos.");
				MEMErase();				
				SendACK();
							
				LCDClear();
				LCDPrintString(1,0, "Grav.GPS");
				LCDPrintString(2,1, "Rot:");

				while(cont_buf_recv_data_pc != 6 && buf_recv_pc[2] != 0x11 && buf_recv_pc[4] != 0x12) {
					LCDSetCursor(2,5);
					LCDWriteInt(cont_script);
								
					if(cont_buf_recv_data_pc == 20) {
						cont_script == 0? cont_script = 1 : cont_script++;
						MEMSetPositionScript(buf_recv_pc, cont_script);
						SendACK();
						cont_buf_recv_data_pc = 0;
					}
					if(buf_recv_pc[2] == 0x11 && buf_recv_pc[4] == 0x12) {
						SendACK();
						LCDClear();
						LCDPrintString(1,0, "Grav.Rot");
						LCDPrintString(2,0, "Sucesso.");
						delay_ms(3000);
						flag_script_is_ready = true;
						cont_script = 0;
						LCDClear();
					}
					if(cont_script >= QTD_MAX_SCRIPTS) {
						LCDClear();
						LCDPrintString(1,0, "Grav.Rot");
						LCDPrintString(2,0, "Max: 100");
						//LCDPrintString(2,3, "Rot.");
						delay_ms(3000);
						cont_script = 0;
						flag_script_is_ready = true;
						LCDClear();
						break;
					}
				}
			}
		}
	}
}
/*================================================================*/






