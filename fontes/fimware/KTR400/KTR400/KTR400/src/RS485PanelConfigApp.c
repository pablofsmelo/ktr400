/****************************************************************
*				File Name: RS485PanelConfigApp.c				*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes:														*
****************************************************************/
#include <asf.h>
#include "RS485PanelConfigApp.h"

const sam_usart_opt_t usart_485_console_settings = {
	USART_485_BAUDRATE,
	USART_485_CHAR_LENGTH,
	USART_485_PARITY,
	USART_485_STOP_BIT,
	US_MR_CHMODE_NORMAL
};

/*=============== Functions for RS232 Protocol ================*/
void RS485Init(void) {
	usart_init_rs232(USART_485, &usart_485_console_settings,sysclk_get_main_hz());
	usart_enable_tx(USART_485);
	usart_enable_rx(USART_485);
		
	ioport_set_pin_level(RD_RS485_PIN,IOPORT_PIN_LEVEL_LOW);
}

void RS485EnableClock(void) {
	sysclk_enable_peripheral_clock(USART_485);
}

void RS485SendBuffer(uint8_t *bf, uint8_t leg) {	
	ioport_set_pin_level(RD_RS485_PIN,IOPORT_PIN_LEVEL_HIGH);
	//USARTWriteBuffer(USART_485,bf, leg);
	ioport_set_pin_level(RD_RS485_PIN,IOPORT_PIN_LEVEL_LOW);
}

void RS485SendSingle(uint8_t snd) {
	ioport_set_pin_level(RD_RS485_PIN,IOPORT_PIN_LEVEL_HIGH);
	//USARTWriteChar(USART_485,snd);
	ioport_set_pin_level(RD_RS485_PIN,IOPORT_PIN_LEVEL_LOW);
}

void RS485InitInterrupt(void) {
	//USARTEnableInterrupt(USART_485, 1);
}
/*================================================================*/
