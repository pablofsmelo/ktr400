/****************************************************************
*				File Name: RS232PanelConfigApp.h				*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes:														*
****************************************************************/
#ifndef RS232CONFIG_H_
#define RS232CONFIG_H_

/*================ Define for RS232 Protocol ==================*/
#define USART_232_PANEL             USART2
#define USART_232_PANEL_ID		    ID_USART2
#define USART_SERIAL_ISR_HANDLER    USART2_Handler
#define USART_232_PANEL_BAUDRATE	38400
#define USART_232_PANEL_CHAR_LENGTH US_MR_CHRL_8_BIT
#define USART_232_PANEL_PARITY	    US_MR_PAR_NO
#define USART_232_PANEL_STOP_BIT    US_MR_NBSTOP_1_BIT

/*=============== Functions for RS232 Protocol ================*/
void Rs232PanelInit			 (void);
void Rs232PanelEnableClock	 (void);
void Rs232PanelSendBuffer	 (uint8_t *bf, uint8_t leg);
void Rs232PanelSendChar		 (uint8_t snd);
void Rs232PanelInitInterrupt (void);


#endif /* RS232CONFIG_H_ */