/****************************************************************
*					File Name: TWIHardware.h					*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes: This library was created based on the I2C protocol		*
****************************************************************/
#ifndef TWIHARDWARE_H_
#define TWIHARDWARE_H_ 

/*=============== Defines TWI Hardware Config =================*/
#define TWIM_USER		 TWIM2
#define TWIM_SPEED		 100000 /* 100KHz */

/*=============== Functions TWI Hardware Config ===============*/
void TWIClockEnable	  (void);
void TWIInit		  (void);
void TWIBufferSend	  (uint32_t reg_addr,uint8_t reg_addr_l,uint8_t addres_chip,void *buffer,uint8_t length_buffer);
void TWIBufferReceive (uint32_t reg_addr,uint8_t reg_addr_l,uint8_t addres_chip,void *buffer,uint8_t length_buffer);


#endif /* TWIHARDWARE_H_ */