/****************************************************************
*				File Name: RS485PanelConfigApp.h				*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes:														*
****************************************************************/
#ifndef RS485CONFIG_H_
#define RS485CONFIG_H_

/*================ Defines to RS485 Protocol ==================*/
#define USART_485                 USART2
#define USART_485_ID			  ID_USART2
#define USART_SERIAL_ISR_HANDLER  USART2_Handler
#define USART_485_BAUDRATE		  38400
#define USART_485_CHAR_LENGTH	  US_MR_CHRL_8_BIT
#define USART_485_PARITY		  US_MR_PAR_NO
#define USART_485_STOP_BIT		  US_MR_NBSTOP_1_BIT 

/*=============== Functions for RS485 Protocol ================*/
void RS485Init			(void);
void RS485EnableClock	(void);
void RS485SendBuffer	(uint8_t *bf, uint8_t leg);
void RS485SendChar		(uint8_t snd);
void RS485InitInterrupt (void);

#endif /* RS485CONFIG_H_ */