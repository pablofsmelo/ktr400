/*********************************************************************
 *				  File Name: LCDConfigApp.c						     *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#include <asf.h>
#include "DefinesConfigApp.h"
#include "LCDConfigApp.h"
#include "RTCConfigApp.h"

struct date_rtc _date_now;

/*==================== Functions for LCD Settings ==================*/
void LCDInit(void) {
	LCDStart();
	LCDClear();
}

void LCDSendNibble(char data_bit) {
	if(data_bit&0x01) ioport_set_pin_level(D4_LCD_PIN, HIGH);
	else			  ioport_set_pin_level(D4_LCD_PIN, LOW);

	if(data_bit&0x02) ioport_set_pin_level(D5_LCD_PIN, HIGH);
	else			  ioport_set_pin_level(D5_LCD_PIN, LOW);

	if(data_bit&0x04) ioport_set_pin_level(D6_LCD_PIN, HIGH);
	else		      ioport_set_pin_level(D6_LCD_PIN, LOW);

	if(data_bit&0x08) ioport_set_pin_level(D7_LCD_PIN, HIGH);
	else			  ioport_set_pin_level(D7_LCD_PIN, LOW);
	
	delay_us(200);
	
	ioport_set_pin_level(EN_LCD_PIN, HIGH);
	delay_us(500);
	ioport_set_pin_level(EN_LCD_PIN, LOW);
	delay_us(500);
}

void LCDSendCmd(char command) {
	ioport_set_pin_level(RS_LCD_PIN, LOW);
	delay_us(500);
	ioport_set_pin_level(EN_LCD_PIN, LOW);
	LCDSendNibble((command>>4)&0x0F);
	delay_us(100);
	LCDSendNibble(command&0x0F);
	delay_us(100);
}

void LCDStart(void) {
	delay_ms(40);
	
	ioport_set_pin_level(RS_LCD_PIN, LOW);
	ioport_set_pin_level(EN_LCD_PIN, LOW);
	ioport_set_pin_level(D4_LCD_PIN, LOW);
	ioport_set_pin_level(D5_LCD_PIN, LOW);
	ioport_set_pin_level(D6_LCD_PIN, LOW);
	ioport_set_pin_level(D7_LCD_PIN, LOW);
	
	LCDSendNibble(START_LCD_CMD);
	delay_ms(5);
	LCDSendNibble(START_LCD_CMD);
	delay_us(300);
	
	LCDSendNibble(START_LCD_CMD);
	LCDSendNibble(RETURN_TO_HOME_CMD);
	LCDSendCmd(LCD_4_BITS_CMD);
	LCDSendCmd(LCD_ON_CMD);
	LCDSendCmd(CLEAR_LCD_CMD);
}

void LCDPrintChar(char data) {
	ioport_set_pin_level(RS_LCD_PIN, HIGH);
	delay_us(500);
	ioport_set_pin_level(EN_LCD_PIN, LOW);
	LCDSendNibble((data>>4)&0x0F);
	delay_us(100);
	LCDSendNibble(data&0x0F);
	delay_us(100);
}

void LCDSetCursor(char row, char column) {
	if(row == 0x01 && column <= 0x0F) LCDSendCmd(0x80|column);
	if(row == 0x02 && column <= 0x0F) LCDSendCmd(0x80|0x40|column);
}

void LCDPrintString(char row, char column, char *str) {
	LCDSetCursor(row, column);
	for(int i=0;str[i]!='\0';i++) LCDPrintChar(str[i]);
}

void LCDWriteFloat(float data_value) {	
	unsigned char i, dig[5], cont_dig;
	unsigned int a;
	
	if(data_value<1000.0)						  cont_dig = 4;
	if(data_value>=1000.0 && data_value <=10000.0) cont_dig = 5;
	
	a = (unsigned int)(data_value * 10.0 + 0.5);
	
	for(i =0; i<cont_dig; i++) {
		dig[i] = (unsigned char)(a % 10);
		a /= 10;
	}
	if(cont_dig == 4) {
		if(!dig[3]) LCDPrintChar(' ');
		else		LCDPrintChar(dig[3]  + '0');
		
		LCDPrintChar(dig[2] + '0');
		LCDPrintChar(dig[1] + '0');
		LCDPrintChar('.');
		LCDPrintChar(dig[0] + '0');
	}
	if(cont_dig == 5) {
		if(!dig[4]) LCDPrintChar(' ');
		else		LCDPrintChar(dig[4] + '0');
		
		LCDPrintChar(dig[3] + '0');
		LCDPrintChar(dig[2] + '0');
		LCDPrintChar(dig[1] + '0');
		LCDPrintChar('.');
		LCDPrintChar(dig[0] + '0');
	}
}

void LCDWriteInt(int data_value) {
	unsigned char i, dig[5], cont_dig;
	unsigned int a;
	
	if(data_value<1000)						  cont_dig = 4;
	if(data_value>=1000 && data_value<=99999) cont_dig = 5;
	
	a = (unsigned int)(data_value * 10.0 + 0.5);
	
	for(i =0; i<cont_dig; i++) {
		dig[i] = (unsigned char)(a % 10);
		a /= 10;
	}
	if(cont_dig == 4) {
	
		if(dig[3]) LCDPrintChar(dig[3]  + '0');
		
		LCDPrintChar(dig[2] + '0');
		LCDPrintChar(dig[1] + '0');
	}
	if(cont_dig == 5) {
		
		if(dig[4]) LCDPrintChar(dig[4] + '0');
		
		LCDPrintChar(dig[3] + '0');
		LCDPrintChar(dig[2] + '0');
		LCDPrintChar(dig[1] + '0');
	}
}

void LCDClear(void) {
	LCDSendCmd(FORCE_CURSOR_1_LINE);
	LCDSendCmd(CLEAR_LCD_CMD);
	delay_ms(100);
}
/*================================================================*/


