/****************************************************************
*					File Name: RTCConfigApp.h					*
* Author: Pablo Melo;											*
* Version FW/HW: 0.1											*
* Notes: This library was created based on DS1307/DS1339		*
****************************************************************/
#ifndef RTCCONFIG_H_
#define RTCCONFIG_H_

/*================ Defines for RTC Settings ====================*/
#define ADDR_WRITE_MODE	        0xD0

#define ADDR_SEC_RTC	        0x00
#define ADDR_MIN_RTC	        0x01
#define ADDR_HOUR_RTC	        0x02
#define ADDR_DAY_RTC	        0x04
#define ADDR_MONTH_RTC	        0x05
#define ADDR_YEAR_RTC	        0x06

#define SEC_RTC_DEFAULT         0x00
#define MIN_RTC_DEFAULT         0x00
#define HOUR_RTC_DEFAULT        0x00
#define DAY_RTC_DEFAULT	        0x01
#define MONTH_RTC_DEFAULT       0x01
#define YEAR_RTC_DEFAULT        0x01

#define SELECT_NEW_DAY_RTC	    0
#define SELECT_NEW_MONTH_RTC    1
#define SELECT_NEW_YEAR_RTC	    2
#define SELECT_NEW_HOUR_RTC	    3
#define SELECT_NEW_MIN_RTC	    4
#define SELECT_NEW_SEC_RTC	    5

#define PAR_DAY_MAX			    31
#define PAR_MONTH_MAX		    12
#define PAR_YEAR_MAX		    99
#define PAR_HOUR_MAX		    23
#define PAR_MINUTE_MAX		    59
#define PAR_SECOND_MAX		    59

#define PAR_MONTH_IMPAR_OR_PAR  2

#define SPECIAL_CHARACTER		0b01011111
#define TIME_WRITE_CHAR			150
#define TIME_BUTTON_RTC			300	
#define TIME_WRITE_SPECIAL_CHAR	300
#define TIME_MENSAGE_ERR_OR_SUC 2000
#define TIME_MENSAGE_INIT_RTC	500

typedef struct date_rtc{
	char second;
	char minute;
	char hour;
	char week_day;
	char day;
	char month;
	char year;
}DATE_RTC_AUX;

extern struct date_rtc modify_date;
extern struct date_rtc date_now;

/*================ Functions for RTC Settings ==================*/
void	 RTCSytemInit		  (void); /* This function should be called at system startup */
void	 RTCDS1307Init        (void);
void	 RTCDS1307SetDateTime (uint32_t address_command, uint8_t data);
uint16_t RTCDS1307GetDateTime (uint32_t address_command);
uint8_t  RTCDS1307BcdToDec	  (uint16_t val_bcd);
uint16_t RTCDS1307DecToBcd	  (uint16_t num);

/*================ Functions for RTC Applition =================*/
void	 RTCDS1307NewDateTime (void);
void	 RTCDS1307StartDefault(void);

#endif /* RTCDS1307_PLATFORMARM_H_ */