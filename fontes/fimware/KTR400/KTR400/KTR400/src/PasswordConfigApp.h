/*********************************************************************
 *					File Name: PasswordConfigApp.h				     *
 * Project Name: KTR400												 *
 * Created: 10/04/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: 															 *
 ********************************************************************/
#ifndef PASSWORDCONFIGAPP_H_
#define PASSWORDCONFIGAPP_H_

/*===================== Defines for Password =======================*/
#define SELECT_FIRST_DIGIT		1
#define SELECT_SECOND_DIGIT		2
#define SELECT_THIRD_DIGIT		3
#define SELECT_FOURTH_DIGIT		4
#define VALUE_MAX_DIGIT			9
#define VALUE_MIN_DIGIT			0

typedef struct digits {
	uint8_t first_digit;
	uint8_t second_digit;
	uint8_t third_digit;
	uint8_t fourth_digit;
}PASSWORD_STRUCT;

extern struct digits password_dig;
uint16_t flag_password_already_initialized;

/*==================== Functions for Password ======================*/
void PasswordSystemInit		(void);
bool PasswordControl		(void);
void NewPassword			(void);
bool CheckPasswordIsCorrect (void);
void PositionDigitLCD		(uint8_t position_digit, uint8_t val_digit);
bool CheckPasswordMem	    (struct digits psw);
void CheckButtonPassword	(uint8_t case_sw);
void PasswordDefault		(void);

#endif /* PASSWORDCONFIGAPP_H_ */