/*********************************************************************
 *					File Name: MyApp.c								 *
 * Project Name: KTR400												 *
 * Created: 10/04/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: This module deals with the state machine					 *
 ********************************************************************/
#include <asf.h>
#include <asf.h>
#include "DefinesConfigApp.h"
#include "FlashHardware.h"
#include "GPSConfigApp.h"
#include "LCDConfigApp.h"
#include "MEMConfigApp.h"
#include "MP3ConfigApp.h"
#include "MyApp.h"
#include "ProtocolCOM.h"
#include "PasswordConfigApp.h"s
#include "RS232PanelConfigApp.h"
#include "RS485PanelConfigApp.h"
#include "RTCConfigApp.h"
#include "USBHardware.h"

uint8_t cont_buf_recv_data_pc;
uint8_t control_case_settings = 0;
uint8_t cont_check_script_clock = 1;
uint8_t sweep_buf = 1;

struct init_mem PARAMETERS_INIT;
struct DATA_CLOCK_MEM script_clk_mem;
struct date_rtc date_now;

struct raw_data raw_data_now;

extern uint8_t buf_recv_pc[MAX_LENGHT_BUFFER_USB];
extern uint8_t flag_usb_is_recv_data;
extern bool flag_to_active_melody;

/*================= Function  For Initializations ================*/
void InitMicrocontroller(void) {
	sysclk_init();
	board_init();
	delay_init(sysclk_get_cpu_hz());
	SysTick_Config(sysclk_get_cpu_hz()/FREQUENCY_HZ_BASE_SYSTICK); /* In this case systick is 1s*/
	delay_us(500);
	configHardwareKTR400();
}

void configHardwareKTR400 (void) {
	GetDataFromUserPage(USER_VAR_IS_INIT, (uint8_t*)&PARAMETERS_INIT, sizeof(PARAMETERS_INIT));

	LCDInit();
 	RTCSytemInit();
	PasswordSystemInit();
	USBInit();
	MP3UartInit();
	delay_ms(10);
	MEMInit();
	
	ioport_set_pin_level(CS_SPI_MEM_PIN,IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_level(WP_MEM_PIN,IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_level(RST_MEM_PIN,IOPORT_PIN_LEVEL_HIGH);
	
	GPSUsartInit();
	flag_to_active_melody = true;
}

/*====================== Finite State Machines =====================*/
void MySystemInit(void) {
	LCDPrintString(1, 1, "KTR400");
	LCDPrintString(2, 0, "Ver. 1.0");
	delay_ms(2500);
	LCDClear();	
	
	PointerFunction = ShowDateHour;
}

void ShowDateHour(void) {	
	PointerFunction = ShowDateHour;
	//UpdateRTC();
	//CheckEnableMelodyClock(date_now); 
	raw_data_now = GPSGetRawData();
	
	/*if(!ioport_get_pin_level(ENTER_BT_PIN)) { //Modificar para PLAY
		LCDClear();
		PointerFunction = TestMelody;
	}*/
	
	if(!ioport_get_pin_level(ENTER_BT_PIN)) { // Modificar para ENTER
		LCDClear();
		PointerFunction = ModifySystemSettings;
	}
}
void TestMelody(void) {	
	LCDClear();
	LCDPrintString(1,1,"Audio");
	LCDPrintString(2,0,"Acionado");
	ioport_set_pin_level(LED_ALARM_PIN, HIGH);
	MP3AjustVolume(PARAMETERS_INIT.value_volume_system);
	delay_ms(TIME_TO_WRITE_CMD);
	MP3PlayMusic(SONG_DEFAULT);
	while(ioport_get_pin_level(CANCEL_BT_PIN));
	
	MP3StopModule();
	delay_ms(TIME_TO_WRITE_CMD);
	ioport_set_pin_level(LED_ALARM_PIN, LOW);
	
	PointerFunction = ShowDateHour;
}

void ModifySystemSettings(void) {
	PointerFunction = ModifySystemSettings;
	
	flag_en_exit_time = true;
	
	LCDPrintString(1,0,"Mod Cfg?");
	LCDPrintString(2,0,"E-S  C-N");
	delay_ms(TIME_MODIFY_SETTINGS);
		
	if(!ioport_get_pin_level(ENTER_BT_PIN)) {
		LCDClear();
		PointerFunction = Settings;
	}
	if(!ioport_get_pin_level(CANCEL_BT_PIN)) {
		LCDClear();
		PointerFunction = ShowDateHour;
	}
	if(flag_en_exit_time == false) {
		PointerFunction = ShowDateHour;
	}
}
void Settings(void){
	bool check_password;
	script_clock content_clk_mem;
	uint32_t cont_time_exit_reset = 0;
			
	PointerFunction = Settings;
	
	control_case_settings = MODIFY_DATE_CASE;
	check_password = CheckPasswordIsCorrect();
	
	flag_en_exit_time =  false;
	
	while(ioport_get_pin_level(CANCEL_BT_PIN) && check_password == true) {
		
		flag_en_exit_time = true;
		
		switch(control_case_settings) 
		{
			case MODIFY_DATE_CASE:
				LCDPrintString(1,0,">Data");
				LCDPrintString(2,0," Senha");
			
				if(flag_en_exit_time == false) {
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}		
				if(!ioport_get_pin_level(ENTER_BT_PIN)) {
					LCDClear();
					RTCDS1307NewDateTime();
					flag_en_exit_time = false;
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}	
				if(!ioport_get_pin_level(DOWN_BT_PIN)){
					LCDClear();
					flag_en_exit_time = false;
					control_case_settings = MODIFY_PASSWORD_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
			break;
		
			case MODIFY_PASSWORD_CASE:
				LCDPrintString(1,0,">Senha");
				LCDPrintString(2,0," Volume");
				
				if(flag_en_exit_time == false) {
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(ENTER_BT_PIN)) {
					LCDClear();
					NewPassword();
					flag_en_exit_time = false;
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(UP_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;					
					control_case_settings = MODIFY_DATE_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
				if(!ioport_get_pin_level(DOWN_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;					
					control_case_settings = MODIFY_VOLUME_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
			break;
		
			case MODIFY_VOLUME_CASE:
				LCDPrintString(1,0,">Volume");
				LCDPrintString(2,0," PC Hor");
				
				if(flag_en_exit_time == false) {
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(ENTER_BT_PIN)) {
					MP3NewVolume();
					flag_en_exit_time = false;
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(UP_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;
					control_case_settings = MODIFY_PASSWORD_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
				if(!ioport_get_pin_level(DOWN_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;
					control_case_settings = MODIFY_SCRIPT_CLOCK_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
			break;
		
			case MODIFY_SCRIPT_CLOCK_CASE:
				LCDPrintString(1,0,">PC Hor");
				LCDPrintString(2,0," PC GPS");
				
				if(flag_en_exit_time == false) {
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(ENTER_BT_PIN)) {
					flag_en_exit_time = false;
					ReceiveScriptClock();
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(UP_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;
					control_case_settings = MODIFY_VOLUME_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);   
				}
				if(!ioport_get_pin_level(DOWN_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;
					control_case_settings = MODIFY_SCRIPT_LOCALIZATION_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
			break;
		
			case MODIFY_SCRIPT_LOCALIZATION_CASE:
				LCDPrintString(1,0,">PC GPS");
				LCDPrintString(2,0," Reset");
				
				if(flag_en_exit_time == false) {
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}	
				if(!ioport_get_pin_level(ENTER_BT_PIN)) {
					flag_en_exit_time = false;
					ReceiveScriptPosition();
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(UP_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;
					control_case_settings = MODIFY_SCRIPT_CLOCK_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
				if(!ioport_get_pin_level(DOWN_BT_PIN)) {
					LCDClear();
					flag_en_exit_time = false;
					control_case_settings = RESET_KTR400_DEFAULT_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
			break;
			
			case RESET_KTR400_DEFAULT_CASE:
				LCDPrintString(1,0,">Reset");
				
				if(!ioport_get_pin_level(ENTER_BT_PIN)) {
					LCDClear();
					LCDPrintString(1,0,"RST Cfg?");
					LCDPrintString(2,0,"E-S  C-N");
					delay_ms(TIME_QUESTION_RST_DEFAULT);
					flag_en_exit_time = true;
					
					while(ioport_get_pin_level(CANCEL_BT_PIN)) {
						delay_ms(1);
						cont_time_exit_reset++;
		
						if(!ioport_get_pin_level(ENTER_BT_PIN)) {
							ResetModuleDefault();
							control_case_settings = RETURN_SHOW_DATE_HOUR;
							break;
						}
						if(cont_time_exit_reset >= TIME_MAX_WAIT_FUNC*100) {
							control_case_settings = RETURN_SHOW_DATE_HOUR;
							break;
						}
					}
		  		}
				if(flag_en_exit_time == false) {
					control_case_settings = RETURN_SHOW_DATE_HOUR;
				}
				if(!ioport_get_pin_level(UP_BT_PIN)) {
					flag_en_exit_time = false;
					control_case_settings = MODIFY_SCRIPT_LOCALIZATION_CASE;
					delay_ms(TIME_CONTROL_SETTINGS);
				}
			break;	
				
			case RETURN_SHOW_DATE_HOUR:
				check_password = false;
				LCDClear();
				PointerFunction = ShowDateHour;
			break;
		}	
		if(!ioport_get_pin_level(CANCEL_BT_PIN)) PointerFunction = ShowDateHour;
	}
	PointerFunction = ShowDateHour;
}

/*====================== Others Functions App =====================*/
bool CheckEnableMelodyClock(struct date_rtc date_now) {
	bool enable_melody = false;
	uint8_t val_melody = 0;
	
	MEMGetClockScript(sweep_buf, &script_clk_mem);
	
	if(script_clk_mem.hour == date_now.hour && script_clk_mem.minute == date_now.minute){
		sweep_buf++;
		
		if(sweep_buf >= 100) {
			 sweep_buf = 0;
		}
		else {
			/* Control Flag for not to repeat the song within one minute (40s + 20s(Time song))*/
			if(date_now.second < TIME_MAX_MELODY_ENABLE_S -1) {
				val_melody = ((script_clk_mem.third_digit_melody - 0x30) * 10) + (script_clk_mem.fourth_digit_melody-0x30);
				enable_melody = true;
				LCDClear();
				LCDPrintString(1,0,"Audio:");
				LCDWriteInt(val_melody);
				LCDPrintString(2,0,"Acionado");
				ioport_set_pin_level(LED_ALARM_PIN, HIGH);
				MP3AjustVolume(PARAMETERS_INIT.value_volume_system);
				delay_ms(TIME_TO_WRITE_CMD);
				MP3PlayMusic(val_melody);
				delay_ms(TIME_MAX_MELODY_ENABLE_MS);
				MP3StopModule();
				LCDClear();
				ioport_set_pin_level(LED_ALARM_PIN, LOW);
				PARAMETERS_INIT.value_last_melody_enable = val_melody;
				StoreDataOnUserPage(USER_VAR_IS_INIT,&PARAMETERS_INIT,sizeof(PARAMETERS_INIT));
				val_melody = 0;	
			}
		}
	}
}
void UpdateRTC(void) {
	date_now.second = RTCDS1307GetDateTime(ADDR_SEC_RTC);
	date_now.minute = RTCDS1307GetDateTime(ADDR_MIN_RTC);
	date_now.hour   = RTCDS1307GetDateTime(ADDR_HOUR_RTC);
	date_now.day    = RTCDS1307GetDateTime(ADDR_DAY_RTC);
	date_now.month  = RTCDS1307GetDateTime(ADDR_MONTH_RTC);
	date_now.year   = RTCDS1307GetDateTime(ADDR_YEAR_RTC);
	
	date_now.second = RTCDS1307BcdToDec(date_now.second);
	date_now.minute = RTCDS1307BcdToDec(date_now.minute);
	date_now.hour   = RTCDS1307BcdToDec(date_now.hour);
	date_now.day    = RTCDS1307BcdToDec(date_now.day);
	date_now.month  = RTCDS1307BcdToDec(date_now.month);
	date_now.year   = RTCDS1307BcdToDec(date_now.year);
	
	if(date_now.minute > 0x3B) {
		RTCDS1307SetDateTime(ADDR_MIN_RTC, (unsigned int)RTCDS1307DecToBcd(0x00));
		RTCDS1307SetDateTime(ADDR_HOUR_RTC,(unsigned int)RTCDS1307DecToBcd(date_now.hour+1)); //testing...
	}
	LCDSetCursor(1,0);
	LCDWriteInt(date_now.day);
	LCDPrintChar('/');
	LCDWriteInt(date_now.month);
	LCDPrintChar('/');
	LCDWriteInt(date_now.year);

	LCDSetCursor(2,0);
	LCDWriteInt(date_now.hour);
	LCDPrintChar(':');
	LCDWriteInt(date_now.minute);
	LCDPrintChar(':');
	LCDWriteInt(date_now.second);
}

void ResetModuleDefault(void) {
	uint8_t i;
	
	LCDClear();
	LCDPrintString(1,0,"Apagando");
	LCDPrintString(2,0, "Memoria.");
	MEMErase();
	MP3VolumeDefaultMem();
	RTCDS1307StartDefault();
	PasswordDefault();
	LCDClear();
	
	for(i = 0; i<8; i++) {
		LCDSetCursor(1,i);
		LCDPrintChar('#');
		delay_ms(200);
	}
	for(i = 0; i<8; i++) {
		LCDSetCursor(2,i);
		LCDPrintChar('#');
		delay_ms(200);
	}
	for(i = 0; i<8; i++) {
		LCDSetCursor(1,i);
		LCDPrintChar(' ');
		delay_ms(200);
	}
	for(i = 0; i<8; i++) {
		LCDSetCursor(2,i);
		LCDPrintChar(' ');
		delay_ms(200);
	}
	LCDClear();
	LCDPrintString(1,0,"Apagado.");
	LCDPrintString(2,0, "Sucesso.");
	delay_ms(2000);
}


