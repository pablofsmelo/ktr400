/*********************************************************************
 *						File Name: ProtocolCOM.h					 *
 * Project Name: KTR400												 *
 * Created: 23/02/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: Protocol of Communication between Microcontroller and PC	 *
 ********************************************************************/
#ifndef PROTOCOLCOM_H_
#define PROTOCOLCOM_H_

/*==== Defines for communication protocol between PC and device =====*/
#define LENGHT_BUF_ACK					 5

#define BUFFER_SIZE_LIST_POSITION	     24
#define BUFFER_SIZE_LIST_HOUR			 6
#define ADDRESS_KTR400_CMD				 0x01
#define ADDRESS_PC_CMD					 0x00
#define START_POSITION_CMD				 0x10
#define STOP_POSITION_CMD				 0x11
#define START_CLOCK_CMD				     0x20
#define CHECKSUM_CLOCK_CMD				 0x21
#define CHECKSUM_POSITION_CMD			 0x11

#define ACK_CMD							 0x66
#define NACK_CMD						 0x67

#define CHECKSUM_ACK_VALUE				 0x67
#define CHECKSUM_NACK_VALUE				 0x68
#define MAX_QTD_OF_LIST_POSITION		 100
#define MAX_QTD_OF_LIST_HOUR			 100

#define SOURCE_COMMUNICATION		     0
#define DESTINATION_COMMUNICATION		 1
#define COMMAND_COMMUNICATION			 2
#define QTD_BYTES_COMMUNICATION			 3
#define CHECKSUM_BYTE_COMMUNICATION		 4

#define	LENGHT_SCRIPT_CLOCK				 8

#define FIRST_DIGIT_MELODY				 0
#define SECOND_DIGIT_MELODY				 1
#define THIRTH_DIGIT_MELODY				 2
#define FOURTH_DIGIT_MELODY				 3		

#define QTD_MAX_SCRIPTS				     100
typedef struct {
	unsigned char *id_device;
	unsigned char script_number;
	unsigned char *melody_number;
	float latitude;
	float longitude;
}script_localization;

typedef struct {
	unsigned char script_number;
	unsigned char *melody_number;
	unsigned char _hour;
	unsigned char _minute;
	unsigned char _seconds;
}script_clock; 

/*==== Functions for communication protocol between PC and device =====*/
void SendACK                  (void);
bool CheckIsInitScriptPosition(unsigned char *data_bytes);
bool CheckIsInitScriptClock   (unsigned char *data_bytes);
bool CheckIsFinishScriptClock (unsigned char *data_bytes);
void ReceiveScriptPosition    (void);
void ReceiveScriptClock       (void);

#endif /* PROTOCOLCOM_H_ */


