/*********************************************************************
 *					File Name: MyApp.h								 *
 * Project Name: KTR400												 *
 * Created: 10/04/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: This module deals with the state machine					 *
 ********************************************************************/
#ifndef MYAPP_H_
#define MYAPP_H_

#include <asf.h>
#include "GPSConfigApp.h"
#include "LCDConfigApp.h"
#include "MEMConfigApp.h"
#include "MP3ConfigApp.h"
#include "RS232PanelConfigApp.h"
#include "RS485PanelConfigApp.h"
#include "RTCConfigApp.h"
#include "USBHardware.h"

/*===================== Defines for MyApp =========================*/
/* Frequency value used to generate the timer period */
#define FREQUENCY_HZ_BASE_SYSTICK		10
#define TIME_PERIOD_LED					10  /*1s*/ 
#define TIME_MAX_WAIT_FUNC				200 /*20s*/

#define MODIFY_DATE_CASE				0
#define MODIFY_PASSWORD_CASE			1
#define MODIFY_VOLUME_CASE				2
#define MODIFY_SCRIPT_CLOCK_CASE	    3
#define MODIFY_SCRIPT_LOCALIZATION_CASE 4
#define RETURN_SHOW_DATE_HOUR			5
#define RESET_KTR400_DEFAULT_CASE		6

#define TIME_CONTROL_SETTINGS			200
#define TIME_MODIFY_SETTINGS			200
#define TIME_QUESTION_RST_DEFAULT		200

/* To change the audio time, change the values of the defines below */
#define TIME_MAX_MELODY_ENABLE_MS		30000
#define TIME_MAX_MELODY_ENABLE_S		30

typedef struct init_mem{
	uint8_t rtc_is_initialized;
	uint8_t password_is_initialized;
	uint8_t value_volume_system;
	uint8_t value_last_melody_enable;
};
extern struct init_mem PARAMETERS_INIT;
extern bool flag_en_exit_time; 

/*============ Function Prototypes For Initializations =============*/
void (*PointerFunction)    (void);
void InitMicrocontroller   (void);
void configHardwareKTR400  (void);

/*====================== Finite State Machines =====================*/
void MySystemInit          (void);
void ShowDateHour          (void); 
void TestMelody            (void);
void ModifySystemSettings  (void);
void Settings              (void);

/*====================== Others Functions App =====================*/
void ResetModuleDefault    (void);
bool CheckEnableMelodyClock(struct date_rtc date_now);
void UpdateRTC			   (void);

#endif /* MYAPP_H_ */