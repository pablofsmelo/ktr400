/*********************************************************************
 *					File Name: MEMConfigApp.c						 *
 * Project Name: KTR400												 *
 * Created: 20/03/2018 14:40:59										 *
 * Author: Pablo Melo												 *
 * Notes:										 					 *
 ********************************************************************/
#include <asf.h>
#include <spi_master.h>
#include "MEMConfigApp.h"
#include "ProtocolCOM.h"
#include "MyApp.h"
#include <string.h>

/*================== Functions of Memory Flash Extern ==================*/
struct spi_device AT25DFX_DEVICE1 = {
	.id = 0
};

void MEMInit(void) {
	spi_master_init(SPI_USER);
	spi_enable(SPI_USER);
	spi_master_setup_device(SPI_USER,&AT25DFX_DEVICE1,SPI_MODE_3,FREQUENCY_SPI_MEM_HZ, BOARD_SPI_ID);
}

void MEMErase(void) {
	uint16_t blocks = 0;
	uint8_t buffer_aux[] = {BLOCK_ERASE_CMD, blocks>>5, blocks<<3, 0};
		
	while(blocks < DATAFLASH_BLOCKS) {
		spi_select_device(SPI_USER,&AT25DFX_DEVICE1);
		delay_ms(10);
		buffer_aux[1] = blocks>>5;
		buffer_aux[2] = blocks<<3;
		spi_write_packet(SPI_USER, buffer_aux, 4);
		spi_deselect_device(SPI_USER,&AT25DFX_DEVICE1);
		blocks++;
		
		MEMCheckIsBusy();
	}
}

uint8_t MEMCheckIsBusy(void) {
	uint8_t ValIsBusy = 0;
	uint8_t buffer_aux[2];
		
	spi_select_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_LOW);
	delay_ms(10);
	spi_write_byte(SPI_USER,STATUS_REGISTER_READ_CMD);
	do
	{
		//val=spi_read_byte(SPI_USER);
		spi_read_packet(SPI_USER,buffer_aux,2);
		ValIsBusy = buffer_aux[0];
	}
	while(!(ValIsBusy&0x80));
	spi_deselect_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_HIGH);
		
	return ValIsBusy;
}

void MEMFlashToBuffer(uint16_t page) {
	uint8_t buffer_aux[] = {MM_PAGE_TO_BUFFER_TRANSFER_CMD, page>>7, page<<1, 0};
	
	MEMCheckIsBusy();
	spi_select_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_LOW);
	delay_ms(10);
	//Write_SPI(MM_PAGE_TO_B1_XFER);
	//Write_SPI((unsigned char)(pagina>>7));
	//Write_SPI((unsigned char)(pagina<<1));
	//Write_SPI(0x00);
	spi_write_packet(SPI_USER,buffer_aux,4);
	spi_deselect_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_HIGH);
}

void MEMBufferToFlash(uint16_t page) {
	uint8_t buffer_aux[] = {BUFFER_TO_MM_PAGE_PROG_WITH_ERASE_CMD, page>>7, page<<1, 0};
	
	MEMCheckIsBusy();
	
	spi_select_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_LOW);
	delay_ms(10);


	//Write_SPI(MM_PAGE_TO_B1_XFER);
	//Write_SPI((unsigned char)(pagina>>7));
	//Write_SPI((unsigned char)(pagina<<1));
	//Write_SPI(0x00);
	spi_write_packet(SPI_USER,buffer_aux,4);
	spi_deselect_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_HIGH);
}

void MEMRecordFlash(uint32_t address, uint8_t *str, uint8_t lenght) {
	uint8_t size_aux;
	uint8_t *ptr;
	uint16_t page, end_buffer;
	
	
	ptr = (uint8_t *) &str[0];
	
	page = (uint16_t)((uint32_t)address/ (uint32_t)DATAFLASH_BUFFER_SIZE);		   /* Calculate the page */  
	end_buffer = (uint16_t)((uint32_t)address % (uint32_t)DATAFLASH_BUFFER_SIZE);  /* Calculate the address in the page */
	
	
	/* Verify Pagination */
	if(end_buffer+lenght> DATAFLASH_BUFFER_SIZE) {
		
		size_aux = DATAFLASH_BUFFER_SIZE - end_buffer; /* Size still the end of page */   
		MEMFlashToBuffer(page);						   /* Search the page in memory and put it in the buffer */								
		MEMWriteBuffer(end_buffer, str, size_aux);	   /* Fills the buffer to the bottom of the page */
		MEMBufferToFlash(page);						   /* Write page in Data Flash */
		page++;										   /* Go to Next page */
		end_buffer = 0;								   /* Address 0 of page */
		lenght -= size_aux;                        
		ptr = &str[size_aux];
	}
	MEMFlashToBuffer(page);							  /* Update buffer with memory contents */
	MEMWriteBuffer(end_buffer, ptr, lenght);		  /* Fills the buffer with the values to be written */
	MEMBufferToFlash(page);							  /* Write the contents of Buffer 1 in FLASH */
}

void MEMReadFlash(uint32_t address, uint8_t *str, uint8_t lenght) {
	uint16_t page = 0, end_buffer = 0;
	uint8_t buf[20];
	
	page = (uint16_t)((uint32_t)address / (uint32_t)DATAFLASH_BUFFER_SIZE);   
	end_buffer = (uint16_t)((uint32_t)address % (uint32_t)DATAFLASH_BUFFER_SIZE); 
		
	uint8_t buffer_aux[] = {MAIN_MEMORY_CONTINUOUS_READ_CMD,page>>7,((page<<1) | (end_buffer>>8)),end_buffer};
	
	MEMCheckIsBusy();
	delay_ms(10);
	spi_select_device(SPI_USER,&AT25DFX_DEVICE1);
	spi_write_packet(SPI_USER,buffer_aux,4);
	spi_read_packet(SPI_USER,str,lenght);
	spi_deselect_device(SPI_USER,&AT25DFX_DEVICE1);
}

void MEMWriteBuffer(uint16_t end_buffer, uint8_t *str, uint8_t lenght) {
	uint8_t buffer_aux[] = {BUFFER_WRITE_CMD,0,end_buffer>>8,end_buffer};
	
	MEMCheckIsBusy();
	spi_select_device(SPI_USER,&AT25DFX_DEVICE1);
	delay_ms(10);
	spi_write_packet(SPI_USER,buffer_aux,4);
	spi_write_packet(SPI_USER,str,lenght);
	spi_deselect_device(SPI_USER,&AT25DFX_DEVICE1);
}

void MEMReadID(void) {
	//uint8_t val=0;
	uint8_t buffer_aux[5] = {0,0,0,0,0};
	
	spi_select_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_LOW);
	delay_ms(10);
	spi_write_byte(SPI_USER,0x9F);
	//do
	//{
	//val=spi_read_byte(SPI_USER);
	spi_read_packet(SPI_USER,buffer_aux,5);
	//val=bf[0];
	//}
	//while(!(val & 0x80));
	spi_deselect_device(SPI_USER,&AT25DFX_DEVICE1);
	//ioport_set_pin_level(PIN_CS_MEM,IOPORT_PIN_LEVEL_HIGH);
}

/*==================== Functions of Application =======================*/

void MEMSetClockScript(unsigned char *buff_usb, uint8_t val_script) {
	uint8_t buf_aux[LENGHT_BUFF_MEM_CLOCK_SCRIPT + 1];
	
	buf_aux[0] = val_script;
	
	for (uint8_t i=1; i<LENGHT_BUFF_MEM_CLOCK_SCRIPT + 1; i++) {
		buf_aux[i] = buff_usb[i-1];
	}
	MEMRecordFlash(ADDR_INIT_CLOCK_SCRIPT + ((val_script - 1) * (LENGHT_BUFF_MEM_CLOCK_SCRIPT+1)), buf_aux, LENGHT_BUFF_MEM_CLOCK_SCRIPT+1);
}	

void MEMGetClockScript(uint8_t val_script, struct DATA_CLOCK_MEM *datas) {
	uint8_t buf_aux[LENGHT_BUFF_MEM_CLOCK_SCRIPT + 1];
	
	MEMReadFlash(ADDR_INIT_CLOCK_SCRIPT + (val_script - 1) * (LENGHT_BUFF_MEM_CLOCK_SCRIPT+1), buf_aux, LENGHT_BUFF_MEM_CLOCK_SCRIPT+1);
	memcpy(datas, buf_aux, (LENGHT_BUFF_MEM_CLOCK_SCRIPT+1));
}

void MEMSetPositionScript(unsigned char *buff_usb, uint8_t val_script) {
	uint8_t buf_aux[LENGHT_BUFF_MEM_POSITION_SCRIPT + 1];
	
	buf_aux[0] = val_script;	
	buf_aux[1] = buff_usb[0];
	buf_aux[2] = buff_usb[1];
	buf_aux[3] = buff_usb[2];
	buf_aux[4] = buff_usb[3];
	buf_aux[5] = buff_usb[4];
	buf_aux[6] = buff_usb[6];
	buf_aux[7] = buff_usb[7];
	buf_aux[8] = buff_usb[12];
	buf_aux[9] = buff_usb[14];
	buf_aux[10]= buff_usb[15];
		
	MEMRecordFlash(ADDR_INIT_POSITION_SCRIPT + ((val_script - 1) * (LENGHT_BUFF_MEM_POSITION_SCRIPT+1)), buf_aux, LENGHT_BUFF_MEM_POSITION_SCRIPT+1);
}
void MEMGetPositionScript(uint8_t val_script, struct DATA_POSITION_MEM *datas) {
	uint8_t buf_aux[LENGHT_BUFF_MEM_POSITION_SCRIPT+ 1];
	
	MEMReadFlash(ADDR_INIT_POSITION_SCRIPT + (val_script - 1) * (LENGHT_BUFF_MEM_POSITION_SCRIPT+1), buf_aux, LENGHT_BUFF_MEM_POSITION_SCRIPT+1);
	memcpy(datas, buf_aux, (LENGHT_BUFF_MEM_POSITION_SCRIPT+1));
}
/*================================================================*/
