/**********************************
 * File Name: HardwareConfig.c
 * Project Name: KTR400	
 * Created: 19/02/2018 14:29:59
 * Author: Pablo Melo
 * Notes:
 **********************************/
#include <asf.h>
#include "HardwareConfig.h"

/* ------------------ TWI (I2C) Functions Configuration ------------------ */

void TWIClockEnable (void) {
	sysclk_enable_peripheral_clock(TWIM_USER);
}

void TWIInit(void) {
	
	/* Set TWIM options */
	uint32_t cpu_speed = sysclk_get_peripheral_bus_hz(TWIM_USER);
	
	struct twim_config opts= {
		.twim_clk = cpu_speed,
		.speed = TWIM_SPEED,
		.hsmode_speed = 0,
		.data_setup_cycles = 0,
		.hsmode_data_setup_cycles = 0,
		.smbus = false,
		.clock_slew_limit = 0,
		.clock_drive_strength_low = 0,
		.data_slew_limit = 0,
		.data_drive_strength_low = 0,
		.hs_clock_slew_limit = 0,
		.hs_clock_drive_strength_high = 0,
		.hs_clock_drive_strength_low = 0,
		.hs_data_slew_limit = 0,
		.hs_data_drive_strength_low = 0,
	};
	
	/* Initialize the TWIM Module */
	twim_set_callback(TWIM_USER, 0, twim_default_callback, 1);
	twim_set_config(TWIM_USER, &opts);
}

void TWIBufferSend(uint32_t reg_addr,uint8_t reg_addr_l,uint8_t addres_chip,void *buffer,uint8_t length_buffer) {
	
	twi_package_t packet_write = {
		.addr	      = reg_addr,			  /* TWI slave memory address data		*/
		.addr_length  = reg_addr_l,			  /* TWI slave memory address data size */
		.chip         = addres_chip,		  /* TWI slave bus address				*/
		.buffer       = buffer,				  /* Transfer data source buffer		*/
		.length       = length_buffer         /* Transfer data size (bytes)			*/
	};
	while(twi_master_write(TWIM_USER, &packet_write) !=  TWI_SUCCESS);
}

void TWIBufferReceive(uint32_t reg_addr,uint8_t reg_addr_l,uint8_t addres_chip,void *buffer,uint8_t length_buffer) {
	
	twi_package_t packet_read = {
		.addr[0]      = reg_addr,			   /* TWI slave memory address data		 */
		.addr_length  = reg_addr_l,			   /* TWI slave memory address data size */
		.chip         = addres_chip,		   /* TWI slave bus address				 */
		.buffer       = buffer,				   /* Transfer data source buffer		 */
		.length       = length_buffer          /* Transfer data size (bytes)		 */
	};
	while(twi_master_read(TWIM_USER, &packet_read) != TWI_SUCCESS);
}

/* ------------------ SPI Functions Configuration ------------------ */







/* ------------------ USART0 Functions Configuration ------------------ */

/* ------------------ USART1 Functions Configuration ------------------ */

/* ------------------ USART2 Functions Configuration ------------------ */

/* ------------------ USB Functions Configuration ------------------ */

/* ------------------ WDT Functions Configuration ------------------ */