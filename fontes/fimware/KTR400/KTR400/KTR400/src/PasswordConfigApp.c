/*********************************************************************
 *					File Name: PasswordConfigApp.c				     *
 * Project Name: KTR400												 *
 * Created: 10/04/2018 14:29:59										 *
 * Author: Pablo Melo												 *
 * Notes: 															 *
 ********************************************************************/
#include <asf.h>
#include "MyApp.h"
#include "PasswordConfigApp.h"
#include "LCDConfigApp.h"
#include "FlashHardware.h"

static unsigned long int set_password = 0;
uint8_t insert_case_password = 1;
uint8_t cursor_position_password = 3;
char val_digit_password = 0;

bool flag_check_password_is_ready = false;
bool flag_control_new_password = false;
bool flag_control_check_password = false;
bool flag_return_dig_password = false;
bool state_password = false;
struct digits password_dig;
struct init_mem PARAMETERS_INIT;

/*==================== Functions for Password ======================*/
void PasswordSystemInit(void) {
	PARAMETERS_INIT.password_is_initialized = false;
	GetDataFromUserPage(USER_VAR_IS_INIT, (uint8_t*)&PARAMETERS_INIT, sizeof(PARAMETERS_INIT));
	
	if(PARAMETERS_INIT.password_is_initialized== false) {
		PasswordDefault();
	}
}

bool PasswordControl (void) {
	static unsigned long int set_password = 0;
	bool state_password = false;
	
	if(flag_control_new_password) {
		LCDClear();
		LCDPrintString(1,0, "Nova Sen");
		LCDPrintString(2,2,	"****");
		delay_ms(200);
	}
	if(flag_control_check_password) {
		LCDClear();
		LCDPrintString(1,1, "Senha:");
		LCDPrintString(2,2,	"****");
		delay_ms(200);
	}
	flag_check_password_is_ready = false;
	insert_case_password = SELECT_FIRST_DIGIT;
	flag_en_exit_time = false;
	
	while (flag_check_password_is_ready == false) {
		delay_ms(200);
	
		switch (insert_case_password)
		{
			case SELECT_FIRST_DIGIT:
			
				LCDSetCursor(2,2);
				LCDPrintChar(0b01011111);
				
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					
					LCDPrintString(2,3,	"***");
					
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_check_password_is_ready = true;
						val_digit_password = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password < 9) val_digit_password++;
						PositionDigitLCD(1, val_digit_password);
					}
					
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password > 0) val_digit_password--;
						PositionDigitLCD(1, val_digit_password);
					}
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						password_dig.first_digit = val_digit_password;
						flag_en_exit_time = false;
						insert_case_password = SELECT_SECOND_DIGIT;
						val_digit_password = 0;
						break;
					}
				}
			break;
			
			case SELECT_SECOND_DIGIT:
			
				LCDSetCursor(2,3);
				LCDPrintChar(0b01011111);
			
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
				
					LCDSetCursor(2,2);
					LCDPrintChar('*');
					LCDSetCursor(2,4);
					LCDPrintChar('*');
					LCDPrintChar('*');
				
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_check_password_is_ready = true;
						val_digit_password = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password < 9) val_digit_password++;
						PositionDigitLCD(2, val_digit_password);
					}
				
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password > 0) val_digit_password--;
						PositionDigitLCD(2, val_digit_password);
					}
				
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						password_dig.second_digit = val_digit_password;
						flag_en_exit_time = false;
						insert_case_password = SELECT_THIRD_DIGIT;
						val_digit_password = 0;
						break;
					}
				}
			break;
			
			case SELECT_THIRD_DIGIT:
			
				LCDSetCursor(2,4);
				LCDPrintChar(0b01011111);
			
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
				
					flag_en_exit_time = true;
					LCDSetCursor(2,2);
					LCDPrintChar('*');
					LCDPrintChar('*');
					LCDSetCursor(2,5);
					LCDPrintChar('*');
				
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_check_password_is_ready = true;
						val_digit_password = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password < 9) val_digit_password++;
						PositionDigitLCD(3, val_digit_password);
					}
				
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password > 0) val_digit_password--;
						PositionDigitLCD(3, val_digit_password);
					}
				
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						password_dig.third_digit = val_digit_password;
						flag_en_exit_time = false;
						insert_case_password = SELECT_FOURTH_DIGIT;
						val_digit_password = 0;
						break;
					}
				}
			break;
			
			case SELECT_FOURTH_DIGIT:
			
				LCDSetCursor(2,5);
				LCDPrintChar(0b01011111);
			
				while (ioport_get_pin_level(CANCEL_BT_PIN)) {
					flag_en_exit_time = true;
					LCDSetCursor(2,2);
					LCDPrintChar('*');
					LCDPrintChar('*');
					LCDPrintChar('*');
				
					if(!ioport_get_pin_level(CANCEL_BT_PIN) || flag_en_exit_time == false) {
						flag_check_password_is_ready = true;
						val_digit_password = 0;
						break;
					}
					if(!ioport_get_pin_level(UP_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password < 9) val_digit_password++;
						PositionDigitLCD(4, val_digit_password);
					}
					if(!ioport_get_pin_level(DOWN_BT_PIN)) {
						flag_en_exit_time = false;
						if(val_digit_password > 0) val_digit_password--;
						PositionDigitLCD(4, val_digit_password);
					}
				
					if(!ioport_get_pin_level(ENTER_BT_PIN)) {
						password_dig.fourth_digit = val_digit_password;
						flag_en_exit_time = false;
						flag_check_password_is_ready = true;
						insert_case_password = SELECT_FIRST_DIGIT;
					
						if(flag_control_new_password) {
							flag_control_new_password = false;
							StoreDataOnUserPage(USER_PASSWORD_MEM, &password_dig, sizeof(password_dig));
							LCDClear();
							LCDPrintString(1,0, "Nova Sen");
							LCDPrintString(2,0, "Sucesso.");
							delay_ms(2000);
							LCDClear();
							val_digit_password = 0;
							password_dig.first_digit = 0;
							password_dig.second_digit = 0;
							password_dig.third_digit = 0;
							password_dig.fourth_digit = 0;
							break;
						}
					
						if(flag_control_check_password) {
							flag_control_check_password = false;
							state_password =  CheckPasswordMem(password_dig);
						
							if(state_password) {
								LCDClear();
								LCDPrintString(1,1, "Senha.");
								LCDPrintString(2,0, "Correta.");
								delay_ms(2000);
								LCDClear();
							
							}
							else {
								LCDClear();
								LCDPrintString(1,1, "Senha.");
								LCDPrintString(2,1, "Errada");
								delay_ms(2000);
								LCDClear();
							}
							val_digit_password = 0;
							password_dig.first_digit = 0;
							password_dig.second_digit = 0;
							password_dig.third_digit = 0;
							password_dig.fourth_digit = 0;
							break;
						}
					}
				}
			break;
		}
	}
	
	return state_password;
}
void NewPassword (void) {
	flag_control_new_password = true;
	flag_control_check_password = false;		
	PasswordControl();
}
bool CheckPasswordIsCorrect (void) {
	bool check_psw_aux;
	flag_control_check_password = true;
	flag_control_new_password = false;
	check_psw_aux = PasswordControl();

	return check_psw_aux;	
}
void PositionDigitLCD(uint8_t position_digit, uint8_t val_digit) {
	if(position_digit == 1) {
		LCDSetCursor(2, 3);
		LCDPrintChar('*');
		LCDPrintChar('*');
		LCDPrintChar('*');
		delay_ms(100);
		LCDSetCursor(2,2);
		LCDPrintChar(' ');
		delay_ms(50);
		LCDSetCursor(2, 2);
		LCDPrintChar(0x30 + val_digit_password);
		delay_ms(100);
	}
	if(position_digit == 2) {
		LCDSetCursor(2, 2);
		LCDPrintChar('*');
		LCDSetCursor(2, 4);
		LCDPrintChar('*');
		LCDPrintChar('*');
		delay_ms(100);
		LCDSetCursor(2,3);
		LCDPrintChar(' ');
		delay_ms(50);
		LCDSetCursor(2, 3);
		LCDPrintChar(0x30 + val_digit_password);
		delay_ms(100);
	}
	if(position_digit == 3) {
		LCDSetCursor(2, 2);
		LCDPrintChar('*');
		LCDPrintChar('*');
		LCDSetCursor(2, 5);
		LCDPrintChar('*');
		delay_ms(100);
		LCDSetCursor(2,4);
		LCDPrintChar(' ');
		delay_ms(50);
		LCDSetCursor(2, 4);
		LCDPrintChar(0x30 + val_digit_password);
		delay_ms(100);
	}
	if(position_digit == 4) {
		LCDSetCursor(2, 2);
		LCDPrintChar('*');
		LCDPrintChar('*');
		LCDPrintChar('*');
		delay_ms(100);
		LCDSetCursor(2,5);
		LCDPrintChar(' ');
		delay_ms(50);
		LCDSetCursor(2, 5);
		LCDPrintChar(0x30 + val_digit_password);
		delay_ms(100);
	}
}

bool CheckPasswordMem(struct digits psw) {
	struct digits psw_aux;
	
	GetDataFromUserPage(USER_PASSWORD_MEM, &psw_aux, sizeof(psw_aux));
	
	if((psw.first_digit == 0 && psw.second_digit == 0 && psw.third_digit == 0 && psw.fourth_digit == 0) || 
	  (psw_aux.first_digit == psw.first_digit && psw_aux.second_digit == psw.second_digit && psw_aux.third_digit == psw.third_digit
	  && psw_aux.fourth_digit == psw.fourth_digit))
		return true;
	else											
		return false;
}

void PasswordDefault(void) {
	password_dig.first_digit = 0;
	password_dig.second_digit = 0;
	password_dig.third_digit = 0;
	password_dig.fourth_digit = 0;
	
	PARAMETERS_INIT.password_is_initialized = 1;
	StoreDataOnUserPage(USER_VAR_IS_INIT,&PARAMETERS_INIT,sizeof(PARAMETERS_INIT));
}
/*================================================================*/
