/***********************************
 * File Name: USARTHardware.c
 * Project Name: KTR400	
 * Created: 20/02/2018 
 * Author: Pablo Melo
 * Notes: p_usart =  USART that you will to select !
  **********************************/

#include <asf.h>
#include "USARTHardware.h" 

uint32_t USARTInit(Usart *p_usart, const sam_usart_opt_t *p_usart_opt) {
	sysclk_enable_peripheral_clock(p_usart);
	usart_init_rs232(p_usart,&p_usart_opt,sysclk_get_cpu_hz());
}

void USARTEnableInterrupt(Usart *p_usart, uint8_t priority) {
	
	int8_t aux_IRQn;
	
	usart_enable_interrupt(p_usart, US_IER_RXRDY);
	
	if(p_usart == USART_GPS) aux_IRQn = USART0_IRQn;
	if(p_usart == USART_MP3) aux_IRQn = USART1_IRQn;
	if(p_usart == USART_485) aux_IRQn = USART2_IRQn;

	irq_register_handler(aux_IRQn, priority);
}

void USARTEnableTX(Usart *p_usart) {
	usart_enable_tx(p_usart);
}

void USARTEnableRX(Usart *p_usart) {
	usart_enable_rx(p_usart);
}

uint32_t USARTWriteBuffer(Usart *p_usart, uint32_t *buf,  uint8_t size) {
	uint8_t cont_size = 0;
	
	for(cont_size = 0; cont_size<size; cont_size++) while(usart_write(p_usart, buf[cont_size])) {}
	delay_ms(2);
}

uint32_t USARTWriteChar(Usart *p_usart, uint32_t c) {
	usart_putchar(p_usart, c);
}